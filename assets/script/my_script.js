$(function() {
    // validte user nane or uniqe coulomn
    $(".unique-field").keyup(function () {
        //    unique-field     field-name=""   data-db=""
        // var total_url = window.location.href;
        // var host_name = window.location.hostname;
        // var pathname  = window.location.pathname;
        // var res = host_name.split(pathname);
        //var protocol = window.location.protocol + "//";
        //var base_url = protocol + 'localhost';
       // console.log("go")
        var url_pass = baseUrl + '/JsControl';
        var field_name = $(this).attr("field-name");
        var table = $(this).attr("data-db");
        var span = "span_" + field_name;
        var value = $(this).val();
        var dataString = 'field_name=' + field_name + '&table=' + table + '&unique_field=1' + "&value=" + value;
        var obj = $(this);
        $.ajax({
            type: 'post',
            url: url_pass,
            data: dataString,
            dataType: 'html',
            cache: false,
            success: function (html) {
                html_out = html.trim();
                if (html_out == "off") {
                    var validClass =$(this).hasClass("unique-error") ;
                    if( validClass != true) {
                        obj.css("border-color", "red");
                        obj.addClass("unique-error");
                        obj.after('<span style="color: red" class="unique-span"> مسجل من قبل  </span>');
                        $('button[type="submit"]').attr("disabled", "disabled");
                        //$('inpu[type="submit"]').attr("disabled","disabled");
                    }
                } else {
                    obj.css("border-color", "green");
                    obj.next(".unique-span").remove();
                    obj.removeClass("valid-error");
                    $('button[type="submit"]').removeAttr("disabled");
                }
            }
        });
        return false;
    });
    //============================================================
    $(".only-number").keypress(function(event) {
        var obj = $(this);
        var theEvent = event || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) {
                theEvent.preventDefault()
            }
            var validClass =obj.hasClass("num-error") ;
            if( validClass != true) {
                obj.addClass("num-error");
                obj.after('<span style="color: red" class="num-span"> يرجى إدخال ارقام فقط  </span>');
            }
        }else{
            obj.next(".num-span").remove();
            obj.removeClass("num-error");
        }

    });
    //==================================================
    $(".only-En").keypress(function (event) {
        var obj = $(this);
        var theEvent = event || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /^[A-Za-z0-9-@-_]*$/;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) {
                theEvent.preventDefault()
            }
            var validClass =obj.hasClass("en-error") ;
            if( validClass != true) {
                obj.addClass("en-error");
                obj.after('<span style="color: red" class="enshlish-span"> يرجى الادخال باللغه الانجليزية فقط </span>');
            }
        }else{
            obj.next(".enshlish-span").remove();
            obj.removeClass("en-error");
        }
    });
    //==================================================
    $(".log-me").click(function (){
        console.log("go")

        var obj = $(this);
        var $form = obj.closest("form");
        var user_name = $("[log-me ='name']",$form).val();
        var password  = $("[log-me ='pass']",$form).val();
        var dataString = {user_name:user_name,password:password};
         console.log(dataString)
        $.ajax({
            type:'post',
            url: baseUrl+'/web-auth',
            data:dataString,
            dataType: 'html',
            cache:false,
            success: function(result){
                result = JSON.parse(result);
                console.log(result)
                if(result.code == 200){
                    var html = '<div class="alert alert-success" role="alert"> تم بنجاح   </div>';
                    $(".log-me-div",$form).html(html);
                    window.location = result.url;
                }else{
                    var text = "";
                    var clas = "danger";
                    switch (result.code) {
                        case 422:
                            text ="إدخل جميع البيانات ";
                            break;
                        case 401: // wrong pass
                            text ="تأكد من  كلمة المرور " ;
                            break;
                        case 404:// not found
                            text = "تأكد من بيانات الحساب ";
                            break;
                        case 423: // blocked account
                            text = "عذرا تم حظر الحساب من قبل إدارة الموقع ";
                            break;
                        default:
                            text = "خطأ" ;
                    }
                    var html = '<div class="alert alert-'+clas+'" role="alert">' +
                        '<h5 class="text-center">'+ text +' </h5> </div>';
                    $(".log-me-div",$form).html(html);
                }
            },
            error:function(error){
                console.log(error.responseText);
				var html = '<div class="alert alert-danger" role="alert">' +
					'<h5 class="text-center">خطأ خطأ </h5> </div>';
                $(".log-me-div",$form).html(html);
            }
        });

    });
    //==================================================


});// end main function

//=========================================================================
function getVisitDay(dateValue){
    var dataString = {search:"search_visit_day",searchDate:dateValue};
    //console.log("ahmed")
    $.ajax({
        type:'post',
        url: baseUrl+'/Dashboard/statistics',
        data:dataString,
        dataType: 'html',
        cache:false,
        success: function(result){
            // console.log(result)
            $("#daly_visit").html(result);
        },
        error:function(error){
            console.log(error.responseText);
        }
    });
}
//=========================================================================
function getVisitMonth(monthValue) {
    var dataString = {search:"search_visit_month",searchMonth:monthValue};
    // console.log("ahmed")
    $.ajax({
        type:'post',
        url: baseUrl+'/Dashboard/statistics',
        data:dataString,
        dataType: 'html',
        cache:false,
        success: function(result){
            //  console.log(result)
            $("#month_visit").html(result);
        },
        error:function(error){
            console.log(error.responseText);
        }
    });
}
//=========================================================================
function valid() {
    if($("#user_pass").val().length > 6){
        document.getElementById('validate1').style.color = '#00FF00';
        document.getElementById('validate1').innerHTML = 'كلمة المرور قوية';
        $('[type="submit"]').removeAttr("disabled");
    }
    else{
        document.getElementById('validate1').style.color = '#F00';
        document.getElementById('validate1').innerHTML = 'كلمة المرور ضعيفة';
        $('[type="submit"]').attr("disabled", "disabled");
    }
}
function valid2() {
    if($("#user_pass").val() == $("#user_pass_validate").val()){
        document.getElementById('validate').style.color = '#00FF00';
        document.getElementById('validate').innerHTML = 'كلمة المرور متطابقة';
        $('[type="submit"]').removeAttr("disabled");
    }else{
        document.getElementById('validate').style.color = '#F00';
        document.getElementById('validate').innerHTML = 'كلمة المرور غير متطابقة';
        $('[type="submit"]').attr("disabled", "disabled");
    }
}
//=========================================================================

//=========================================================================

