<?php
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//--------------- Login   ---------------------------
$route['(ar|en)/auth'] = 'Login/checkLogin';
$route['auth']         = 'Login/checkLogin';
$route['(ar|en)/logout'] = 'Login/logout';
$route['logout']         = 'Login/logout';

//--------------- Dashboard   ----------------------
$route['(ar|en)/Dashboard'] = 'Dashboard';
$route['Dashboard'] = 'Dashboard';
$route['(ar|en)/main-setting'] = 'Dashboard/mainData';
$route['main-setting'] = 'Dashboard/mainData';

//--------------- main-setting   ----------------------
$route['(ar|en)/main-setting'] = 'MainData/add';
$route['main-setting'] = 'MainData/add';
$route['(ar|en)/main-setting/(:any)'] = 'MainData/$1';
$route['main-setting/(:any)'] = 'MainData/$1';
$route['(ar|en)/main-setting/(:any)/(:any)']= 'MainData/$1/$2';
$route['main-setting/(:any)/(:any)']= 'MainData/$1/$2';

//--------------- definitions   ----------------------
$route['(ar|en)/admin-definitions'] = 'Definition';
$route['admin-definitions'] = 'Definition';
$route['(ar|en)/admin-definitions/(:any)'] = 'Definition/$1';
$route['admin-definitions/(:any)'] = 'Definition/$1';
$route['(ar|en)/admin-definitions/(:any)/(:any)']= 'Definition/$1/$2';
$route['admin-definitions/(:any)/(:any)']= 'Definition/$1/$2';

//--------------- main-setting   ----------------------
$route['(ar|en)/admin-users'] = 'Admin';
$route['admin-users'] = 'Admin';
$route['(ar|en)/admin-users/(:any)'] = 'Admin/$1';
$route['admin-users/(:any)'] = 'Admin/$1';
$route['(ar|en)/admin-users/(:any)/(:any)']= 'Admin/$1/$2';
$route['admin-users/(:any)/(:any)']= 'Admin/$1/$2';


//--------------- admin-setting   ----------------------
$route['(ar|en)/admin-backgrounds'] = 'BgAdd/add';
$route['admin-backgrounds'] = 'BgAdd/add';
$route['(ar|en)/admin-backgrounds/(:any)'] = 'BgAdd/$1';
$route['admin-backgrounds/(:any)'] = 'BgAdd/$1';
$route['(ar|en)/admin-backgrounds/(:any)/(:any)']= 'BgAdd/$1/$2';
$route['admin-backgrounds/(:any)/(:any)']= 'BgAdd/$1/$2';

/**
 *  ============================================================
 *
 *  ------------------------------------------------------------
 *
 *  ============================================================
 */

//--------------- company_activities   ----------------------
$route['(ar|en)/admin-company-activity'] = 'CompanyActivity';
$route['admin-company-activity'] = 'CompanyActivity';
$route['(ar|en)/admin-company-activity/(:any)'] = 'CompanyActivity/$1';
$route['admin-company-activity/(:any)'] = 'CompanyActivity/$1';
$route['(ar|en)/admin-company-activity/(:any)/(:any)']= 'CompanyActivity/$1/$2';
$route['admin-company-activity/(:any)/(:any)']= 'CompanyActivity/$1/$2';

//--------------- job titles   ----------------------
$route['(ar|en)/admin-job-title'] = 'JobTitle';
$route['admin-job-title'] = 'JobTitle';
$route['(ar|en)/admin-job-title/(:any)'] = 'JobTitle/$1';
$route['admin-job-title/(:any)'] = 'JobTitle/$1';
$route['(ar|en)/admin-job-title/(:any)/(:any)']= 'JobTitle/$1/$2';
$route['admin-job-title/(:any)/(:any)']= 'JobTitle/$1/$2';

//--------------- Person   ----------------------
$route['(ar|en)/admin-person'] = 'Person';
$route['admin-person'] = 'Person';
$route['(ar|en)/admin-person/(:any)'] = 'Person/$1';
$route['admin-person/(:any)'] = 'Person/$1';
$route['(ar|en)/admin-person/(:any)/(:any)']= 'Person/$1/$2';
$route['admin-person/(:any)/(:any)']= 'Person/$1/$2';
$route['(ar|en)/admin-person/active/(:any)/(:any)']= 'Person/active/$1/$2';
$route['admin-person/active/(:any)/(:any)']= 'Person/active/$1/$2';

//--------------- Company   ----------------------
$route['(ar|en)/admin-company'] = 'Company';
$route['admin-company'] = 'Company';
$route['(ar|en)/admin-company/(:any)'] = 'Company/$1';
$route['admin-company/(:any)'] = 'Company/$1';
$route['(ar|en)/admin-company/(:any)/(:any)']= 'Company/$1/$2';
$route['admin-company/(:any)/(:any)']= 'Company/$1/$2';
$route['(ar|en)/admin-company/active/(:any)/(:any)']= 'Company/active/$1/$2';
$route['admin-company/active/(:any)/(:any)']= 'Company/active/$1/$2';

//---------------  Job    ----------------------
$route['(ar|en)/admin-job'] = 'Job';
$route['admin-job'] = 'Job';
$route['(ar|en)/admin-job/(:any)'] = 'Job/$1';
$route['admin-job/(:any)'] = 'Job/$1';
$route['(ar|en)/admin-job/(:any)/(:any)']= 'Job/$1/$2';
$route['admin-job/(:any)/(:any)']= 'Job/$1/$2';

//---------------  ApplyJob    ----------------------
$route['(ar|en)/admin-apply-job'] = 'ApplyJob';
$route['admin-apply-job'] = 'ApplyJob';
$route['(ar|en)/admin-apply-job/(:any)'] = 'ApplyJob/$1';
$route['admin-apply-job/(:any)'] = 'ApplyJob/$1';
$route['(ar|en)/admin-apply-job/(:any)/(:any)']= 'ApplyJob/$1/$2';
$route['admin-apply-job/(:any)/(:any)']= 'ApplyJob/$1/$2';

//---------------  Country    ----------------------
$route['(ar|en)/admin-country'] = 'Country';
$route['admin-country'] = 'Country';
$route['(ar|en)/admin-country/(:any)'] = 'Country/$1';
$route['admin-country/(:any)'] = 'Country/$1';
$route['(ar|en)/admin-country/(:any)/(:any)']= 'Country/$1/$2';
$route['admin-country/(:any)/(:any)']= 'Country/$1/$2';

//---------------  City    ----------------------
$route['(ar|en)/admin-city'] = 'City';
$route['admin-city'] = 'City';
$route['(ar|en)/admin-city/(:any)'] = 'City/$1';
$route['admin-city/(:any)'] = 'City/$1';
$route['(ar|en)/admin-city/(:any)/(:any)']= 'City/$1/$2';
$route['admin-city/(:any)/(:any)']= 'City/$1/$2';

//---------------  Contact    ----------------------
$route['(ar|en)/admin-contact'] = 'Contact';
$route['admin-contact'] = 'Contact';
$route['(ar|en)/admin-contact/(:any)'] = 'Contact/$1';
$route['admin-contact/(:any)'] = 'Contact/$1';
$route['(ar|en)/admin-contact/(:any)/(:any)']= 'Contact/$1/$2';
$route['admin-contact/(:any)/(:any)']= 'Contact/$1/$2';