<?php
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//--------------- home   ---------------------------
$route['(ar|en)/home'] = 'Web';
$route['home']         = 'Web';

//--------------- Login   ---------------------------
$route['(ar|en)/web-auth'] = 'Web/weblogin';
$route['web-auth']         = 'Web/weblogin';

//--------------- logout   ----------------------
$route['(ar|en)/profile/logout']['GET']  = 'Web/logout';
$route['profile/logout']['GET']          = 'Web/logout';
//--------------- logout   ----------------------
$route['(ar|en)/getCity/(:any)']  = 'Web/getCity/$1';
$route['getCity/(:any)']          = 'Web/getCity/$1';

//--------------- client-register   ----------------------
$route['(ar|en)/register']['POST'] = 'Web/register';
$route['register']['POST']         = 'Web/register';

//--------------- company-profile   ----------------------
$route['(ar|en)/company-profile/(:any)']['GET'] = 'Web/companyProfile/$1';
$route['company-profile/(:any)']['GET']         = 'Web/companyProfile/$1';

//--------------- user-profile   ----------------------
$route['(ar|en)/user-profile/(:any)']['GET'] = 'Web/clientProfile/$1';
$route['user-profile/(:any)']['GET']         = 'Web/clientProfile/$1';

//--------------- update-profile   ----------------------
$route['(ar|en)/update-profile/(:any)']['POST'] = 'Web/updateProfile/$1';
$route['update-profile/(:any)']['POST']         = 'Web/updateProfile/$1';

//--------------- update-profile   ----------------------
$route['(ar|en)/update-social/(:any)']['POST'] = 'Web/updateSocial/$1';
$route['update-social/(:any)']['POST']         = 'Web/updateSocial/$1';

//--------------- update-profile   ----------------------
$route['(ar|en)/update-auth/(:any)']['POST'] = 'Web/updateAuth/$1';
$route['update-auth/(:any)']['POST']         = 'Web/updateAuth/$1';

//--------------- search   -----------------
$route['(ar|en)/search']['GET'] = 'Web/search';
$route['search']['GET'] = 'Web/search';
//--------------- searchBy   -----------------
$route['(ar|en)/searchBy'] = 'Web/searchBy';
$route['searchBy'] = 'Web/searchBy';

//--------------- show-user   ----------------------
$route['(ar|en)/show-user/(:any)']['GET'] = 'Web/showClientProfile/$1';
$route['show-user/(:any)']['GET']         = 'Web/showClientProfile/$1';

//--------------- show-company   ----------------------
$route['(ar|en)/show-company/(:any)']['GET'] = 'Web/showCompanyProfile/$1';
$route['show-company/(:any)']['GET']         = 'Web/showCompanyProfile/$1';

//--------------- read download file   -----------------
$route['read']['GET'] = 'Web/readFile';
$route['download']['GET'] = 'Web/download';

//--------------- publish job    ----------------------
$route['(ar|en)/publish-job/(:any)']['GET']  = 'Web/publishJob/$1';
$route['publish-job/(:any)']['GET']          = 'Web/publishJob/$1';

//--------------- edit job    ----------------------
$route['(ar|en)/edit-job/(:any)/(:any)']['GET']  = 'Web/editJob/$1/$2';
$route['edit-job/(:any)/(:any)']['GET']          = 'Web/editJob/$1/$2';

//--------------- add job    ----------------------
$route['(ar|en)/add-job/(:any)']['POST']  = 'Web/addJob/$1';
$route['add-job/(:any)']['POST']          = 'Web/addJob/$1';

//--------------- update job    ----------------------
$route['(ar|en)/update-job/(:any)/(:any)']['POST']  = 'Web/updateJob/$1/$2';
$route['update-job/(:any)/(:any)']['POST']          = 'Web/updateJob/$1/$2';

//--------------- update job    ----------------------
$route['(ar|en)/delete-job/(:any)/(:any)']['GET']  = 'Web/deleteJob/$1/$2';
$route['delete-job/(:any)/(:any)']['GET']          = 'Web/deleteJob/$1/$2';

//---------------  job detals    ----------------------
$route['(ar|en)/show-job/(:any)/(:any)']['GET']  = 'Web/showJob/$1/$2';
$route['show-job/(:any)/(:any)']['GET']          = 'Web/showJob/$1/$2';

//--------------- add job    ----------------------
$route['(ar|en)/apply-job']['POST']  = 'Web/applyJob';
$route['apply-job']['POST']          = 'Web/applyJob';

//--------------- add job    ----------------------
$route['(ar|en)/get-appliers']['GET']  = 'Web/getAppliers';
$route['get-appliers']['GET']          = 'Web/getAppliers';