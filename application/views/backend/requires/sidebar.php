<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1"
     m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <?php $dir = ($this->AdminLang == "ar")? "la-angle-left":"la-angle-right"; ?>
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
            <a href="<?=base_url().'Dashboard'?>" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-line-graph"></i>
                <span class="m-menu__link-title">
                    <span  class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            <?=lang("home")?>
                        </span>

                    </span>
                </span>
            </a>
        </li>
        <?php if(isset($_SESSION['is_developer']) && $_SESSION['is_developer'] == 1){?>
       <!-- <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-layers"></i>
                <span class="m-menu__link-text">إدارة النظام </span>
                <i class="m-menu__ver-arrow la <?/*=$dir*/?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">إدارة النظام</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?/*=base_url().""*/?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span></i>
                            <span class="m-menu__link-text">الإدارات</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?/*=base_url().""*/?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span></i>
                            <span class="m-menu__link-text">الصفحات</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?/*=base_url().""*/?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span></i>
                            <span class="m-menu__link-text">الصلاحيات</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>-->
        <?php }?>
        <!--
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">المركز الاعلامي</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">المركز الاعلامي</span>
                        </span>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">level 2</span>
                            <i class="m-menu__ver-arrow la <?=$dir?>"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="#" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">level 3</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="#" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">level 2</span></a>
                    </li>
                </ul>
            </div>
        </li>
        -->
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">مستخدمى لوحة التحكم  </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">مستخدمى لوحة التحكم </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-users"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض</span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-users/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">اضافة  </span></a>
                    </li>

                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">البيانات الاساسية </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">البيانات الاساسية </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."main-setting"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">بيانات الموقع</span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-contact"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">رسائل التواصل </span></a>
                    </li>
                     <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-backgrounds/edit/1"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إعدادات الخلفيات </span></a>
                    </li>

                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">الاعدادات </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">الاعدادات </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-definitions"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-definitions/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-country"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">الدول  </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-city"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">المدن   </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">نشاط المؤسسة</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">نشاط المؤسسة</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-company-activity"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-company-activity/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">المسميات الوظيفية</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">المسميات الوظيفية</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-job-title"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-job-title/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">باحث عن عمل</span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">باحث عن عمل</span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-person"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-person/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة  </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">أصحاب عمل (شركات) </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">أصحاب عمل (شركات) </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-company"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-company/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة  </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">الوظائف  </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">الوظائف  </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-job"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض   </span></a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-job/add"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافة   </span></a>
                    </li>

                </ul>
            </div>
        </li>
        <!------------------------------------------>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-web"></i>
                <span class="m-menu__link-text">طلبات التوظيف   </span>
                <i class="m-menu__ver-arrow la <?=$dir?>"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                            <span class="m-menu__link-text">طلبات التوظيف   </span>
                        </span>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="<?=base_url()."admin-apply-job"?>" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">عرض   </span></a>
                    </li>
                </ul>
            </div>
        </li>
        <!------------------------------------------>
    </ul>
</div>