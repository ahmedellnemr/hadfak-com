<?php if($op == 'UPDTATE' ):
    $out['input']='UPDTATE';
    $out['input_title']='تعديل ';
else:
    $out['input']='INSERT';
    $out['input_title']='حفظ ';
endif?>

<?= form_open_multipart($form ,["class"=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']);  ?>

<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>الإسم بالكامل</label>
            <input type="text" name="Pdata[name]" class="form-control" value="<?=$profile_data->name?>"  data-validation="required">
        </div>
        <div class="col-md-4 col-sm-6">
            <label>تاريخ الميلاد</label>
            <?php if (isset($client_data->date_of_birth)): ?>
                <input type="hidden" name="status" value="update">
                <input type="text" name="Pdata_client[date_of_birth]" class="form-control datepicker-m" value="<?=date("Y-m-d",$client_data->date_of_birth)?>"
                       data-validation="required">
            <?php else: ?>
                <input type="hidden" name="status" value="insert">
                <input type="text" name="Pdata_client[date_of_birth]" class="form-control datepicker-m" value=""
                       data-validation="required">
            <?php endif ?>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>الجنسية</label>
            <select name="Pdata_client[nationality]" class="form-control selectpicker " id="j-category" data-validation="required" aria-required="true"  data-show-subtext="true" data-live-search="true" >
                <option value="">اختر الجنسية</option>
                <?php if (isset($all_countries)): ?>
                    <?php foreach ($all_countries as $row):
                        $sel = "";
                        if(isset($client_data->nationality)){
                            if($client_data->nationality == $row->id_country){
                                $sel = "selected";
                            }
                        }
                        ?>
                        <option value="<?=$row->id_country?>" <?=$sel?> ><?=$row->ar_nationality?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف الجنسيات</option>
                <?php endif ?>

            </select>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>بلد الإقامة الأن</label>
            <select name="Pdata[country_id]" class="form-control selectpicker " id="country-ids"  data-show-subtext="true" data-live-search="true" >
                <option value="">اختر الدولة</option>
                <?php if (isset($all_countries)): ?>
                    <?php foreach ($all_countries as $row):
                        $sel = "";
                        if($profile_data->country_id == $row->id_country){
                            $sel = "selected";
                        }?>
                        <option value="<?=$row->id_country?>" <?=$sel?>><?=$row->ar_name?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف الدول</option>
                <?php endif ?>

            </select>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>المدينة</label>
            <select name="Pdata[city_id]" class="form-control selectpicker " id="city-ids"  data-show-subtext="true" data-live-search="true" >
                <option value="">اختر المدينة</option>
                <?php if (isset($all_cities)): ?>
                    <?php foreach ($all_cities as $row):
                        $sel = "";
                        if($profile_data->city_id == $row->id_city){
                            $sel = "selected";
                        }?>
                        <option value="<?=$row->id_city?>" <?=$sel?> ><?=$row->ar_city_title?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المدن</option>
                <?php endif ?>
            </select>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>العنوان</label>
            <input name="Pdata[address]" type="text" value="<?=$profile_data->address?>" class="form-control" data-validation="required">
        </div>
    </div>
    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>البريد الإلكتروني</label>
            <input type="text" name="Pdata[email]" value="<?= $profile_data->email ?>" data-validation="email"
                   class="form-control unique-field" field-name="email" data-db="registrations" >
        </div>
        <div class="col-md-4 col-sm-6">
            <label>رقم الهاتف</label>
            <input type="text" name="Pdata[phone]" value="<?= $profile_data->phone ?>"
                   class="form-control unique-field" field-name="phone" data-db="registrations" >
        </div>
        <div class="col-md-4 col-sm-6">
            <label>رقم الواتس اب</label>
            <input type="text" name="Pdata[whatsapp_num]" value="<?= $profile_data->whatsapp_num ?>" data-validation="required"
                   class="form-control unique-field" field-name="whatsapp_num" data-db="registrations" >
        </div>
    </div>
    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>المؤهل الدراسي</label>
            <select name="Pdata_client[qualification_id_fk]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                <option value="">اختر المؤهل</option>
                <?php if (isset($all_qualifications)): ?>
                    <?php foreach ($all_qualifications as $row):
                        $sel = "";
                        if (isset($client_data->qualification_id_fk)) {
                            if ($client_data->qualification_id_fk == $row->id_definition) {
                                $sel = "selected";
                            }
                        } ?>
                        <option value="<?=$row->id_definition?>"  <?=$sel?> ><?=$row->ar_title?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المؤهل</option>
                <?php endif ?>
            </select>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>تاريخ الحصول عليه</label>
            <?php if (isset($client_data->qualification_date)): ?>
                <input type="text" name="Pdata_client[qualification_date]"  value="<?=date("Y-m-d",$client_data->qualification_date)?>"
                       class="form-control datepicker-m" data-validation="required">
            <?php else: ?>
                <input type="text" name="Pdata_client[qualification_date]"
                       class="form-control datepicker-m" data-validation="required">
            <?php endif ?>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>المسمى الوظيفي</label>
            <select name="Pdata_client[job_title_id_fk]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                <option value="">اختر المسمى الوظيفي</option>
                <?php if (isset($all_job_titles) && !empty($all_job_titles)): ?>
                    <?php foreach ($all_job_titles as $row):
                        $sel = "";
                        if (isset($client_data->job_title_id_fk)) {
                            if ($client_data->job_title_id_fk == $row->job_id) {
                                $sel = "selected";
                            }
                        }  ?>
                        <option value="<?=$row->job_id."##".$row->activity_id_fk?>" <?=$sel?>><?=$row->ar_title?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المسمى الوظيفي</option>
                <?php endif ?>
            </select>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>اجمالى سنوات الخبرة</label>
            <select name="Pdata_client[experience_years]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                <option value="">اختر العدد</option>
                <?php if (isset($all_experience_yearss)):?>
                    <?php foreach ($all_experience_yearss as $row):
                        $sel = "";
                        if (isset($client_data->job_title_id_fk)) {
                            if ($client_data->experience_years == $row->id_definition) {
                                $sel = "selected";
                            }
                        }?>
                        <option value="<?=$row->id_definition?>" <?=$sel?>><?=$row->ar_title?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف العدد</option>
                <?php endif ?>
            </select>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>نبذة عنك</label>
            <textarea name="Pdata[about]" class="form-control" rows="5"
                      placeholder="يظهر للشركات الباحثة عنك"
                      data-validation="required"><?=$profile_data->about?></textarea>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>الخبرات الوظيفية</label>
            <textarea name="Pdata_client[experience]" class="form-control" rows="5"
                      placeholder="يظهر للشركات الباحثة عنك"
                      data-validation="required">
                                            <?=(isset($client_data->experience))? $client_data->experience:"";?>
                                        </textarea>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>تحميل صورة شخصية</label>
            <?php if ( !empty($profile_data->logo) && $profile_data->logo != null) { ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"
                       data-default-file="<?php echo base_url() .IMAGEPATH .$profile_data->logo  ?>"/>
            <?php } else { ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"/>
            <?php } ?>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>تحميل صورة الغلاف</label>
            <?php if ( !empty($profile_data->banner) && $profile_data->banner != null) { ?>
                <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"
                       data-default-file="<?php echo base_url() .IMAGEPATH .$profile_data->banner  ?>"/>
            <?php } else { ?>
                <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"/>
            <?php } ?>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>تحميل السيرة الذاتية</label>
            <?php if ( isset($client_data->cv_file)) {
                $filePath = base_url().'read?file='.$client_data->cv_file ;
                $downPath = base_url().'read?file='.$client_data->cv_file ;
                $target = 'target="_blank"';
                ?>
                <input type="file" id="input-file-now-custom-1" name="cv_file" class="dropify"
                       data-default-file="<?php echo base_url() .FILESPATHS .$client_data->cv_file  ?>"/>

            <?php } else { ?>
                <input type="file" id="input-file-now-custom-1" name="cv_file" accept="application/pdf" class="dropify"/>
            <?php } ?>

        </div>
    </div>
    <?php $this->load->view('backend/tabs/tab_social_edit');?>
    <?php $this->load->view('backend/tabs/tab_settings');?>
</div>

<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions--solid">
        <div class="row">
            <div class="col-lg-6">
                <button type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"
                        class="btn btn-primary">
                    <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?>
                </button>
                <!--     <button type="reset" class="btn btn-secondary">Cancel</button>-->
            </div>
            <div class="col-lg-6 m--align-right">
                <!--  <button type="reset" class="btn btn-danger">Delete</button>-->
            </div>
        </div>
    </div>
</div>
<?= form_close()?>



