<div class="m-portlet__body">
    <?php if(isset($data_table ) && $data_table!=null && !empty($data_table)):?>
        <table id="myTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>الوظيفة </th>
                <th>الشركة </th>
                <th>النشاط </th>
                <th> نوع الدوام</th>
                <th>عدد الوظائف  </th>
                <th>الراتب  </th>
                <th>الخبرة المطلوبة  </th>
                <th>المؤهل المطلوب  </th>
                <th>  التحكم  </th>
                <th>المشاركة</th>
            </tr>
            </thead>
            <?php $x = 1; foreach($data_table as $row):?>
                <tr>
                    <td><?=$x++?></td>
                    <td><?=(isset($row->job_title->ar_title))? $row->job_title->ar_title:"غير محدد ";?></td>
                    <td><?=(isset($row->user_data->name))? $row->user_data->name:"غير محدد ";?></td>
                    <td><?=(isset($row->activity->ar_title))? $row->activity->ar_title:"غير محدد ";?></td>
                    <td><?=(isset($row->type_work->ar_title))? $row->type_work->ar_title:"غير محدد ";?></td>
                    <td><?=$row->positions?></td>
                    <td><?=$row->salary?></td>
                    <td><?=(isset($row->experience_year->ar_title))? $row->experience_year->ar_title:"غير محدد ";?></td>
                    <td><?=(isset($row->qualification->ar_title))? $row->qualification->ar_title:"غير محدد ";?></td>

                    <td class="text-center">
                        <a href="<?=base_url()."admin-job/edit/".$row->id?>">
                            <button type="button" class="btn m-btn--pill btn-info btn-sm" title="تعديل ">
                                <i class="fa fa-pen-alt fa-xs"></i></button></a>
                        <a href="<?=base_url()."admin-job/delete/".$row->id?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                            <button type="button" class="btn m-btn--pill btn-danger btn-sm" title="حذف">
                                <i class="fa fa-trash-alt fa-xs"> </i> </button></a>
                    </td>
                    
                     <td>
                          
                     <a href="http://www.facebook.com/sharer.php?u=<?=base_url()."show-job/".$row->id."/".$row->company_id_fk ?>" target="_blank"	title="Click to share">
                      <i class="fab fa-facebook-square fa-2x" style="color: blue;" aria-hidden="true"></i></a>
                     
                       <a href="http://twitter.com/share?text=An%20intersting%20blog&url=https:<?=base_url()."show-job/".$row->id."/".$row->company_id_fk ?>" target="_blank"	title="Click to post to Twitter">
                       <i class="fab fa-twitter-square fa-2x" style="color:#0080FF;" aria-hidden="true"></i></a>
                     
                      </td>
                

                </tr>
            <?php endforeach ;?>
        </table>
    <?php else:
        echo '<div class="alert alert-danger  alert-rounded">
                  <i class="ti-user"></i> لا يوجد بيانات  
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span> </button>
             </div>';
    endif;?>
</div>
