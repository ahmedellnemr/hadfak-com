<?php if($op == 'UPDTATE' ):
    $out['input']='UPDTATE';
    $out['input_title']='تعديل ';
else:
    $out['input']='INSERT';
    $out['input_title']='حفظ ';
endif?>

<?=form_open_multipart($form,["class"=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']);?>

<div class="m-portlet__body">

    <div class="form-group m-form__group row">
        <div class="col-md-6 col-sm-6">

            <label>إسم الشركة </label>
            <select name="Pjob[company_id_fk]" class="form-control selectpicker"
                    data-show-subtext="true" data-live-search="true"
                    data-validation="required" aria-required="true">
                <option value="">اختر </option>
                <?php if (isset($companies) && !empty($companies)): ?>
                    <?php foreach ($companies as $row):
                        $sel = ($one->company_id_fk == $row->user_id) ? "selected" : ""; ?>
                        <option
                            value="<?= $row->user_id ?>" <?= $sel ?>><?= $row->name ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">لا يوجد بيانات </option>
                <?php endif ?>
            </select>

        </div>

    </div>

    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-4">

            <label>عنوان الوظيفة</label>
            <select name="Pjob[job_title_id]" class="form-control" id="j-category"
                    data-validation="required" aria-required="true">
                <option value="">اختر المسمى الوظيفي</option>
                <?php if (isset($all_job_titles) && !empty($all_job_titles)): ?>
                    <?php foreach ($all_job_titles as $row):
                        $sel = ($one->job_title_id == $row->job_id) ? "selected" : ""; ?>
                        <option
                            value="<?= $row->job_id . "##" . $row->activity_id_fk ?>" <?= $sel ?>><?= $row->ar_title ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المسمى الوظيفي</option>
                <?php endif ?>
            </select>

        </div>
        <div class="col-md-4 col-sm-4">

            <label>البريد الإلكتروني للتواصل</label>
            <input type="text" name="Pjob[job_email]" value="<?= $one->job_email ?>" class="form-control"
                   data-validation="email">

        </div>
        <div class="col-md-4 col-sm-4">

            <label>نوع الدوام</label>
            <select name="Pjob[type_work_id]" class="form-control input-lg" data-validation="required"
                    aria-required="true">
                <option value="">اختر</option>
                <?php if (isset($type_work)): ?>
                    <?php foreach ($type_work as $row):
                        $sel = ($one->type_work_id == $row->id_definition) ? "selected" : ""; ?>
                        <option value="<?= $row->id_definition ?>" <?= $sel ?>><?= $row->ar_title ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المؤهل</option>
                <?php endif ?>
            </select>

        </div>
    </div>



    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-4">

            <label>عدد الوظائف </label>
            <input name="Pjob[positions]" type="text" value="<?= $one->positions ?>" class="form-control"
                   data-validation="required">

        </div>
        <div class="col-md-4 col-sm-4">

            <label>الراتب </label>
            <input name="Pjob[salary]" type="text" value="<?= $one->salary ?>" class="form-control"
                   data-validation="required">

        </div>
        <div class="col-md-4 col-sm-4">

            <label>إظهار إسم الشركة </label> <br>
            <input name="Pjob[show_company]" type="radio" value="1" <?= ($one->show_company == 1) ? "checked" : ""; ?>
                   data-validation="required"> نعم
            <input name="Pjob[show_company]" type="radio" value="2" <?= ($one->show_company == 2) ? "checked" : ""; ?>
                   data-validation="required"> لا

        </div>
    </div>

    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-4">

            <label>
                الخبرة المطلوبة</label>
            <select name="Pjob[experience_years]" class="form-control" id="j-category"
                    data-validation="required" aria-required="true">
                <option value="">اختر العدد</option>
                <?php if (isset($all_experience_yearss)): ?>
                    <?php foreach ($all_experience_yearss as $row):
                        $sel = ($one->experience_years == $row->id_definition) ? "selected" : ""; ?>
                        <option value="<?= $row->id_definition ?>" <?= $sel ?>><?= $row->ar_title ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف العدد</option>
                <?php endif ?>
            </select>

        </div>
        <div class="col-md-4 col-sm-4">

            <label>
                المؤهل المطلوب </label>
            <select name="Pjob[qualification_id_fk]" class="form-control" id="j-category"
                    data-validation="required" aria-required="true">
                <option value="">اختر المؤهل</option>
                <?php if (isset($all_qualifications)): ?>
                    <?php foreach ($all_qualifications as $row):
                        $sel = ($one->qualification_id_fk == $row->id_definition) ? "selected" : "";
                        ?>
                        <option value="<?= $row->id_definition ?>" <?= $sel ?> ><?= $row->ar_title ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المؤهل</option>
                <?php endif ?>
            </select>

        </div>
        <div class="col-md-4 col-sm-4">

            <label>النوع</label> <br>
            <input name="Pjob[gender]" type="radio" value="1" <?= ($one->gender == 1) ? "checked" : "" ?>
                   data-validation="required"> ذكر
            <input name="Pjob[gender]" type="radio" value="2" <?= ($one->gender == 2) ? "checked" : "" ?>
                   data-validation="required"> انثى
            <input name="Pjob[gender]" type="radio" value="3" <?= ($one->gender == 3) ? "checked" : "" ?>
                   data-validation="required">ذكر و انثى

        </div>
    </div>

    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-4">
            <label>وصف الوظيفة</label>
            <textarea name="Pjob[details]" class="form-control"data-validation="required" rows="10">
                            <?= $one->details ?>
            </textarea>
        </div>
        <div class="col-md-4 col-sm-4">
            <label>متطلبات الوظيفة</label>
            <textarea name="Pjob[requirements]" class="form-control" rows="10" data-validation="required">
                                <?= $one->requirements ?>
            </textarea>
        </div>
        <div class="col-md-4 col-sm-4">
            <label>الصورة </label>
            <?php if ( !empty($one->logo) && $one->logo != null) { ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"
                       data-default-file="<?php echo base_url() .IMAGEPATH .$one->logo  ?>"/>
            <?php } else { ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"/>
            <?php } ?>
        </div>
    </div>

</div>

<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions--solid">
        <div class="row">
            <div class="col-lg-6">
                <button type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"
                        class="btn btn-primary">
                    <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?>
                </button>
                <!--     <button type="reset" class="btn btn-secondary">Cancel</button>-->
            </div>
            <div class="col-lg-6 m--align-right">
                <!--  <button type="reset" class="btn btn-danger">Delete</button>-->
            </div>
        </div>
    </div>
</div>
<?= form_close()?>



