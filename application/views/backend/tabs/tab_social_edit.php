
<div class="form-group m-form__group row">
    <div class="col-md-4 col-sm-6">
        <?php if (isset($social_data)): ?>
            <input type="hidden" name="status" value="update">
        <?php else: ?>
            <input type="hidden" name="status" value="insert">
        <?php endif ?>
        <label>رابط فيسبوك Facebook</label>
        <input type="text" name="Psocial[facebook]" class="form-control"
               value="<?= (isset($social_data->facebook) ? $social_data->facebook : "") ?>">
    </div>
    <div class="col-md-4 col-sm-6">
        <label>رابط تويتر Twitter</label>
        <input type="text" name="Psocial[twitter]" class="form-control"
               value="<?= (isset($social_data->twitter) ? $social_data->twitter : "") ?>">
    </div>
    <div class="col-md-4 col-sm-6">
        <label>رابط جيميل Gmail</label>
        <input type="text" name="Psocial[gmail]" class="form-control"
               value="<?= (isset($social_data->gmail) ? $social_data->gmail : "") ?>">
    </div>
</div>
<div class="form-group m-form__group row">
    <div class="col-md-4 col-sm-6">
        <label>رابط لينكد ان Linkedin</label>
        <input type="text" name="Psocial[linkedin]" class="form-control"
               value="<?= (isset($social_data->linkedin) ? $social_data->linkedin : "") ?>">
    </div>
    <div class="col-md-4 col-sm-6">
        <label>رابط إنستجرام Instagram</label>
        <input type="text" name="Psocial[instagram]" class="form-control"
               value="<?= (isset($social_data->instagram) ? $social_data->instagram : "") ?>">
    </div>
    <div class="col-md-4 col-sm-6">
        <label>رابط يوتيوب Youtube</label>
        <input type="text" name="Psocial[youtube]" class="form-control"
               value="<?= (isset($social_data->youtube) ? $social_data->youtube : "") ?>">
    </div>
</div>


