<?php if($op == 'UPDTATE' ):
    $valid = '';
else:
    $valid = 'data-validation="required"';
endif?>
    <div class="form-group m-form__group row">

        <div class="col-md-4 col-sm-6">
            <label>إسم المستخدم</label>
            <input type="text"  name="Pdata[username]" value="<?=$profile_data->username?>" data-validation="required"
                   class="form-control unique-field" field-name="username" data-db="registrations" >
        </div>
        <div class="col-md-4 col-sm-6">
            <label>كلمة المرور الجديدة</label>
            <input type="password"  name="Pdata[password]" <?=$valid?> class="form-control"  placeholder="*********">
        </div>
        <div class="col-md-4 col-sm-6">
            <label>تأكيد كلمة المرور</label>
            <input type="password"  <?=$valid?> class="form-control"  placeholder="*********">
        </div>
    </div>
