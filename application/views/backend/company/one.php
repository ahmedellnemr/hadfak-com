<?php if($op == 'UPDTATE' ):
    $out['input']='UPDTATE';
    $out['input_title']='تعديل ';
else:
    $out['input']='INSERT';
    $out['input_title']='حفظ ';
endif?>

<?= form_open_multipart($form ,["class"=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']);  ?>

<div class="m-portlet__body">

    <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>إسم المؤسسة</label>
            <input type="text" name="Pdata[name]" class="form-control" value="<?= $profile_data->name ?>"
                   data-validation="required">
        </div>
        <div class="col-md-4 col-sm-6">
            <label>الاسم المفضل ظهوره فى الاعلان</label>
            <input type="text" name="Pdata[nickname]" class="form-control" value="<?= $profile_data->nickname ?>"
                   data-validation="required">
        </div>
      <!--  <div class="col-md-4 col-sm-6">
            <label>تاريخ الإنشاء</label>
            <?php if (isset($company_data->date_created)): ?>
                <input type="hidden" name="status" value="update">
                <input type="text" name="Pdata_company[date_created]" class="form-control datepicker"
                       value="<?= date("Y-m-d", $company_data->date_created) ?>"
                       data-validation="required">
            <?php else: ?>
                <input type="hidden" name="status" value="insert">
                <input type="text" name="Pdata_company[date_created]" class="form-control datepicker" value=""
                       data-validation="required">
            <?php endif ?>
        </div> -->
    
        <div class="col-md-4 col-sm-6">
            <label>نشاط المؤسسة</label>
            <select name="Pdata_company[activity_id_fk]" class="form-control selectpicker " id="j-category"
                    data-show-subtext="true" data-live-search="true">
                <?php if (isset($all_company_activities)): ?>
                    <?php foreach ($all_company_activities as $row):
                        $sel = "";
                        if (isset($company_data->activity_id_fk)) {
                            if ($company_data->activity_id_fk == $row->activity_id) {
                                $sel = "selected";
                            }
                        }
                        ?>
                        <option value="<?= $row->activity_id ?>" <?= $sel ?> ><?= $row->ar_title ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف نشاط</option>
                <?php endif ?>
            </select>
        </div>
        </div>
        <div class="form-group m-form__group row">
        <div class="col-md-4 col-sm-6">
            <label>عدد الموظفين</label>
            <select name="Pdata_company[number_employees]" class="form-control selectpicker " id="j-category"
                    data-show-subtext="true" data-live-search="true">
                <option value="">اختر</option>
                <?php if (isset($all_number_employees)): ?>
                    <?php foreach ($all_number_employees as $row):
                        $sel = "";
                        if (isset($company_data->number_employees)) {
                            if ($company_data->number_employees == $row->id_definition) {
                                $sel = "selected";
                            }
                        } ?>
                        <option value="<?= $row->id_definition ?>" <?= $sel ?> ><?= $row->ar_title ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المؤهل</option>
                <?php endif ?>
            </select>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>الدولة</label>
            <select name="Pdata[country_id]" class="form-control selectpicker " id="country-ids" data-show-subtext="true"
                    data-live-search="true">
                <option value="">اختر الدولة</option>
                <?php if (isset($all_countries)): ?>
                    <?php foreach ($all_countries as $row):
                        $sel = "";
                        if ($profile_data->country_id == $row->id_country) {
                            $sel = "selected";
                        } ?>
                        <option value="<?= $row->id_country ?>" <?= $sel ?>><?= $row->ar_name ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف الدول</option>
                <?php endif ?>

            </select>
        </div>
    
        <div class="col-md-4 col-sm-6">
            <label>المدينة</label>
            <select name="Pdata[city_id]" class="form-control selectpicker " id="city-ids" data-show-subtext="true"
                    data-live-search="true">
                <option value="">اختر المدينة</option>
                <?php if (isset($all_cities)): ?>
                    <?php foreach ($all_cities as $row):
                        $sel = "";
                        if ($profile_data->city_id == $row->id_city) {
                            $sel = "selected";
                        } ?>
                        <option value="<?= $row->id_city ?>" <?= $sel ?> ><?= $row->ar_city_title ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                    <option value="">أضف المدن</option>
                <?php endif ?>
            </select>
        </div>
        </div>
    <div class="form-group m-form__group row">
      <!--  <div class="col-md-4 col-sm-6">
            <label>مقر الشركة</label>
            <input name="Pdata[address]" type="text" value="<?= $profile_data->address ?>" class="form-control"
                   data-validation="required">
        </div> -->
        <div class="col-md-4 col-sm-6">
            <label>البريد الإلكتروني</label>
            <input type="text" name="Pdata[email]" value="<?= $profile_data->email ?>" data-validation="email"
                   class="form-control unique-field" field-name="email" data-db="registrations" >
        </div>
    
        <div class="col-md-4 col-sm-6">
            <label>رقم الهاتف</label>
            <input type="text" name="Pdata[phone]" value="<?= $profile_data->phone ?>"
                   class="form-control unique-field" field-name="phone" data-db="registrations" >
        </div>
        <div class="col-md-4 col-sm-6">
            <label>رقم الواتس اب</label>
            <input type="text" name="Pdata[whatsapp_num]" value="<?= $profile_data->whatsapp_num ?>"  
                   class="form-control unique-field" field-name="whatsapp_num" data-db="registrations" >
        </div>
        <!--<div class="col-md-4 col-sm-6">
            <label>مسئول التواصل مع الشركة </label>
            <input type="text" name="Pdata_company[responsible_name]" class="form-control"
                   value="<?= (isset($company_data->responsible_name)) ? $company_data->responsible_name : ""; ?>">
        </div>-->
    </div>
    <div class="form-group m-form__group row">
      <!--  <div class="col-md-4 col-sm-6">
            <label>وظيفة مسئول التواصل</label>
            <input type="text" name="Pdata_company[responsible_job]" class="form-control"
                   value="<?= (isset($company_data->responsible_job)) ? $company_data->responsible_job : ""; ?>">
        </div>
        <div class="col-md-4 col-sm-6">
            <label>رقم هاتف المسئول</label>
            <input type="number" name="Pdata_company[responsible_phone]"
                   class="form-control"
                   value="<?= (isset($company_data->responsible_phone)) ? $company_data->responsible_phone : ""; ?>">
        </div>-->
        <div class="col-md-4 col-sm-6">
            <label>نبذة عن المؤسسة</label>
            <textarea name="Pdata[about]" class="form-control" rows="7"
                      placeholder="يظهر للشركات الباحثة عنك"
                      data-validation="required"><?= $profile_data->about ?></textarea>
        </div>
    
        <div class="col-md-4 col-sm-6">
            <label>تحميل صورة شخصية</label>
            <?php if (!empty($profile_data->logo) && $profile_data->logo != null) { ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"
                       data-default-file="<?php echo base_url() . IMAGEPATH . $profile_data->logo ?>"/>
            <?php } else { ?>
                <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"/>
            <?php } ?>
        </div>
        <div class="col-md-4 col-sm-6">
            <label>تحميل صورة الغلاف</label>
            <?php if (!empty($profile_data->banner) && $profile_data->banner != null) { ?>
                <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"
                       data-default-file="<?php echo base_url() . IMAGEPATH . $profile_data->banner ?>"/>
            <?php } else { ?>
                <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"/>
            <?php } ?>
        </div>
    </div>

    <?php $this->load->view('backend/tabs/tab_social_edit');?>
    <?php $this->load->view('backend/tabs/tab_settings');?>
</div>

<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions--solid">
        <div class="row">
            <div class="col-lg-6">
                <button type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"
                        class="btn btn-primary">
                    <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?>
                </button>
                <!--     <button type="reset" class="btn btn-secondary">Cancel</button>-->
            </div>
            <div class="col-lg-6 m--align-right">
                <!--  <button type="reset" class="btn btn-danger">Delete</button>-->
            </div>
        </div>
    </div>
</div>
<?= form_close()?>



