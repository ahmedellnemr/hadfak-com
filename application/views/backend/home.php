<style>

    
    .card-shadow {
        box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
    }
</style>
    <!-- Column -->
    <div class="col-xl-4">
        <div class="">
            <input type="date" value="<?= date("Y-m-d") ?>" class="form-control"
                   onchange="getVisitDay(this.value);">
        </div>
        <br>
        <div class="card card-warning card-inverse card-shadow" style="background-color: #34bf34;margin-bottom: 15px;">
            <div class="box_dash  text-center">
                <h1 class="font-light text-white" id="daly_visit">
                    <?php if (isset($daly_visit)) {
                        echo $daly_visit;
                    } else {
                        echo 0;
                    } ?>
                </h1>
                <h6 class="text-white"><i class="fa fa-book"></i></h6>
                <h6 class="text-white">عدد الزوار خلال اليوم</h6>

            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-xl-4">
        <div class="">
            <select class="form-control" onchange="getVisitMonth(this.value);">
                <?php if (isset($arMonth) && !empty($arMonth) && $arMonth != null) {
                    foreach ($arMonth as $key => $value) {
                        ?>
                        <option value="<?= $key ?>" <?= (date("F", time()) == $key) ? "selected" : ""; ?>><?= $value ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
        </div>
        <br>
        <div class="card card-warning card-inverse card-shadow" style="background-color: #ff9c00;margin-bottom: 15px;">
            <div class="box_dash text-center">
                <h1 class="font-light text-white" id="month_visit">
                    <?php
                    if (isset($month_visit)) {
                        echo $month_visit;
                    } else {
                        echo 290;
                    } ?>
                </h1>
                <h6 class="text-white"><i class="fa fa-book"></i></h6>
                <h6 class="text-white">عدد الزوار خلال الشهر </h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-xl-4" style="margin-top: auto;">
        <div class="card card-info card-inverse card-shadow" style="background-color: #358cfb;margin-bottom: 15px;">
            <div class="box_dash text-center">
                <h1 class="font-light text-white" id="month_visit">
                    <?php
                    if (isset($total_visit)) {
                        echo $total_visit;
                    } else {
                        echo 290;
                    } ?>
                </h1>
                <h6 class="text-white"><i class="fa fa-book"></i></h6>
                <h6 class="text-white">إجمالى عدد الزوار </h6>
            </div>
        </div>
    </div>

    <hr>

    <div class="col-xl-4">

        <!--begin:: Widgets/Activity-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light  m-portlet--rounded-force">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                            المستخدمين
                        </h3>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="m-widget17">
                    <div class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                        <div class="m-widget17__chart" style="height:200px;">
                            <canvas id="m_chart_activities"></canvas>
                        </div>
                    </div>
                    <div class="m-widget17__stats">
                        <div class="m-widget17__items m-widget17__items-col1">
                            <div class="m-widget17__item">
                            <span class="m-widget17__icon">
                                <i class="flaticon-user m--font-brand"></i>
                            </span>
                                <span class="m-widget17__subtitle">
															أفراد
							</span>
                                <span class="m-widget17__desc">
															<?= $client ?>
							</span>
                            </div>
                        </div>
                        <div class="m-widget17__items m-widget17__items-col2">
                            <div class="m-widget17__item">
                             <span class="m-widget17__icon">
								<i class="flaticon-home m--font-info"></i>
                             </span>
                                <span class="m-widget17__subtitle">
															شركات
                             </span>
                                <span class="m-widget17__desc">
															<?= $company ?>
                             </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end:: Widgets/Activity-->
    </div>
    <div class="col-xl-4">

        <!--begin:: Widgets/Activity-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light  m-portlet--rounded-force">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                            الوظائف
                        </h3>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="m-widget17">
                    <div class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                        <div class="m-widget17__chart" style="height:200px;">
                            <canvas id="m_chart_activities"></canvas>
                        </div>
                    </div>
                    <div class="m-widget17__stats">
                        <div class="m-widget17__items m-widget17__items-col1">
                            <div class="m-widget17__item">
                            <span class="m-widget17__icon">
                                <i class="fa fa-address-card"></i>
                            </span>
                                <span class="m-widget17__subtitle">
															الوظائف
							</span>
                                <span class="m-widget17__desc">
															<?= $job ?>
							</span>
                            </div>
                            <div class="m-widget17__item">
                             <span class="m-widget17__icon">
								<i class="fa fa-book"></i>
                             </span>
                                <span class="m-widget17__subtitle">
															النشاطات
                             </span>
                                <span class="m-widget17__desc">
															<?= $activaty ?>
                             </span>
                            </div>
                        </div>
                        <div class="m-widget17__items m-widget17__items-col2">

                            <div class="m-widget17__item">
                             <span class="m-widget17__icon">
								<i class="fa fa-clipboard-list"></i>
                             </span>
                                <span class="m-widget17__subtitle">
															طلبات التوظيف
                             </span>
                                <span class="m-widget17__desc">
															<?= $apply_job ?>
                             </span>
                            </div>
                            <div class="m-widget17__item">
                             <span class="m-widget17__icon">
								<i class="fa fa-book"></i>
                             </span>
                                <span class="m-widget17__subtitle">
															المسميات الوظيفية
                             </span>
                                <span class="m-widget17__desc">
															<?= $job_title ?>
                             </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end:: Widgets/Activity-->
    </div>
    <div class="col-xl-4">

        <!--begin:: Widgets/Authors Profit-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            احدث اشتراكات
                        </h3>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="m-widget4">

                    <?php if (isset($users) && !empty($users)): ?>
                        <?php foreach ($users as $row): ?>
                            <div class="m-widget4__item">
                                <div class="m-widget4__img m-widget4__img--logo">
                                    <?php if (!empty($row->logo) && is_file(IMAGEPATH . $row->logo)): ?>
                                        <img src="<?= base_url() . IMAGEPATH . $row->logo ?>" class="img-responsive"
                                             alt=""/>
                                    <?php else: ?>
                                        <img src="<?= base_url() . FAVICONPATH . USERIMAGE ?>" class=img-responsive"
                                             alt=""/>
                                    <?php endif ?>
                                </div>
                                <div class="m-widget4__info">
                                <span class="m-widget4__title">
                                    <?= $row->name ?>
                                </span><br>
                                    <span class="m-widget4__sub">
                                    <?= $row->email ?>
                                </span>
                                </div>
                                <span class="m-widget4__ext">
                                <span class="m-widget4__number m--font-brand">
                                    <?= date("Y-m-d", $row->created_at) ?></span>
                            </span>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>

                </div>
            </div>
        </div>

        <!--end:: Widgets/Authors Profit-->
    </div>




