<?php  if(isset($_SESSION["user_data"]['is_web_login']) && $_SESSION["user_data"]['user_type'] == 2  ): ?>
<style>
.ur-detail-wrap {
    border: 3px solid #07b107;
}
.sidebar  .job-detail-des{
    box-shadow: 0px 0px 10px 2px rgba(0,0,0,0.1);
    border: 3px solid #07b107;
    padding: 10px;
 }
</style>

<?php endif ?>
<section class="inner-header-title" style="background-image:url(<?=base_url().WEBASSETS?>img/banner-10.jpg);">
    <div class="container">
        <h1> <?=(isset($one->job_title->ar_title)? $one->job_title->ar_title:"تفاصيل الوظيفة ")?></h1>
    </div>
</section>


<section class="detail-desc">
    <div class="container">

        <div class="ur-detail-wrap top-lay">

            <div class="ur-detail-box">

                <div class="ur-thumb">
                    <?php if ( !empty($profile_data->logo) && is_file(IMAGEPATH . $profile_data->logo)): ?>
                        <img src="<?= base_url() . IMAGEPATH . $profile_data->logo ?>" class="img-responsive" alt=""/>
                    <?php else: ?>
                        <img src="<?=base_url().FAVICONPATH . HLOGO ?>" class="img-responsive" alt="">
                    <?php endif ?>

                </div>
                <div class="ur-caption">
                    <h4 class="ur-title"><?=$profile_data->name?></h4>
                    <p class="ur-location"><i class="fa fa-map-marker "></i>
                        <?=(isset($profile_data->country->ar_name))? $profile_data->country->ar_name:"";?> -
                        <?=(isset($profile_data->city->ar_city_title ))? $profile_data->city->ar_city_title :"";?> -
                        <?=$profile_data->address?>
                    </p>
                    <span class="ur-designation"><i class="fa fa-home "></i>
                         <?=(isset($company_data->activity->ar_title)? $company_data->activity->ar_title:"")?>
                        </span>
                </div>

            </div>

            <div class="ur-detail-btn">

                <?php  if(isset($_SESSION["user_data"]['is_web_login'])):?>

                <?php  if($_SESSION["user_data"]['user_type'] == 1  ): ?>
                    <?php if (isset($apply_case) && $apply_case == true): ?>
                        <a id="apply-it" job="<?= $one->id ?>" com="<?= $profile_data->user_id ?>"
                           class="btn btn-warning mrg-bot-10 full-width">
                            <i class="ti-star mrg-r-5"></i>التقدم إلى الوظيفة</a><br>
                    <?php else: ?>
                        <a id="apply-it" job="false" com="false"
                           class="btn btn-danger mrg-bot-10 full-width">
                            <i class="ti-star mrg-r-5"></i>لقد تقدمت لهذه الوظيفة</a><br>
                    <?php endif ?>
                <?php endif ?>

                <?php else: ?>
                    <a id="be-login"
                       class="btn btn-warning mrg-bot-10 full-width">
                        <i class="ti-star mrg-r-5"></i>التقدم إلى الوظيفة</a><br>
                <?php endif ?>
            </div>

        </div>

    </div>
</section>


<section class="full-detail-description full-detail">
    <div class="container">
        <!-- Job Description -->
        <div class="col-md-8 col-sm-12">
            <div class="full-card">

                <div class=" row-bottom mrg-0">
                    <h2 class="detail-title">تفاصيل الوظيفة</h2>
                    <ul class="job-detail-des">
                        <li><span>عنوان الوظيفة:</span> <?=(isset($one->job_title->ar_title)? $one->job_title->ar_title:"")?></li>
                        <li><span>المجال:</span><?=(isset($one->activity->ar_title)? $one->activity->ar_title:"")?></li>
                        <li><span>نوع الدوام:</span><?=(isset($one->type_work->ar_title)? $one->type_work->ar_title:"")?></li>
                        <li><span>مقر العمل:</span><?=(isset($profile_data->city->ar_city_title ))? $profile_data->city->ar_city_title :"";?></li>
                        <li><span>تاريخ الإعلان:</span><?=date("Y-m-d",$one->created_at)?></li>
                        <li><span>عدد الموظفين المطلوبين:</span><?=$one->positions?></li>
                    </ul>
                </div>



                <div class=" row-bottom mrg-0">
                    <h2 class="detail-title">وصف الوظيفة</h2>
                    <p><?=$one->details?></p>
                   </div>

                <div class=" row-bottom mrg-0">
                    <h2 class="detail-title">متطلبات الوظيفة</h2>
                    <p>المهارات والخبرات المطلوبة.</p>
                    <ul class="detail-list">
                        <li><?=$one->requirements?></li>
                    </ul>
                </div>
                <div class=" row-bottom mrg-0">
                    <h2 class="detail-title">نبذة عن الشركة</h2>
                  <?=$profile_data->about?>

                </div>

                <div class=" row-bottom mrg-0">
                    <h2 class="detail-title">بيانات الشركة</h2>
                    <?php  if(isset($_SESSION["user_data"]['is_web_login'])):?>
                    <ul class="job-detail-des">
                        <li><span>العنوان:</span> <?=$profile_data->address?></li>
                        <li><span>المدينة:</span><?=(isset($profile_data->city->ar_city_title ))? $profile_data->city->ar_city_title :"";?> </li>
                        <li><span>الدولة:</span><?=(isset($profile_data->country->ar_name))? $profile_data->country->ar_name:"";?> </li>
                        <li><span>هاتف:</span><?=$profile_data->phone?></li>
                       <!-- <li><span>فاكس:</span>(622) 123 456</li>-->
                        <li><span>البريد الإلكتروني:</span><?=$profile_data->email?></li>
                    </ul>
                    

                    <?php else: ?>
                     <div class="alert alert-danger"><p style="margin-bottom: 0px;">يتوجب عليك تسجيل الدخول أولا حتى تتمكن من رؤية التفاصيل</p></div>
                
                   <?php endif ?>
                
                </div>


            </div>
        </div>
        <!-- End Job Description -->

        <!-- Start Sidebar -->
        <div class="col-md-4 col-sm-12">
            <div class="sidebar right-sidebar">


                <div class="side-widget">
                    <h2 class="side-widget-title">نظرة عامة على الوظيفة</h2>
                    <div class="widget-text padd-0">
                        <div class="ur-detail-wrap">
                            <div class="ur-detail-wrap-body ptop-20">
                                <ul class="ove-detail-list">

                                    <li>
                                        <i class="fa fa-money"></i>
                                        <h5>الراتب</h5>
                                        <span><?=$one->salary?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-venus-mars"></i>
                                        <h5>النوع </h5>
                                        <span>
                                            <?php
                                            if ($one->gender == 1)
                                                echo "ذكر";
                                              elseif ($one->gender == 2                           ) {
                                                  echo "انثى";
                                              }
                                              else {
                                                  echo "ذكر و انثى";
                                              }
                                            ?>
                                        </span>
                                    </li>

                                    <li>
                                        <i class="fa fa-plus-square-o "></i>
                                        <h5>الخبرة المطلوبة</h5>
                                        <span>
                                             <?=(isset($one->experience_year->ar_title))? $one->experience_year->ar_title:"مؤهل"?>

                                        </span>
                                    </li>

                                    <li>
                                        <i class="fa fa-home"></i>
                                        <h5>المجال</h5>
                                        <span><?=(isset($one->activity->ar_title))? $one->activity->ar_title:"وظيفة"?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-book"></i>
                                        <h5>المؤهل المطلوب</h5>
                                        <span>
                                           <?=(isset($one->qualification->ar_title))? $one->qualification->ar_title:" سنوات"?>
                                        </span>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="side-widget">
                    <h2 class="side-widget-title">بيانات الشركة</h2>
                    <div class="widget-text padd-0">
                    <?php  if(isset($_SESSION["user_data"]['is_web_login'])):?>
                        <ul class="job-detail-des">
                            <li><span>العنوان:</span> <?=$profile_data->address?></li>
                            <li><span>المدينة:</span><?=(isset($profile_data->city->ar_city_title ))? $profile_data->city->ar_city_title :"";?> </li>
                            <li><span>الدولة:</span><?=(isset($profile_data->country->ar_name))? $profile_data->country->ar_name:"";?> </li>
                            <li><span>هاتف:</span><?=$profile_data->phone?></li>
                            <!-- <li><span>فاكس:</span>(622) 123 456</li>-->
                            <li><span>البريد الإلكتروني:</span><?=$profile_data->email?></li>
                        </ul>
                        <?php else: ?>
                    <div class="alert alert-danger"><p style="margin-bottom: 0px;">يتوجب عليك تسجيل الدخول أولا حتى تتمكن من رؤية التفاصيل</p></div>
                
                   <?php endif ?>
                    </div>
                </div>

             <!--   <div class="side-widget">
                    <h2 class="side-widget-title">إعلان</h2>
                    <div class="widget-text padd-0">
                        <div class="ad-banner">
                            <img src="http://via.placeholder.com/320x285" class="img-responsive" alt="">
                        </div>
                    </div>
                </div>-->

            </div>
        </div>
        <!-- End Sidebar -->
    </div>
</section>


