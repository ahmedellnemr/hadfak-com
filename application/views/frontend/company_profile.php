
<?php if (!empty($profile_data->banner) && is_file(IMAGEPATH . $profile_data->banner)){
    $banner  = base_url().IMAGEPATH .$profile_data->banner;
 }
 else{
    if(!empty($all_imgs[0]->company_image)){ $banner  =  base_url() .IMAGEPATH .$all_imgs[0]->company_image; }
    else{$banner  = base_url().WEBASSETS .'img/banner-10.jpg';}
      
 }
   


 
?>
<section class="inner-header-title" style="background-image:url(<?=$banner?>);">
    <div class="container">
        <h1><?=$profile_data->name?></h1>
    </div>
</section>


<div class="clearfix"></div>
<!-- Title Header End -->

<!-- Candidate Profile Start -->
<section class="detail-desc advance-detail-pr gray-bg">
    <div class="container">
        <div class="ur-detail-wrap create-kit padd-bot-0">

            <div class="row">
                <div class="detail-pic">
                    <?php if ( !empty($profile_data->logo) && is_file(IMAGEPATH . $profile_data->logo)): ?>
                        <img src="<?= base_url() . IMAGEPATH . $profile_data->logo ?>" class="img" alt=""/>
                    <?php else: ?>
                        <img src="<?= base_url() . FAVICONPATH ?>company.png" class="img" alt=""/>
                    <?php endif ?>
                    <a href="#" class="detail-edit" title="edit"><i class="fa fa-pencil"></i></a>
                </div>
                <div class="detail-status">
                    <span> <?=($profile_data->is_login == 1)? "Active Now":"Not Active"?> </span>
                </div>
            </div>

            <div class="row bottom-mrg">
                <div class="col-md-12 col-sm-12">
                    <div class="advance-detail detail-desc-caption">
                        <h4><?=$profile_data->name?></h4>
                        <span class="designation"><?=(isset($company_data->activity->ar_title)? $company_data->activity->ar_title:"")?></span>
                        <ul>
                            <li><strong class="j-view"><?=(isset($company_data->number_employees_data->ar_title)? $company_data->number_employees_data->ar_title:"1")?></strong>موظفين الشركة</li>
                            <li><strong class="j-applied"><?=$total_com_jobs?></strong>الوظائف المتاحة</li>
                            <li>
                                    <?php if ($total_jobs > 0): ?>
                                        <a href="<?= base_url() . "get-appliers?type=appliers&company_id=" . $profile_data->user_id ?>">
                                            <strong class="j-shared"> <?= $total_jobs ?> </strong>
                                        </a>
                                    <?php else: ?>
                                <strong class="j-shared"> <?= $total_jobs ?></strong>
                                    <?php endif ?>
                                المتقدمين للوظائف </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row no-padd">
                <div class="detail pannel-footer">
                    <div class="col-md-5 col-sm-5">
                        <ul class="detail-footer-social">
                            <?php if (isset($social_data)): ?>
                                <li><a href="<?= $social_data->facebook ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?= $social_data->gmail ?>"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="<?= $social_data->twitter ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?= $social_data->linkedin ?>"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="<?= $social_data->instagram ?>"><i class="fa fa-instagram"></i></a></li>
                            <?php else: ?>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <?php endif ?>
                        </ul>
                    </div>
                    <div class="col-md-7 col-sm-7">
                        <div class="detail-pannel-footer-btn pull-right">
                            <a href="#simple-design-tab" class="footer-btn grn-btn" title="">تعديل الان</a>
                            <?php if (isset($company_data->activity_id_fk)): ?>
                                <a href="<?= base_url() . "publish-job/" . $profile_data->user_id ?>"
                                   class="footer-btn blu-btn" title="">اعلان عن وظيفة</a>
                            <?php else: ?>
                                <a class="footer-btn blu-btn" title="يرجى إستكمال بياناتك  لنشر وظيفة">اعلان عن وظيفة</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="full-detail-description full-detail gray-bg">
    <div class="container">
        <div class="col-md-12 col-sm-12 no-pxs">
            <div class="full-card">
                <div class="deatil-tab-employ tool-tab">
                    <ul class="nav simple nav-tabs" id="simple-design-tab" role="tablist">
                        <li role="presentation" class="active"><a href="#about" role="tab"  data-toggle="tab">نبذة عن المؤسسة</a></li>
                        <li role="presentation"><a href="#address" role="tab"  data-toggle="tab">معلومات التواصل</a></li>

                        <li role="presentation"><a href="#post-job" role="tab"  data-toggle="tab">الوظائف التى تم نشرها</a></li>

                        <li role="presentation"><a href="#apply-job" role="tab"  data-toggle="tab">طلبات التوظيف</a></li>

                        <li role="presentation"><a href="#friends" role="tab"  data-toggle="tab">أشخاص مقترحة</a></li>

                        <li role="presentation"><a id="lnk2" href="#profile-edit" role="tab"  data-toggle="tab">تعديل الصفحة الشخصية</a></li>
                        <li role="presentation"><a href="#social-edit" role="tab"  data-toggle="tab">تعديل بيانات التواصل الإجتماعي</a></li>
                        <li role="presentation"><a href="#settings" role="tab"  data-toggle="tab">الإعدادات</a></li>


                    </ul>
                    <!-- Start All Sec -->
                    <div class="tab-content">
                        <!-- Start About Sec -->
                        <div id="about" class="tab-pane fade in active">
                            <?php $this->load->view('frontend/tabs/tab_about');?>
                        </div>
                        <!-- End About Sec -->

                        <!-- Start Address Sec -->
                        <div id="address" class="tab-pane fade">
                            <?php $this->load->view('frontend/tabs/tab_address');?>
                        </div>
                        <!-- End Address Sec -->


                        <!-- Start Job List -->
                        <div id="post-job" class="tab-pane fade">
                            <div class="row">
                                <h3>لديك <?=count($com_jobs)?> وظيفة تم نشرها</h3>
                                <?php if (isset($com_jobs) && !empty($com_jobs)) { ?>
                                    <div class="row">
                                        <?php foreach ($com_jobs as $row): ?>
                                            <article>
                                                <div class="mng-company">
                                                    <div class="col-md-2 col-sm-2 col-xs-3 no-pxs">
                                                        <div class="mng-company-pic">
                                                            <?php if ( !empty($row->logo) && is_file(IMAGEPATH . $row->logo)): ?>
                                                                <img src="<?= base_url() . IMAGEPATH . $row->logo ?>" class="img-responsive" alt=""/>
                                                            <?php else: ?>
                                                                <img src="<?=base_url().FAVICONPATH?>job.png" class="img-responsive" alt="">
                                                            <?php endif ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 col-xs-9 pad-4">
                                                        <div class="mng-company-name">
                                                            <a href="<?=base_url()."show-job/".$row->id."/".$profile_data->user_id?>">
                                                                <h5> <span class="cmp-tagline"><?=$row->ar_title?></span></h5>
                                                            </a>
                                                            <span class="cmp-time"><?=calculate_from_time($row->created_at)?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-xs-6 pad-4">
                                                        <div class="mng-company-location">
                                                            <p>
                                                                <i class="fa fa-map-marker"></i>
                                                                <?=$profile_data->address?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-sm-1 col-xs-6">
                                                        <div class="mng-company-action">
                                                            <a href="<?=base_url()."edit-job/".$profile_data->user_id."/".$row->id?>"><i class="fa fa-edit"></i> </a>
                                                           <!--  -->
                                                            <a onclick="delete_con(this,'<?=$profile_data->user_id?>','<?=$row->id?>');">
                                                                <i class="fa fa-trash-o"></i> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--  <span class="tg-themetag tg-featuretag">Premium</span>-->
                                            </article>
                                        <?php endforeach ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <!--  <div class="row">
                                  <ul class="pagination">
                                      <li><a href="#">«</a></li>
                                      <li class="active"><a href="#">1</a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">4</a></li>
                                      <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                                      <li><a href="#">»</a></li>
                                  </ul>
                              </div>-->
                        </div>
                        <!-- End Job List -->

                        <!-- Start Friend List -->
                        <div id="apply-job" class="tab-pane fade">


                            <h3>لديك <?=count($applaiers)?> طلب توظيف </h3>
                            <?php if (isset($applaiers) && !empty($applaiers)) { ?>
                                <div class="row">
                                    <?php foreach ($applaiers as $row): ?>
                                        <article>
                                            <div class="mng-company">
                                                <div class="col-md-2 col-sm-2">
                                                    <div class="mng-company-pic">
                                                        <?php if ( isset($row->user_data->logo)
                                                            && !empty($row->user_data->logo)
                                                            && is_file(IMAGEPATH . $row->user_data->logo)): ?>
                                                            <img src="<?= base_url() . IMAGEPATH . $row->user_data->logo ?>" class="img-responsive" alt=""/>
                                                        <?php else: ?>
                                                            <img src="<?=base_url().FAVICONPATH . USERIMAGE ?>" class="img-responsive" alt="">
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-sm-5">
                                                    <div class="mng-company-name">
                                                        <?php if(isset($row->user_data->name)){?>
                                                            <a href="<?=base_url()."show-user/".$row->user_data->user_id?>">
                                                                <h5> <span class="cmp-tagline"><?=$row->user_data->name?></span></h5>
                                                            </a>
                                                            <span class="cmp-time"><?=calculate_from_time($row->created_at)?></span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="mng-company-location">
                                                        <p>
                                                            <i class="fa fa-list"></i>
                                                            <?=(isset($row->job->ar_title))? $row->job->ar_title:""?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-1 col-sm-1">
                                                    <div class="mng-company-action">
                                                        <a href="<?=base_url()."show-user/".$row->user_data->user_id?>">
                                                            <i class="fa fa-eye"></i> </a>
                                                        <?php if (isset($row->client->cv_file) && !empty($row->client->cv_file)
                                                         && is_file(FILESPATHS . $row->client->cv_file) ): ?>
                                                            <a href="<?= base_url() . "download?file=" . $row->client->cv_file ?>">
                                                                <i class="fa fa-download"></i> </a>
                                                        <?php endif ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--  <span class="tg-themetag tg-featuretag">Premium</span>-->
                                        </article>
                                    <?php endforeach ?>
                                </div>
                            <?php } ?>
                            <!--  <div class="row">
                                  <ul class="pagination">
                                      <li><a href="#">«</a></li>
                                      <li class="active"><a href="#">1</a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">4</a></li>
                                      <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                                      <li><a href="#">»</a></li>
                                  </ul>
                              </div>-->
                        </div>
                        <!-- End Friend List -->
                        <!-- Start Friend List -->
                        <div id="friends" class="tab-pane fade">
                            <div class="row">
                                <?php
                                $pass["div"] = 4;
                                $this->load->view('frontend/load/search_person', $pass); ?>
                            </div>
                            <!--  <div class="row">
                                  <ul class="pagination">
                                      <li><a href="#">«</a></li>
                                      <li class="active"><a href="#">1</a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">4</a></li>
                                      <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                                      <li><a href="#">»</a></li>
                                  </ul>
                              </div>-->
                        </div>
                        <!-- End Friend List -->


                        <div id="profile-edit" class="tab-pane fade">
                            <div class="row no-mrg">
                                <h3>تعديل الصفحة الشخصية</h3>
                                <div class="edit-pro">
                                    <?= form_open_multipart('update-profile/'.$profile_data->user_id);  ?>
                                    <div class="col-md-4 col-sm-6">
                                        <label>إسم المؤسسة</label>
                                        <input type="text" name="Pdata[name]" class="form-control" value="<?=$profile_data->name?>"  data-validation="required">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>الاسم المفضل ظهوره فى الاعلان</label>
                                        <input type="text" name="Pdata[nickname]" class="form-control" value="<?=$profile_data->nickname?>"  data-validation="required">
                                    </div>
                                    <!--<div class="col-md-4 col-sm-6">
                                        <label>تاريخ الإنشاء</label>
                                        <?php //if (isset($company_data->date_created)): ?>
                                            <input type="hidden" name="status" value="update">
                                            <input type="text" name="Pdata_company[date_created]" class="form-control datepicker" value="<?=date("Y-m-d",$company_data->date_created)?>"
                                                   data-validation="required">
                                        <?php //else: ?>
                                            <input type="hidden" name="status" value="insert">
                                            <input type="text" name="Pdata_company[date_created]" class="form-control datepicker" value=""
                                                   data-validation="required">
                                        <?php //endif ?>
                                    </div>-->
                                    <div class="col-md-4 col-sm-6">
                                        <label>نشاط المؤسسة</label>
                                        <select name="Pdata_company[activity_id_fk]" class="form-control selectpicker " id="j-category"  data-show-subtext="true" data-live-search="true" >
                                                <?php if (isset($all_company_activities)): ?>
                                                    <?php foreach ($all_company_activities as $row):
                                                        $sel = "";
                                                        if(isset($company_data->activity_id_fk)){
                                                            if($company_data->activity_id_fk == $row->activity_id){
                                                                $sel = "selected";
                                                            }
                                                        }
                                                        ?>
                                                    <option value="<?=$row->activity_id?>" <?=$sel?> ><?=$row->ar_title?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف نشاط</option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>عدد الموظفين</label>
                                        <select name="Pdata_company[number_employees]" class="form-control selectpicker " id="j-category"  data-show-subtext="true" data-live-search="true" >
                                            <option value="">اختر </option>
                                            <?php if (isset($all_number_employees)): ?>
                                                <?php foreach ($all_number_employees as $row):
                                                    $sel = "";
                                                    if (isset($company_data->number_employees)) {
                                                        if ($company_data->number_employees == $row->id_definition) {
                                                            $sel = "selected";
                                                        }
                                                    } ?>
                                                    <option value="<?=$row->id_definition?>"  <?=$sel?> ><?=$row->ar_title?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف المؤهل</option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>الدولة</label>
                                        <select name="Pdata[country_id]" class="form-control selectpicker " id="country-ids"  data-show-subtext="true" data-live-search="true" >
                                            <option value="">اختر الدولة</option>
                                            <?php if (isset($all_countries)): ?>
                                                <?php foreach ($all_countries as $row):
                                                    $sel = "";
                                                    if($profile_data->country_id == $row->id_country){
                                                        $sel = "selected";
                                                    }?>
                                                    <option value="<?=$row->id_country?>" <?=$sel?>><?=$row->ar_name?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف الدول</option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>المدينة</label>
                                        <select name="Pdata[city_id]" class="form-control selectpicker " id="city-ids"  data-show-subtext="true" data-live-search="true" >
                                            <?php if(!empty($profile_data->city_id)):?>
                                                <option value="">اختر المدينة</option>
                                                <?php if (isset($all_cities)): ?>
                                                    <?php foreach ($all_cities as $row):
                                                        $sel = "";
                                                        if($profile_data->city_id == $row->id_city){
                                                            $sel = "selected";
                                                        }?>
                                                        <option value="<?=$row->id_city?>" <?=$sel?> ><?=$row->ar_city_title?></option>
                                                    <?php endforeach ?>
                                                <?php else: ?>
                                                    <option value="">أضف المدن</option>
                                                <?php endif ?>
                                            <?php else:?>
                                                <option value="">اختر الدولة أولا  </option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <!--<div class="col-md-4 col-sm-6">
                                        <label>مقر الشركة</label>
                                        <input name="Pdata[address]" type="text" value="<?=$profile_data->address?>" class="form-control" data-validation="required">
                                    </div>-->
                                    <div class="col-md-4 col-sm-6">
                                        <label>البريد الإلكتروني</label>
                                        <input type="text" name="Pdata[email]" value="<?=$profile_data->email?>"
                                               class="form-control unique-field" field-name="email" data-db="registrations" data-validation="email">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>رقم الهاتف</label>
                                        <input type="text" name="Pdata[phone]" value="<?=$profile_data->phone?>"
                                               class="form-control unique-field" field-name="phone" data-db="registrations"
                                                >
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>رقم الواتس اب</label>
                                        <input type="text" name="Pdata[whatsapp_num]" value="<?=$profile_data->whatsapp_num?>"
                                               class="form-control unique-field" field-name="whatsapp_num" data-db="registrations"
                                               >
                                    </div>


                                   <!-- <div class="col-md-4 col-sm-6">
                                        <label>مسئول التواصل مع الشركة </label>
                                        <input type="text" name="Pdata_company[responsible_name]" class="form-control"
                                               value="<?= (isset($company_data->responsible_name)) ? $company_data->responsible_name : ""; ?>">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>وظيفة مسئول التواصل</label>
                                        <input type="text" name="Pdata_company[responsible_job]" class="form-control"
                                               value="<?= (isset($company_data->responsible_job)) ? $company_data->responsible_job : ""; ?>">
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <label>رقم هاتف المسئول</label>
                                        <input type="number" name="Pdata_company[responsible_phone]"
                                               class="form-control"
                                               value="<?= (isset($company_data->responsible_phone)) ? $company_data->responsible_phone : ""; ?>">
                                    </div>-->

                                    <div class="clearfix"></div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>نبذة عن المؤسسة</label>
                                        <textarea name="Pdata[about]" class="form-control"
                                                  placeholder="يظهر للشركات الباحثة عنك"
                                                  data-validation="required"><?=$profile_data->about?></textarea>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>تحميل صورة شخصية</label>
                                        <?php if ( !empty($profile_data->logo) && $profile_data->logo != null) { ?>
                                            <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"
                                                   data-default-file="<?php echo base_url() .IMAGEPATH .$profile_data->logo  ?>"/>
                                        <?php } else { ?>
                                            <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"/>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>تحميل صورة الغلاف</label>
                                        <?php if ( !empty($profile_data->banner) && $profile_data->banner != null) { ?>
                                            <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"
                                                   data-default-file="<?php echo base_url() .IMAGEPATH .$profile_data->banner  ?>"/>
                                        <?php } else { ?>
                                            <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"/>
                                        <?php } ?>
                                    </div>

                                    <div class="col-sm-12 text-center mtop-20">
                                        <button type="submit" name="add" value="add"  class="btn btn-primary blu-btn update-btn">تعديل الأن</button>
                                    </div>
                                    <?=form_close()?>
                                </div>
                            </div>
                        </div>

                        <div id="social-edit" class="tab-pane fade">
                            <?php $this->load->view('frontend/tabs/tab_social_edit');?>
                        </div>
                        <!-- Start Settings -->
                        <div id="settings" class="tab-pane fade">
                            <?php $this->load->view('frontend/tabs/tab_settings');?>
                        </div>
                        <!-- End Settings -->



                    </div>
                    <!-- Start All Sec -->
                </div>
            </div>
        </div>
    </div>
</section>

