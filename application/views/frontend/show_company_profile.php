
<?php if (!empty($profile_data->banner) && is_file(IMAGEPATH . $profile_data->banner)){
    $banner  = base_url().IMAGEPATH .$profile_data->banner;
 }
 else{
    if(!empty($all_imgs[0]->company_image)){ $banner  =  base_url() .IMAGEPATH .$all_imgs[0]->company_image; }
    else{$banner  = base_url().WEBASSETS .'img/banner-10.jpg';}
      
 }
 

 
?>
<!-- Title Header Start -->
<section class="inner-header-title" style="background-image:url(<?=$banner?>);">
    <div class="container">
        <h1><?=$profile_data->name?></h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Title Header End -->

<!-- Company Detail Start -->
<section class="detail-desc">
    <div class="container">

        <div class="ur-detail-wrap top-lay">

            <div class="ur-detail-box">

                <div class="ur-thumb">
                    <?php if ( !empty($profile_data->logo) && is_file(IMAGEPATH . $profile_data->logo)): ?>
                        <img src="<?= base_url() . IMAGEPATH . $profile_data->logo ?>" class="img-responsive" alt=""/>
                    <?php else: ?>
                        <img src="<?= base_url() . FAVICONPATH ?>company.png" class="img-responsive" alt=""/>
                    <?php endif ?>
                </div>
                <div class="ur-caption">
                    <h4 class="ur-title"><?=$profile_data->name?></h4>
                    <p class="ur-location">
                        <i class="ti-location-pin mrg-r-5"></i>
                        <?=(isset($profile_data->country->ar_name))? $profile_data->country->ar_name:"";?> -
                        <?=(isset($profile_data->city->ar_city_title ))? $profile_data->city->ar_city_title :"";?> -
                        <?=$profile_data->address?>
                    </p>
                    <span class="ur-designation">
                        <?=(isset($company_data->activity->ar_title)? $company_data->activity->ar_title:"")?>
                    </span>
                    <div class="rateing">
                        <i class="fa fa-star filled"></i>
                        <i class="fa fa-star filled"></i>
                        <i class="fa fa-star filled"></i>
                        <i class="fa fa-star filled"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>

            </div>

          <!--  <div class="ur-detail-btn">
                <a href="#" class="btn btn-warning mrg-bot-10 full-width">Follow Now</a><br>
                <a href="#" class="btn btn-primary full-width">Get in Touch</a>
            </div>-->

        </div>

    </div>
</section>
<!-- Company Detail End -->

<!-- company full detail Start -->
<section class="full-detail-description full-detail">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-md-8 col-xs-12 no-pxs">

                <div class="row-bottom">
                    <h2 class="detail-title">عـــن الشركـــــــة</h2>
                    <p><?=$profile_data->about?></p>
                </div>

                <div class="row-bottom">
                    <h2 class="detail-title">تقدم إلى وظائف شركة<?=$profile_data->name?></h2>
                    <h3>لديك <?=count($jobs)?> وظيفة تم نشرها</h3>

                    <!--Browse Job In Grid-->
                    <div class="row extra-mrg">

                        <?php
                        $pass["div"] = 6;
                        $this->load->view('frontend/load/search_job',$pass); ?>

                    </div>
                    <!--/.Browse Job In Grid-->

                </div>

            </div>

            <div class="col-lg-4 col-md-4 col-xs-12 no-pxs">
                <div class="full-sidebar-wrap">

                    <!-- Company overview -->
                    <div class="sidebar-widgets">

                        <div class="ur-detail-wrap">
                            <div class="ur-detail-wrap-header">
                                <h4>نبذة عن الشركة</h4>
                            </div>
                            <div class="ur-detail-wrap-body">
                                <ul class="ove-detail-list">

                                    <!--<li>
                                        <i class="fa fa-building"></i>
                                        <h5>تاريخ الإنشاء</h5>
                                        <span><?=(isset($company_data->date_created))? date("Y-m-d",$company_data->date_created):"";?></span>
                                    </li>-->

                                    <li>
                                        <i class="fa fa-users"></i>
                                        <h5>عدد الموظفين</h5>
                                        <span><?=(isset($company_data->number_employees_data->ar_title)? $company_data->number_employees_data->ar_title:"1")?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-user"></i>
                                        <h5>صاحب العمل</h5>
                                        <span><?=(isset($company_data->responsible_name)? $company_data->responsible_name:"")?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-envelope-o"></i>
                                        <h5>البريد الإلكتروني</h5>
                                        <span><?=$profile_data->email?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-mobile"></i>
                                        <h5>رقم الهاتف</h5>
                                        <span><?=$profile_data->phone?></span>
                                    </li>
                                    <li>
                                        <i class="fa fa-whatsapp"></i>
                                        <h5>رقم الواتساب</h5>
                                        <span><?=$profile_data->whatsapp_num?></span>
                                    </li>

                                </ul>
                            </div>
                        </div>

                    </div>
                    <!-- /Company overview -->

                    <!-- Working Days
                    <div class="sidebar-widgets">

                        <div class="ur-detail-wrap">
                            <div class="ur-detail-wrap-header">
                                <h4>أيام العمل الرسمية</h4>
                            </div>
                            <div class="ur-detail-wrap-body">
                                <ul class="working-days">

                                    <li>السبت<span>9AM - 5PM</span></li>
                                    <li>الأحد<span>9AM - 5PM</span></li>
                                    <li>الإثنين<span>9AM - 5PM</span></li>
                                    <li class="active">الثلاثاء<span>9AM - 5PM</span></li>
                                    <li>الأربعاء<span>9AM - 5PM</span></li>
                                    <li>الخميس<span>9AM - 5PM</span></li>
                                    <li class="close-day">الجمعة<span>Close</span></li>

                                </ul>
                            </div>
                        </div>

                    </div> -->
                    <!-- /Working Days -->

                    <!-- Follow Links -->
                    <div class="sidebar-widgets">

                        <div class="ur-detail-wrap">
                            <div class="ur-detail-wrap-header">
                                <h4>تابعنا على</h4>
                            </div>
                            <div class="ur-detail-wrap-body">
                                <ul class="follow-links">
                                    <?php if (isset($social_data)): ?>
                                        <li><a href="<?= $social_data->facebook ?>">
                                                <i class="ti-facebook"></i>Facebook</a>
                                        </li>
                                        <li><a href="<?= $social_data->twitter ?>">
                                                <i class="ti-twitter-alt"></i>Twitter</a>
                                        </li>
                                        <li><a href="<?= $social_data->linkedin ?>">
                                                <i class="ti-linkedin"></i>Linked In</a>
                                        </li>
                                        <li><a href="<?= $social_data->instagram ?>">
                                                <i class="ti-instagram"></i>Instagram</a>
                                        </li>
                                    <?php else: ?>
                                        <li><a href="#"><i class="ti-facebook"></i>Facebook</a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i>Twitter</a></li>
                                        <li><a href="#"><i class="ti-linkedin"></i>Linked In</a></li>
                                        <li><a href="#"><i class="ti-instagram"></i>Instagram</a></li>
                                    <?php endif ?>

                                </ul>
                            </div>
                        </div>

                    </div>
                    <!-- /Working Days -->



                </div>
            </div>

        </div>
    </div>
</section>
<!-- company full detail End -->


