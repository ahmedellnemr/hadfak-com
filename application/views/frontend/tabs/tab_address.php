<h3>معلومات التواصل</h3>
<ul class="job-detail-des">
    <li><span>العنوان:</span><?= $profile_data->address ?></li>
    <li>
        <span>المدينة:</span><?= (isset($profile_data->city->ar_city_title)) ? $profile_data->city->ar_city_title : "المدينة"; ?>
    </li>
    <li>
        <span>الدولة:</span><?= (isset($profile_data->country->ar_name)) ? $profile_data->country->ar_name : "الدولة"; ?>
    </li>
    <li><span>الهاتف:</span><?=$profile_data->phone?></li>
    <li><span>البريد الإلكتروني:</span><?=$profile_data->email?></li>
    <?php if (isset($social_data)): ?>
        <li><span>رابط فيسبوك Facebook:</span> <?=$social_data->facebook?></li>
        <li><span>رابط تويتر Twitter:</span> <?=$social_data->twitter?></li>
        <li><span>رابط جيميل Gmail:</span> <?=$social_data->gmail?></li>
        <li><span>رابط لينكد ان Linkedin:</span><?=$social_data->linkedin?></li>
        <li><span>رابط إنستجرام Instagram:</span> <?=$social_data->instagram?></li>
        <li><span>رابط يوتيوب Youtube:</span> <?=$social_data->youtube?></li>
    <?php else: ?>
        <li><span>رابط فيسبوك Facebook:</span> #</li>
        <li><span>رابط تويتر Twitter:</span> #</li>
        <li><span>رابط جيميل Gmail:</span> #</li>
        <li><span>رابط لينكد ان Linkedin:</span>#</li>
        <li><span>رابط إنستجرام Instagram:</span> #</li>
        <li><span>رابط يوتيوب Youtube:</span> #</li>
    <?php endif ?>
</ul>