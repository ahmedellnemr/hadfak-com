<div class="row no-mrg">
    <h3>تعديل بيانات الحساب</h3>
    <?= form_open_multipart('update-profile/'.$profile_data->user_id);  ?>
    <div class="edit-pro">
        <div class="col-md-4 col-sm-6">
            <label>إسم المستخدم</label>
            <input type="text"  name="Pdata[username]" value="<?=$profile_data->username?>" class="form-control" data-validation="required">
        </div>

        <div class="col-md-4 col-sm-6">
            <label>كلمة المرور القديمة</label>
            <input type="password"  class="form-control" data-validation="required" placeholder="*********">
        </div>
        <div class="col-md-4 col-sm-6">
            <label>كلمة المرور الجديدة</label>
            <input type="password"  name="Pdata[password]" class="form-control" data-validation="required" placeholder="*********">
        </div>
        <div class="col-md-4 col-sm-6">
            <label>تأكيد كلمة المرور</label>
            <input type="password"  class="form-control" data-validation="required" placeholder="*********">
        </div>


        <div class="col-sm-12 text-center">
            <button type="submit" name="add" value="add"  class="btn btn-primary blu-btn update-btn">تعديل الأن</button>
        </div>
    </div>
    <?=form_close()?>
</div>