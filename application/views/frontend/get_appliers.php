<!-- Title Header Start -->
<style>
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<section class="inner-header-title" style="background-image:url(<?=base_url().WEBASSETS?>img/banner-10.jpg);min-height: 240px;">
    <div class="container-fluid">
        <h1> <?=$title?> </h1>
    </div>
</section>


<section class="hiring-jop ptop-30">
    <div class="container-fluid">
        <div class="row" id="search_result">

            <?php
            if (isset($persons) && !empty($persons)) {
                foreach ($persons as $row): ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="manage-cndt">
                            <div class="cndt-status pending">
                                <?= (isset($row->experience_year->ar_title)) ? $row->experience_year->ar_title : "سنوات"; ?>
                            </div>
                            <div class="cndt-caption">
                                <div class="cndt-pic">
                                    <?php if (isset($row->user_data->logo) && !empty($row->user_data->logo) && is_file(IMAGEPATH . $row->user_data->logo)): ?>
                                        <img src="<?= base_url() . IMAGEPATH . $row->user_data->logo ?>" class="img-responsive" alt=""/>
                                    <?php else: ?>
                                        <img src="<?= base_url() . FAVICONPATH .USERIMAGE?>" class="img-responsive" alt=""/>
                                    <?php endif ?>
                                </div>
                                <h4><?= (isset($row->user_data->name)) ? $row->user_data->name : ""; ?></h4>
                                <span><?= (isset($row->job_title->ar_title) ? $row->job_title->ar_title : "") ?></span>
                                <p><?= (isset($row->user_data->about)) ? word_limiter($row->user_data->about, 12) : ""; ?></p>
                            </div>
                            <a href="<?= base_url() . "show-user/" . $row->user_id ?>" title="" class="cndt-profile-btn">مشاهدة
                                الصفحة الشخصية</a>
                        </div>
                    </div>
                <?php endforeach;
            }
            ?>

            <?php
            if (isset($jobs) &&!empty($jobs)) {
                foreach ($jobs as $row): ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="grid-view brows-job-list">
                            <div class="brows-job-company-img">
                                <?php if (isset($row->logo) && !empty($row->logo) && is_file(IMAGEPATH . $row->logo)): ?>
                                    <img src="<?= base_url() . IMAGEPATH . $row->logo ?>" class="img-responsive" alt=""/>
                                <?php else: ?>
                                    <img src="<?= base_url() . FAVICONPATH .JOBIMAGE?>" class="img-responsive" alt=""/>
                                <?php endif ?>
                            </div>
                            <div class="brows-job-position">
                                <h3>
                                    <a href="<?=base_url()."show-job/".$row->id."/".$row->company_id_fk ?>">
                                        <?= $row->ar_title ?>
                                    </a>
                                </h3>
                                <p><span><?= $row->name ?></span></p>
                            </div>
                            <div class="job-position">
                                <span class="job-num"><?=$row->positions?> Position </span>
                            </div>
                            <div class="brows-job-type">
                                <span class="full-time"><?= (isset($row->type_work->ar_title) ? $row->type_work->ar_title : "") ?></span>
                            </div>
                            <ul class="grid-view-caption">
                                <li>
                                    <div class="brows-job-location">
                                        <p>
                                            <i class="fa fa-map-marker"></i>
                                            <?= (isset($row->city->ar_city_title)) ? $row->city->ar_city_title : ""; ?> ,
                                            <?= (isset($row->user_data->address)) ? $row->user_data->address : ""; ?>
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <p>
                                        <?php if (!empty($row->salary) && $row->salary !=null): ?>
                                            <span class="brows-job-sallery"><i class="fa fa-money"></i>
                         <?= $row->salary ?>
                    </span>
                                        <?php endif ?>
                                    </p>
                                </li>
                            </ul>
                            <!--   <span class="tg-themetag tg-featuretag">Premium</span>-->
                        </div>
                    </div>
                <?php endforeach;
            }
            ?>
        </div>
        <!-- <div class="row">
             <ul class="pagination">
                 <li><a href="#">«</a></li>
                 <li class="active"><a href="#">1</a></li>
                 <li><a href="#">2</a></li>
                 <li><a href="#">3</a></li>
                 <li><a href="#">4</a></li>
                 <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                 <li><a href="#">»</a></li>
             </ul>
         </div>-->
    </div>
</section>


