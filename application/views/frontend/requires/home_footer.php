


<!-------------------------------------- login  ------------------------------------------------>
<div class="modal fade modal-sign" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="tab" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#login-main" role="tab" data-toggle="tab">تسجيل الدخول</a>
						</li>
						<li role="presentation">
							<a href="#register-in-login" role="tab" data-toggle="tab">  مستخدم جديد( أفراد )</a>
						</li>
						<li role="presentation">
							<a href="#register-company-in-login" role="tab" data-toggle="tab">   مستخدم جديد( صاحب عمل )</a>
						</li>
                    </ul>
                    <div class="tab-content" id="myModalLabel2">
                        <div role="tabpanel" class="tab-pane fade in active" id="login-main">
                            <img src="<?=base_url().WEBASSETS?>img/logo.png" class="img-responsive" alt="" />
                            <h5 class="text-center">خدمات إلكترونية متكاملة ( أفراد - صاحب عمل )</h5>
                            <div class="subscribe wow fadeInUp">
                                <?php //echo validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
                                <?php //echo form_open_multipart('web-auth',["class"=>"form-inline"]);  ?>
                                <form class=form-inline">
                                    <div class="log-me-div"></div>
                                <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="username" log-me="name" class="form-control" placeholder="إسم المستخدم"  data-validation="required">
                                            <input type="password" name="password" log-me="pass" class="form-control" placeholder="كلمة المرور"  data-validation="required">


                                            <div class="row form-group info">
                                                <div class="col-sm-6">
                                                    <label for="user-remember-field" class="remember">
                                                        <input type="checkbox" name="remember" id="user-remember-field" value="true"> تذكر كلمة المرور فى المرة القادمة
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <a class="back-link" href="#forgot-password-form-wrapper" title="Forgot Password">هل نسيت كلمة المرور ؟</a>
                                                </div>
                                            </div>
                                            <div class="center">
                                                <button type="button" id="login-btn" class="log-me submit-btn"> تسجيل الدخول </button>
                                            </div>
                                        </div>
                                        <div class="wrapper-social-login">
                                            <div class="line-header"><span>or</span></div>
                                            <div class="inner-social">
                                                <div class="facebook-login-btn-wrapper">
                                                    <a class="facebook-login-btn" href="<?=$this->facebookAuthURL?>"><i class="fa fa-facebook"></i> Facebook</a>
                                                </div>
                                                <div class="google-login-btn-wrapper">
                                                    <a class="google-login-btn" href="<?=$this->googleLoginURL?>">
														<i class="fa fa-google"></i> Google</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                                    <?php // echo form_close()?>
                            </div>
                        </div>
						<div role="tabpanel" class="tab-pane fade" id="register-in-login">
							<img src="<?=base_url().WEBASSETS?>img/logo.png" class="img-responsive" alt="" />
							<h5 class="text-center">خدمات إلكترونية متكاملة ( أفراد )</h5>
							<?= validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
							<?= form_open_multipart('register',["class"=>"form-inline"]);  ?>
							<div class="col-sm-12">
								<div class="form-group">
									<input type="hidden" name="Pdata[user_type]" value="1">
									<div class="form-group col-xs-12 padding-4">
										<label>الإسم بالكامل</label>
										<input type="text" name="Pdata[name]" class="form-control" placeholder=""  data-validation="required">
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 padding-4">
										<div class="form-group ">
											<label>الجنسية</label>
											<select name="Pdata_client[nationality]" class="form-control   " id="j-category" data-validation="required" aria-required="true"    >
												<option value="">اختر الجنسية</option>
												<?php if (isset($all_countries)): ?>
													<?php foreach ($all_countries as $row):  ?>
														<option value="<?=$row->id_country?>" ><?=$row->ar_nationality?></option>
													<?php endforeach ?>
												<?php endif ?>
											</select>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 padding-4">
										<div class="form-group ">
											<label>تاريخ الميلاد <small class="visible-xs">(المس من اليسار لليمين)</small></label>

											<input type="hidden" name="status" value="insert">
											<input type="text" name="Pdata_client[date_of_birth]" class="form-control datepicker" value=""
												   data-validation="required" id="meladyInput" readonly>
										</div>
									</div>
									<div class="col-sm-6 col-xs-12 padding-4">
										<div class="form-group ">
											<label>المسمى الوظيفي</label>
											<select name="Pdata_client[job_title_id_fk]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
												<option value="">اختر المسمى الوظيفي</option>
												<?php if (isset($all_job_titles) && !empty($all_job_titles)): ?>
													<?php foreach ($all_job_titles as $row):?>
														<option value="<?=$row->job_id."##".$row->activity_id_fk?>" ><?=$row->ar_title?></option>
													<?php endforeach ?>
												<?php endif ?>
											</select>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 padding-4">
										<div class="form-group ">
											<label>بلد الإقامة الأن</label>
											<select name="Pdata[country_id]" class="form-control  " id="country-ids"   >
												<option value="">اختر الدولة</option>
												<?php if (isset($all_countries)): ?>
													<?php foreach ($all_countries as $row):  ?>
														<option value="<?=$row->id_country?>"  ><?=$row->ar_name?></option>
													<?php endforeach ?>

												<?php endif ?>

											</select>
										</div>
									</div>
									<div class="col-sm-6  col-xs-12 padding-4">
										<div class="form-group ">
											<label>البريد الإلكتروني</label>
											<input type="text" name="Pdata[email]" class="form-control unique-field" field-name="email" data-db="registrations" placeholder=""  data-validation="email"  >
										</div>
									</div>
									<div class="col-sm-6  col-xs-12 padding-4">
										<label>اجمالى سنوات الخبرة</label>
										<select name="Pdata_client[experience_years]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
											<option value="">اختر العدد</option>
											<?php if (isset($all_experience_yearss)):?>
												<?php foreach ($all_experience_yearss as $row):
													$sel = "";
													if (isset($client_data->job_title_id_fk)) {
														if ($client_data->experience_years == $row->id_definition) {
															$sel = "selected";
														}
													}?>
													<option value="<?=$row->id_definition?>" <?=$sel?>><?=$row->ar_title?></option>
												<?php endforeach ?>

											<?php endif ?>
										</select>
									</div>
									<div class=" col-sm-6 col-xs-12 padding-4">
										<div class="form-group">
											<label>تحميل صورة شخصية</label>
											<input type="file" id="input-file-now-custom-1" name="logo" class="dropify" accept="image/x-png,image/jpg,image/jpeg" data-validation="required"/>
										</div>
									</div>
									<div class=" col-sm-6 col-xs-12 padding-4">
										<div class="form-group">
											<label>تحميل السيرة الذاتية (PDF , png , jpg , docx)</label>

											<input type="file" id="input-file-now-custom-1" name="cv_file" accept="application/pdf,.doc, .docx,image/x-png,image/jpg,image/jpeg"  class="dropify" data-validation="required"/>


										</div>
									</div>
									<div class="form-group col-xs-12 padding-4">
										<label>إسم المستخدم</label>
										<input type="text" name="Pdata[username]" class="form-control unique-field" field-name="username" data-db="registrations" placeholder=""  data-validation="alphanumeric custom" >
									</div>
									<div class="col-sm-6 col-xs-12 padding-4">
										<div class="form-group">
											<label>كلمة المرور</label>
											<input type="password" name="Pdata[password]" class="form-control" placeholder="********"  data-validation="required">
										</div>
									</div>
									<div class="col-sm-6 col-xs-12 padding-4">
										<div class="form-group">
											<label>تأكيد كلمة المرور</label>
											<input type="password"  class="form-control" placeholder="********"  data-validation="required">
										</div>
									</div>
									<div class="form-group center col-xs-12">
										<button type="submit" name="add" value="add" id="subscribe" class="submit-btn"> إنشاء حساب </button>
									</div>
								</div>
							</div>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="register-company-in-login">
							<img src="<?=base_url().WEBASSETS?>img/logo.png" class="img-responsive" alt="" />
							<h5 class="text-center">خدمات مؤسستك بين يديك ( صاحب عمل )</h5>
							<?php echo validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
							<?php echo form_open_multipart('register',["class"=>"form-inline"]);  ?>

							<div class="col-sm-12">
								<div class="form-group">
									<input type="hidden" name="Pdata[user_type]" value="2">
									<div class="form-group">
										<label>إسم المؤسسة</label>
										<input type="text" name="Pdata[name]" class="form-control" placeholder=""  data-validation="required">
									</div>
									<div class="form-group">
										<label>البريد الإلكتروني</label>
										<input type="text" name="Pdata[email]" class="form-control unique-field" field-name="email" data-db="registrations" placeholder=""  data-validation="email">
									</div>
									<div class="form-group">
										<label>إسم المستخدم</label>
										<input type="text" name="Pdata[username]" class="form-control unique-field" field-name="username" data-db="registrations" placeholder=""  data-validation="alphanumeric custom" >
									</div>
									<div class="form-group">
										<label>كلمة المرور</label>
										<input type="password" name="Pdata[password]" class="form-control"  placeholder=""  data-validation="required">
									</div>
									<div class="form-group">
										<label>تأكيد كلمة المرور</label>
										<input type="password"  class="form-control" placeholder=""  data-validation="required">
									</div>


									<div class="form-group center">
										<button type="submit" name="add" value="add" id="subscribe" class="submit-btn"> إنشاء حساب </button>
									</div>
								</div>
							</div>
							</form>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!-------------------------------------- person  ------------------------------------------------>
<div class="modal fade modal-sign" id="signup-person" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="tab" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#login" role="tab" data-toggle="tab">تسجيل الدخول</a>
                        </li>
                        <li role="presentation">
                            <a href="#register" role="tab" data-toggle="tab"> مستخدم جديد</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myModalLabel2">
                        <div role="tabpanel" class="tab-pane fade in active" id="login">
                            <img src="<?=base_url().WEBASSETS?>img/logo.png" class="img-responsive" alt="" />
                            <h5 class="text-center">خدمات إلكترونية متكاملة ( أفراد )</h5>
                            <div class="subscribe wow fadeInUp">
                                <?php //echo validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
                                <?php // echo form_open_multipart('web-auth',["class"=>"form-inline"]);  ?>
                                <form class="form-inline">
                                    <div class="log-me-div"></div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" name="username" log-me="name" class="form-control" placeholder="إسم المستخدم"  data-validation="required">
                                        <input type="password" name="password" log-me="pass" class="form-control" placeholder="كلمة المرور"  data-validation="required">


                                        <div class="row form-group info">
                                            <div class="col-sm-6">
                                                <label for="user-remember-field" class="remember">
                                                    <input type="checkbox" name="remember" id="user-remember-field" value="true"> تذكر كلمة المرور فى المرة القادمة
                                                </label>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <a class="back-link" href="#forgot-password-form-wrapper" title="Forgot Password">هل نسيت كلمة المرور ؟</a>
                                            </div>
                                        </div>
                                        <div class="center">
                                            <button type="button" id="login-btn" class="log-me submit-btn"> تسجيل الدخول </button>
                                        </div>
                                    </div>
                                    <div class="wrapper-social-login">
                                        <div class="line-header"><span>or</span></div>
                                        <div class="inner-social">
                                            <div class="facebook-login-btn-wrapper">
												<a class="facebook-login-btn" href="<?=$this->facebookAuthURL?>"><i class="fa fa-facebook"></i> Facebook</a>
                                            </div>
                                            <div class="google-login-btn-wrapper">
												<a class="google-login-btn" href="<?=$this->googleLoginURL?>">
													<i class="fa fa-google"></i> Google</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                </form>
                                <?php //echo form_close()?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="register">
                            <img src="<?=base_url().WEBASSETS?>img/logo.png" class="img-responsive" alt="" />
                            <h5 class="text-center">خدمات إلكترونية متكاملة ( أفراد )</h5>
                            <?= validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
                            <?= form_open_multipart('register',["class"=>"form-inline"]);  ?>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="hidden" name="Pdata[user_type]" value="1">
                                        <div class="form-group col-xs-12 padding-4">
                                           <label>الإسم بالكامل</label>
                                            <input type="text" name="Pdata[name]" class="form-control" placeholder=""  data-validation="required">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding-4">
                                              <div class="form-group ">
                                                <label>الجنسية</label>
                                                <select name="Pdata_client[nationality]" class="form-control   " id="j-category" data-validation="required" aria-required="true"    >
                                                    <option value="">اختر الجنسية</option>
                                                    <?php if (isset($all_countries)): ?>
                                                        <?php foreach ($all_countries as $row):  ?>
                                                            <option value="<?=$row->id_country?>" ><?=$row->ar_nationality?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding-4">
                                           <div class="form-group ">
                                            <label>تاريخ الميلاد <small class="visible-xs">(المس من اليسار لليمين)</small></label>

                                                <input type="hidden" name="status" value="insert">
                                                <input type="text" name="Pdata_client[date_of_birth]" class="form-control datepicker" value=""
                                                       data-validation="required" id="meladyInput" readonly>
                                             </div>
                                        </div>
										<div class="col-sm-6 col-xs-12 padding-4">
                                         <div class="form-group ">
                                            <label>المسمى الوظيفي</label>
                                            <select name="Pdata_client[job_title_id_fk]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                                                <option value="">اختر المسمى الوظيفي</option>
                                                <?php if (isset($all_job_titles) && !empty($all_job_titles)): ?>
                                                    <?php foreach ($all_job_titles as $row):?>
                                                        <option value="<?=$row->job_id."##".$row->activity_id_fk?>" ><?=$row->ar_title?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding-4">
                                           <div class="form-group ">
                                            <label>بلد الإقامة الأن</label>
                                            <select name="Pdata[country_id]" class="form-control  " id="country-ids"   >
                                                <option value="">اختر الدولة</option>
                                                <?php if (isset($all_countries)): ?>
                                                    <?php foreach ($all_countries as $row):  ?>
                                                        <option value="<?=$row->id_country?>"  ><?=$row->ar_name?></option>
                                                    <?php endforeach ?>

                                                <?php endif ?>

                                            </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6  col-xs-12 padding-4">
                                            <div class="form-group ">
                                               <label>البريد الإلكتروني</label>
                                               <input type="text" name="Pdata[email]" class="form-control unique-field" field-name="email" data-db="registrations" placeholder=""  data-validation="email"  >
                                            </div>
                                        </div>
                                        <div class="col-sm-6  col-xs-12 padding-4">
                                            <label>اجمالى سنوات الخبرة</label>
                                            <select name="Pdata_client[experience_years]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                                                <option value="">اختر العدد</option>
                                                <?php if (isset($all_experience_yearss)):?>
                                                    <?php foreach ($all_experience_yearss as $row):
                                                    $sel = "";
                                                    if (isset($client_data->job_title_id_fk)) {
                                                        if ($client_data->experience_years == $row->id_definition) {
                                                            $sel = "selected";
                                                        }
                                                    }?>
                                                        <option value="<?=$row->id_definition?>" <?=$sel?>><?=$row->ar_title?></option>
                                                    <?php endforeach ?>
                                             
                                                <?php endif ?>
                                            </select>
                                        </div>
                                        <div class=" col-sm-6 col-xs-12 padding-4">
                                            <div class="form-group">
                                                <label>تحميل صورة شخصية</label>
                                                    <input type="file" id="input-file-now-custom-1" name="logo" class="dropify" accept="image/x-png,image/jpg,image/jpeg" data-validation="required"/>
                                            </div>
                                        </div>
                                        <div class=" col-sm-6 col-xs-12 padding-4">
                                            <div class="form-group">
                                                <label>تحميل السيرة الذاتية (PDF , png , jpg , docx)</label>

                                                    <input type="file" id="input-file-now-custom-1" name="cv_file" accept="application/pdf,.doc, .docx,image/x-png,image/jpg,image/jpeg"  class="dropify" data-validation="required"/>


                                            </div>
                                        </div>
                                        <div class="form-group col-xs-12 padding-4">
                                           <label>إسم المستخدم</label>
                                           <input type="text" name="Pdata[username]" class="form-control unique-field" field-name="username" data-db="registrations" placeholder=""  data-validation="alphanumeric custom" >
                                        </div>
                                        <div class="col-sm-6 col-xs-12 padding-4">
                                              <div class="form-group">
                                               <label>كلمة المرور</label>
                                               <input type="password" name="Pdata[password]" class="form-control" placeholder="********"  data-validation="required">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 padding-4">
                                            <div class="form-group">
                                               <label>تأكيد كلمة المرور</label>
                                               <input type="password"  class="form-control" placeholder="********"  data-validation="required">
                                            </div>
                                        </div>
                                        <div class="form-group center col-xs-12">
                                            <button type="submit" name="add" value="add" id="subscribe" class="submit-btn"> إنشاء حساب </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-------------------------------------- company  ------------------------------------------------>
<div class="modal fade modal-sign modal-company" id="signup-company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="tab" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#login-company" role="tab" data-toggle="tab">تسجيل الدخول</a>
                        </li>
                        <li role="presentation">
                            <a href="#register-company" role="tab" data-toggle="tab"> مستخدم جديد</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myModalLabel2">
                        <div role="tabpanel" class="tab-pane fade in active" id="login-company">
                            <img src="<?=base_url().WEBASSETS?>img/logo.png" class="img-responsive" alt="" />
                            <h5 class="text-center">خدمات مؤسستك بين يديك ( صاحب عمل )</h5>
                            <div class="subscribe wow fadeInUp">
                                <?php //echo validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
                                <?php //echo form_open_multipart('web-auth',["class"=>"form-inline"]);  ?>
                                <form class="form-inline">
                                    <div class="log-me-div"></div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" name="username" log-me="name" class="form-control" placeholder="إسم المستخدم"  data-validation="required">
                                        <input type="password" name="password" log-me="pass" class="form-control" placeholder="كلمة المرور"  data-validation="required">


                                        <div class="row form-group info">
                                            <div class="col-sm-6">
                                                <label for="user-remember-field" class="remember">
                                                    <input type="checkbox" name="remember" id="user-remember-field" value="true"> تذكر كلمة المرور فى المرة القادمة
                                                </label>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <a class="back-link" href="#forgot-password-form-wrapper" title="Forgot Password">هل نسيت كلمة المرور ؟</a>
                                            </div>
                                        </div>
                                        <div class="center">
                                            <button type="button" id="login-btn" class="log-me submit-btn"> تسجيل الدخول </button>
                                        </div>
                                    </div>
                                    <div class="wrapper-social-login">
                                        <div class="line-header"><span>or</span></div>
                                        <div class="inner-social">
                                            <div class="facebook-login-btn-wrapper">
												<a class="facebook-login-btn" href="<?=$this->facebookAuthURL?>"><i class="fa fa-facebook"></i> Facebook</a>
                                            </div>
                                            <div class="google-login-btn-wrapper">
												<a class="google-login-btn" href="<?=$this->googleLoginURL?>">
													<i class="fa fa-google"></i> Google</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                </form>
                                <?php // echo form_close()?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="register-company">
                            <img src="<?=base_url().WEBASSETS?>img/logo.png" class="img-responsive" alt="" />
                            <h5 class="text-center">خدمات مؤسستك بين يديك ( صاحب عمل )</h5>
                            <?php echo validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
                            <?php echo form_open_multipart('register',["class"=>"form-inline"]);  ?>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="hidden" name="Pdata[user_type]" value="2">
                                    <div class="form-group">
                                       <label>إسم المؤسسة</label>
                                       <input type="text" name="Pdata[name]" class="form-control" placeholder=""  data-validation="required">
                                    </div>
                                    <div class="form-group">
                                       <label>البريد الإلكتروني</label>
                                       <input type="text" name="Pdata[email]" class="form-control unique-field" field-name="email" data-db="registrations" placeholder=""  data-validation="email">
                                    </div>
                                    <div class="form-group">
                                       <label>إسم المستخدم</label>
                                       <input type="text" name="Pdata[username]" class="form-control unique-field" field-name="username" data-db="registrations" placeholder=""  data-validation="alphanumeric custom" >
                                    </div>
                                    <div class="form-group">
                                       <label>كلمة المرور</label>
                                       <input type="password" name="Pdata[password]" class="form-control"  placeholder=""  data-validation="required">
                                    </div>
                                    <div class="form-group">
                                       <label>تأكيد كلمة المرور</label>
                                       <input type="password"  class="form-control" placeholder=""  data-validation="required">
                                    </div>


                                    <div class="form-group center">
                                        <button type="submit" name="add" value="add" id="subscribe" class="submit-btn"> إنشاء حساب </button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript" src="<?=base_url().WEBASSETS?>js/jquery-1.10.1.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/bootstrap-arabic.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/bootstrap-select.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/jquery.datetimepicker.full2.js"></script>
<script src="<?=base_url().WEBASSETS?>js/jquery.easing.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/jquery.lightbox-0.5.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/owl.carousel.min.js"></script>
<script src="<?=base_url().WEBASSETS?>js/custom.js"></script>
<script src="<?=base_url().WEBASSETS?>js/wow.min.js"></script>
<script src="<?=base_url().'assets/'?>sweetalert/sweetalert.js"></script>

<?php   $this->load->view('backend/requires/config_js');?>
<script src="<?= base_url() . ASS ?>script/my_script.js?v=<?=time()?>" type="text/javascript"></script>


<?php if (isset($erorr_mail)): ?>
	<script>
		$(document).ready(function () {
			swal({
					title: "خطأ",
					text: "تأكد من حساب جوجل  ",
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "شكرا",
					closeOnConfirm: false
				},
				function(){
					document.location.href = "/";
				});


		//	swal("خطأ!", "تأكد من حساب جوجل !", "success")
		//	document.location.href = "/";
		});
	</script>
<?php endif ?>


<script>
    new WOW().init();
    $('.datepicker').datetimepicker({
        format:'Y-m-d',
        timepicker: false
    });
    $.datetimepicker.setLocale('ar');
</script>
<script>
$('.detail-edit').click(function(){

   $('html, body').animate({
        scrollTop: $("#simple-design-tab").offset().top
    }, 2000);
});
window.activateTab = function(tab) {
   $('.nav-tabs a[href="#' + tab + '"]').tab('show');
}
</script>
<script>

    function delete_con( obj ,user_id,id ) {
        swal({
                title: "هل أنت متأكد",
                text: "سيتم حذف الوظيفة ",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "نعم!",
                closeOnConfirm: false
            },
            function(){
             //
                $.ajax({
                    type:'get',
                    url: '<?=base_url()?>delete-job/'+user_id+'/'+id,
                    dataType: 'html',
                    cache:false,
                    success: function(html){
                        swal({
                                title:"حذف!",
                                text:  "تم الحذف بنجاح .",
                                type: "success",
                            },
                            function(){
                                $(obj).closest('article').remove();
                            });

                    },
                    error:function(error){
                        console.log(error.responseText);
                    }
                });
        });
    }
</script>

<script>

    $('.search-bt').on('click', function(e) {
        var dataString = $(".search_key").serialize();
        //console.log(dataString);
        $("#option_table").html('<div class="loader" style="margin: auto;"></div>');
        $.ajax({
            type:'post',
            url: '<?= base_url().'searchBy' ?>',
            data:dataString,
            dataType: 'html',
            cache:false,
            success: function(html){
              // console.log(200);
              $('#search_result').html(html);
            },
            error:function(error){
				console.log(error.status);
               // console.log(error.responseText);
            }
        });
    });

    $('#apply-it').on('click', function(e) {
        var obj = $(this);
        if( obj.attr("job") != "false"  &&  obj.attr("com") != "false"){
            var dataString = {job_id: obj.attr("job"),company_id:obj.attr("com")};
            // console.log(dataString);
            $.ajax({
                type:'post',
                url: '<?= base_url().'apply-job' ?>',
                data:dataString,
                dataType: 'html',
                cache:false,
                success: function(html){
                   // console.log(html);
                    swal({
                            title:"شكرا لك",
                            text: "تم التقديم للوظيفة بنجاح",
                            type: "success",
                        },
                        function(){
                            obj.attr("job","false");
                            obj.attr("com","false");
                            obj.text("لقد تقدمت لهذه الوظيفة");
                        }
                    );

                },
                error:function(error){
                    console.log(error.responseText);
                }
            });
        }
        else{
            swal({
                    title:"شكرا لك",
                    text: "لقد تقدمت لهذه الوظيفة",
                    type: "error",
                },
                function(){
                    obj.attr("job","false");
                    obj.attr("com","false");
                }
            );
        }
    });

    $('#be-login').on('click', function(e) {
        swal({
                title:"تنبية",
                text: "يتوجب عليك تسجيل الدخول اولا لتستطيع التقدم للوظيفة",
                type: "error",
            },function () {
			   $("#signup").modal();
			   $("#login-main").html($("#login-main").html());
			}
        );
    });

    $("#send-contact").on('click', function() {

        //----------------------------------------
        validateEmail();
        var cbs = document.getElementsByClassName('send-contact');
        var flagValid = true;
        for (var i = 0; i < cbs.length; i++) {
            var validClass =$(cbs[i]).hasClass("valid-error") ;
            if(cbs[i].value == "" ){
                if( validClass != true){
                    $(cbs[i]).addClass("valid-error");
                    $(cbs[i]).after('<span style="color: red" class="valid-span"> هذا الحقل ضرورى  </span>');
                }
                flagValid = false;
            }else{
                $(cbs[i]).next(".valid-span").remove();
                $(cbs[i]).removeClass("valid-error");
            }
        }
        //----------------------------------------
        if(flagValid == true && emailValid ==true){
            var dataString = $(".send-contact").serialize();
            //console.log(dataString)
            var msg = '<?=($this->webLang == "ar")? "شكرا لتواصلك معنا":"Thank for contact with us";?>';
            var msgTitle = '<?=($this->webLang == "ar")? "تم الإرسال !":"Send success nessage !";?>';
            $.ajax({
                type:'post',
                url: '<?= base_url().'web/contact'?>',
                data:dataString,
                dataType: 'html',
                cache:false,
                success: function(html){
                    swal({
                            title:msgTitle,
                            text: msg,
                            type: "success",
                        },
                        function(){
                            $(".send-contact").val("");
                        }
                    );
                },
                error:function(error){
                    console.log(error.responseText);
                }
            });
        }
    });

     emailValid = true;
    function validateEmail(){
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if (reg.test($('#email').value) == false)
            {
                emailValid =false;

                alert('بريد إلكترونى خاطئ');
                return false;
            }

            return true;

    }

    $("#country-ids").on("change",function () {
        if($(this).val() != ""){
            var url = '<?=base_url()?>getCity/'+$(this).val();
           // console.log(url)
            $.ajax({
                type:'get',
                url: url,
                dataType: 'html',
                cache:false,
                success: function(html){
                    $("#city-ids").html(html);
                    $('#city-ids').selectpicker("refresh");
                },
                error:function(error){
                    console.log(error.responseText);
                }
            });
        }
    })

    $(".make-other").on('change',function () {
        var value_pass = $(this).val();
        var obj = $(this)
        var validClass = obj.hasClass("have-other") ;
        if(value_pass == 'other' ){
            if ( validClass != true) {
                obj.before('<input type="text" ' +
                    'name="new_job" placeholder="أضف المسمى الجديد " '  +
                    'class="form-control input-other"' +
                    'data-validation="required">'
                );
                obj.addClass("have-other");
            }
            else {

            }
        }
        else{
            $(".input-other").remove();
            obj.removeClass("have-other");
        }
    })

</script>


<script type="text/javascript">
    $(function() {
        $('#thumbnails a').lightBox();
    });
</script>
<!--<script >

    function autoResizeDiv()
    {
        //document.getElementById('content').style.height = window.innerHeight +'px';
        var heightOfContent = window.innerHeight ;

        var divHeightHeader = $(".mheader").height();




        var newheight= heightOfContent  - divHeightHeader  +'px';

        $('.warpper').css("height", newheight);

    }
    function autoResizeDivMobile()
    {
        //document.getElementById('content').style.height = window.innerHeight +'px';
        var heightOfContent = window.innerHeight ;
        $('.warpper').css("height", "auto");

    }

    var mq = window.matchMedia( "(min-width: 767px)" );

    if(mq.matches) {
        // the width of browser is more then 767px

        window.onresize = autoResizeDiv;
        autoResizeDiv();
    } else {
        // the width of browser is less then 767px

        window.onresize = autoResizeDivMobile;
        autoResizeDivMobile();
    }


</script>-->


<!-- file upload -->
<script src="<?=base_url() ?>assets/dropify/js/jasny-bootstrap.js"></script>
<!-- This is data table -->
<!-- jQuery file upload -->
<script src="<?=base_url() ?>assets/dropify/dist/js/dropify.min.js"></script>
<script>
    $(document).ready(function () {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-d�posez un fichier ici ou cliquez',
                replace: 'Glissez-d�posez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'D�sol�, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function (event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function (event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function (event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function (e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>



<script src="<?= base_url() ?>assets/validator/jquery.form-validator.js"></script>
<script>
    $(function () {
        $.validate({
            validateHiddenInputs: true
        });
    });
</script>

<script>
$('#meladyInput').click(function(){
    $(".xdsoft_datetimepicker").css("display","block !important");
});


</script>

</body>
</html>
