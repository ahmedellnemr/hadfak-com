
<footer class="footer">
    <div class="row lg-menu">
        <div class="container">
            <div class="col-md-4 col-sm-4">
                <img src="<?=base_url().WEBASSETS?>img/footer-logo.png" class="img-responsive" alt="">
            </div>
            <div class="col-md-8 co-sm-8 pull-right">
                <ul>
                    <li><a href="<?=base_url()?>">الرئيسية <span class="sr-only">(current)</span></a></li>
                    <?php if (isset($_SESSION["user_data"]["user_data"]["user_type"])): ?>
                        <?php if ($_SESSION["user_data"]["user_type"] == 1): ?>
                            <li><a href="<?=base_url().'user-profile/'.$_SESSION["user_data"]["user_id"]?>">سيرتى الذاتية</a></li>
                        <?php endif ?>
                        <?php if ($_SESSION["user_data"]["user_type"] == 2): ?>
                            <li><a href="<?=base_url().'company-profile/'.$_SESSION["user_data"]["user_id"]?>">صفحة شركتي</a></li>
                        <?php endif ?>
                    <?php endif ?>
                    <li><a href="<?=base_url().'search?type=job'?>">البحث عن الوظائف</a></li>
                    <li><a href="<?=base_url().'search?type=user'?>">البحث عن أشخاص</a></li>
                    <li><a href="<?=base_url().'search?type=company'?>">ابحث عن شركة</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row no-padding">
        <div class="container">
            <div class="col-md-3 col-sm-12">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">عن هدفــــــك</h3>
                    <div class="textwidget">
                        <?php if (isset($this->setting->ar_address) && !empty($this->setting->ar_address)): ?>
                        <p><?=$this->setting->ar_address?></p>
                        <?php else: ?>
                            <p>شركة هدفها توصيل أصحاب العمل إلى إيجاد الموظف المثالى , وتهدف غلى خدمة الباحث عن وظيفة
                                جيدة.</p>
                        <?php endif ?>

                        <?php if (isset($this->setting->ar_about) && !empty($this->setting->ar_about)): ?>
                            <p><?=$this->setting->ar_about?></p>
                        <?php else: ?>
                            <p>المملكة العربية السعودية<br> الرياض , شارع العزيزية</p>
                        <?php endif ?>

                        <p><strong>البريد الإلكتروني:</strong>
                            <?php if (isset($this->setting->emails) && !empty($this->setting->emails)): ?>
                                <?=$this->setting->emails?>
                            <?php else: ?>
                                Support@careerdesk
                            <?php endif ?>
                        </p>

                        <p><strong>الهاتف:</strong>
                            <a>
                                <?php if (isset($this->setting->phones) && !empty($this->setting->phones)): ?>
                                    <?=$this->setting->phones?>
                                <?php else: ?>
                                    555-555-1234
                                <?php endif ?>
                            </a>
                        </p>

                        <ul class="footer-social">
                            <li><a href="<?=(isset($this->setting->phones))? $this->setting->facebook:"#" ?>"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="<?=(isset($this->setting->phones))? $this->setting->google_plus:"#" ?>"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="<?=(isset($this->setting->phones))? $this->setting->twitter:"#" ?>"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="<?=(isset($this->setting->phones))? $this->setting->instagram:"#" ?>"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="<?=(isset($this->setting->phones))? $this->setting->linkedin:"#" ?>"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">شركات مميزة</h3>
                    <div class="textwidget">
                        <div class="textwidget">
                            <ul class="footer-navigation">
                                <?php if (isset($this->all_com) && !empty($this->all_com)): ?>
                                    <?php foreach ($this->all_com as $row): ?>
                                        <li>
                                            <a href="<?=base_url()."show-company/".$row->user_id?>" title="">
                                                <?=$row->name?>
                                            </a>
                                        </li>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <li><a href="#" title="">Future Careers Middle East Freezone LLC</a></li>
                                    <li><a href="#" title="">Cognizant Technology Solutions - Middle East</a></li>
                                    <li><a href="#" title="">Magrabi Hospitals & Centers</a></li>
                                    <li><a href="#" title="">ALROWAD IT SOLUTIONS</a></li>
                                    <li><a href="#" title="">Saudi Home Loans Company</a></li>
                                    <li><a href="#" title="">Al Alamiah Internet & Communication Co.</a></li>
                                <?php endif ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">تصنيفات الوظائف</h3>
                    <div class="textwidget">
                        <ul class="footer-navigation">
                            <?php if (isset($this->all_jobs) && !empty($this->all_jobs)): ?>
                                <?php foreach ($this->all_jobs as $row): ?>
                                    <li>
                                        <a href="<?= base_url() . "show-job/" . $row->id ."/".$row->company_id_fk?>" title="">

                                            <?=(isset($row->job_title->ar_title))? $row->job_title->ar_title:"وظيفة"?>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            <?php else: ?>
                                <li><a href="#" title="">Front-end Design</a></li>
                                <li><a href="#" title="">Android Developer</a></li>
                                <li><a href="#" title="">CMS Development</a></li>
                                <li><a href="#" title="">PHP Development</a></li>
                                <li><a href="#" title="">IOS Developer</a></li>
                                <li><a href="#" title="">Iphone Developer</a></li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">تواصل معنا</h3>
                    <div class="textwidget">
                     <!--   <form class="">-->

                            <input type="text" name="name" class="form-control send-contact" placeholder="إسمك بالكامل">
                            <input type="email" name="email" class="form-control send-contact" id="email" placeholder="البريد الإلكتروني"  >
                            <textarea name="message" class="form-control send-contact" placeholder="الرسالة"></textarea>
                            <button type="button" id="send-contact" class="btn btn-primary">أرسل الأن</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row copyright">
        <div class="container">
            <p>حقوق النشر محفوظة لشركة هدفك دوت كوم © <?=date("Y")?>. جميع الحقوق محفوظة</p>
        </div>
    </div>
</footer>

<?php $this->load->view('frontend/requires/home_footer');?>