<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="موقع توظيف للباحثين عن عمل , يمكنك بكل سهوله من إيجاد الوظيفة المناسبة">
    <meta name="author" content="">
    <link rel="canonical" href="http://hadfak.com/" />


    <title>هدفك دوت كوم</title>
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url().WEBASSETS?>img/favicon/favicon-32x32.png">


    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/bootstrap-arabic-theme.min.css" />
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/bootstrap-arabic.min.css" />
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/bootstrap-select.min.css" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/jquery.datetimepicker.css" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/owl.carousel.css" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/owl.theme.css" >
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/jquery.lightbox-0.5.css" >
    <link rel="stylesheet" href="<?=base_url() ?>assets/dropify/dist/css/dropify.min.css">
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/animate.css">
    <link rel="stylesheet" href="<?=base_url().'assets/'?>sweetalert/sweetalert.css">
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
    <link rel="stylesheet" href="<?=base_url().WEBASSETS?>css/responsive.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">



   <?php $actual_link = current_url();?>
<link rel="canonical" href="<?php echo $actual_link?>">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="article">
<meta property="og:title" content='<?php  if(isset($share_title)){echo trim(strip_tags($share_title), '"');}else{ echo "هدفك دوت كوم";} ?>' >
<meta property="og:description" content='<?php  if(isset($share_content)){echo trim(strip_tags($share_content), '"');}else{ echo "هدفك دوت كوم";} ?>'>
<meta property="og:url" content="<?php echo $actual_link?>">
<meta property="og:site_name" content="Yoko Co.">
<meta property="article:tag" content="facebook">
<meta property="article:tag" content="wordpress">
<meta property="article:section" content="Advice">
<meta property="article:published_time" content="<?=date("Y-m-d")?>T11:22:16-04:00">
<meta property="article:modified_time" content="<?=date("Y-m-d")?>T16:31:30-04:00">
<meta property="og:updated_time" content="<?=date("Y-m-d")?>4T16:31:30-04:00">
<meta property="og:image" content="<?php if(isset($share_image)){ echo  base_url().$share_image ;} else{ echo base_url()."assets/frontend/img/logo.png"; }?>">
<meta property="og:image:secure_url" content="<?php if(isset($share_image)){ echo  base_url().$share_image ;} else{ echo base_url()."assets/frontend/img/logo.png" ;} ?>">
<meta property="og:image:width" content="3428">
<meta property="og:image:height" content="2332">



</head>

<body id="page-top" data-spy="scroll" >


<header class="mheader">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url()?>">
                    <img src="<?=base_url().FAVICONPATH.HLOGO?>">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <?php if (isset($_SESSION["user_data"]["user_type"])): ?>
                        <?php if ($_SESSION["user_data"]["user_type"] == 1): ?>
                            <li class="active"><a href="<?=base_url().'search?type=job'?>">الرئيسية <span class="sr-only">(current)</span></a></li>
                            <li><a href="<?=base_url().'user-profile/'.$_SESSION["user_data"]["user_id"]?>">سيرتى الذاتية</a></li>
                            <li><a href="<?=base_url().'search?type=job'?>">البحث عن الوظائف</a></li>
                            <li><a href="<?=base_url().'search?type=company'?>">ابحث عن شركة</a></li>
                        <?php endif ?>
                        <?php if ($_SESSION["user_data"]["user_type"] == 2): ?>
                            <li class="active"><a href="<?=base_url().'search?type=user'?>">الرئيسية <span class="sr-only">(current)</span></a></li>
                            <li><a href="<?=base_url().'company-profile/'.$_SESSION["user_data"]["user_id"]?>">صفحة شركتي</a></li>
                           <!-- <li><a href="<?=base_url().'search?type=job'?>">البحث عن الوظائف</a></li>-->
                            <li><a href="<?=base_url().'search?type=user'?>">البحث عن أشخاص</a></li>
                          <!--  <li><a href="<?=base_url().'search?type=company'?>">ابحث عن شركة</a></li> -->
                        <?php endif ?>
                    <?php else: ?>
                        <li class="active"><a href="<?=base_url()?>">الرئيسية <span class="sr-only">(current)</span></a></li>
                        <li><a href="<?=base_url().'search?type=job'?>">البحث عن الوظائف</a></li>
                        <!--<li><a href="<?=base_url().'search?type=user'?>">البحث عن أشخاص</a></li>
                        <li><a href="<?=base_url().'search?type=company'?>">ابحث عن شركة</a></li>-->
                    <?php endif ?>


                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php if (isset($_SESSION["user_data"]["is_web_login"]) && $_SESSION["user_data"]["is_web_login"] == true): ?>
                        <li><a href="<?=base_url().'profile/logout'?>"  class="signout">تسجيل
                                الخروج </a></li>
                        <li><a class="no-padding" href="#">
                        <div class="header-pic">
                          
                          <?php if ($_SESSION["user_data"]["user_type"] == 1): ?>
                            <?php if ( !empty($_SESSION["user_data"]["logo"]) && is_file(IMAGEPATH . $_SESSION["user_data"]["logo"])): ?>
                                <img src="<?= base_url() . IMAGEPATH . $_SESSION["user_data"]["logo"] ?>" class="img-responsive" alt=""/>
                            <?php else: ?>
                                <img src="<?= base_url() . FAVICONPATH ?>client.png" class="img-responsive" alt=""/>
                            <?php endif ?>
                            
                           <?php endif ?>
                          <?php if ($_SESSION["user_data"]["user_type"] == 2): ?> 
                            
                             <?php if ( !empty($profile_data->logo) && is_file(IMAGEPATH . $profile_data->logo)): ?>
                                <img src="<?= base_url() . IMAGEPATH . $profile_data->logo ?>" class="img" alt=""/>
                            <?php else: ?>
                                <img src="<?= base_url() . FAVICONPATH ?>company.png" class="img" alt=""/>
                            <?php endif ?>
                            <?php endif ?> 
                            
                            
                            
                            
                        </div>
                        </a>
                        
                        </li>
                    <?php else: ?>
                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#signup" class="signin">تسجيل
                                الدخول الآن</a></li>
                    <?php endif ?>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>

