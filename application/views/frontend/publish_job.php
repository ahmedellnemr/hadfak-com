
<section class="inner-header-title" style="background-image:url(<?=base_url().WEBASSETS?>img/banner-10.jpg);">
    <div class="container">
        <h1><?=$title?></h1>
    </div>
</section>

    <?php if($op == "insert"){ ?>
    <?= form_open_multipart('add-job/'.$profile_data->user_id , ["class"=>'add-feild']);  ?>
   <?php }elseif($op == "update"){?>
    <?= form_open_multipart('update-job/'.$profile_data->user_id."/".$one->id , ["class"=>'add-feild']);  ?>
   <?php }?>
<div class="detail-desc section">
    <div class="container">
        <div class="ur-detail-wrap create-kit padd-bot-0">

            <div class="row">
                <div class="detail-pic js">
                    <div class="box">
                        <input type="file" name="logo" id="upload-pic" class="inputfile">
                        <label for="upload-pic">
                            <i class="fa fa-upload" aria-hidden="true"></i><span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row bottom-mrg">

                <div class="col-md-12 col-sm-12">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                            <label>عنوان الوظيفة</label>
                            <input type="hidden" name="Pjob[activity_id_fk]" value="<?=$company_data->activity_id_fk?>">
                            <select name="Pjob[job_title_id]" class="form-control make-other" id="j-category"
                                    data-validation="required" aria-required="true">
                                <option value="">اختر المسمى الوظيفي</option>
                                <?php if (isset($all_job_titles) && !empty($all_job_titles)): ?>
                                    <?php foreach ($all_job_titles as $row):
                                         $sel = ($one->job_title_id == $row->job_id)? "selected":"";  ?>
                                        <option value="<?= $row->job_id . "##" . $row->activity_id_fk ?>" <?=$sel?>><?= $row->ar_title ?></option>
                                    <?php endforeach ?>
                                    <option value="other">اخرى </option>
                                <?php else: ?>
                                    <option value="">أضف المسمى الوظيفي</option>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                            <label>البريد الإلكتروني للتواصل</label>
                            <input type="text" name="Pjob[job_email]" value="<?=$one->job_email?>" class="form-control"
                                   data-validation="email">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                            <label>نوع الدوام</label>
                            <select name="Pjob[type_work_id]" class="form-control input-lg" data-validation="required"
                                    aria-required="true">
                                <option value="">اختر</option>
                                <?php if (isset($type_work)): ?>
                                    <?php foreach ($type_work as $row):
                                        $sel = ($one->type_work_id == $row->id_definition)? "selected":"";?>
                                        <option value="<?= $row->id_definition ?>" <?=$sel?>><?= $row->ar_title ?></option>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <option value="">أضف المؤهل</option>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                            <label>مقر العمل </label>
                            <input name="" type="text" value="<?= $profile_data->address ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-4">
                        <div class="input-group">
                            <label>عدد الوظائف </label>
                            <input name="Pjob[positions]" type="text" value="<?=$one->positions?>" class="form-control"
                                   data-validation="required">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="input-group">
                            <label>الراتب </label>
                            <input name="Pjob[salary]" type="text" value="<?=$one->salary?>" class="form-control"
                                   data-validation="required">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="input-group">
                            <label>إظهار إسم الشركة </label> <br>
                            <input name="Pjob[show_company]" type="radio" value="1" <?=($one->show_company == 1)? "checked":"";?> data-validation="required"> نعم
                            <input name="Pjob[show_company]" type="radio" value="2" <?=($one->show_company == 2)? "checked":"";?> data-validation="required"> لا
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-4">
                        <div class="input-group">
                            <label>
                                الخبرة المطلوبة</label>
                            <select name="Pjob[experience_years]" class="form-control" id="j-category"
                                    data-validation="required" aria-required="true">
                                <option value="">اختر العدد</option>
                                <?php if (isset($all_experience_yearss)): ?>
                                    <?php foreach ($all_experience_yearss as $row):
                                        $sel = ($one->experience_years == $row->id_definition)? "selected":""; ?>
                                        <option value="<?= $row->id_definition ?>" <?= $sel ?>><?= $row->ar_title ?></option>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <option value="">أضف العدد</option>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="input-group">
                            <label>
                                المؤهل المطلوب </label>
                            <select name="Pjob[qualification_id_fk]" class="form-control" id="j-category"
                                    data-validation="required" aria-required="true">
                                <option value="">اختر المؤهل</option>
                                <?php if (isset($all_qualifications)): ?>
                                    <?php foreach ($all_qualifications as $row):
                                        $sel = ($one->qualification_id_fk == $row->id_definition)? "selected":"";
                                        ?>
                                        <option value="<?= $row->id_definition ?>" <?= $sel ?> ><?= $row->ar_title ?></option>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <option value="">أضف المؤهل</option>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="input-group">
                            <label>النوع</label> <br>
                            <input name="Pjob[gender]" type="radio" value="1"  <?=($one->gender == 1)? "checked":""?> data-validation="required"> ذكر
                            <input name="Pjob[gender]" type="radio" value="2"  <?=($one->gender == 2)? "checked":""?> data-validation="required"> انثى
                            <input name="Pjob[gender]" type="radio" value="3"  <?=($one->gender == 3)? "checked":""?> data-validation="required">ذكر و انثى
                        </div>
                    </div>
                </div>

                    <div class="col-md-12 col-sm-12">
                        <h2 class="detail-title">وصف الوظيفة</h2>
                        <textarea name="Pjob[details]" class="form-control" placeholder="وصف الوظيفة" data-validation="required">
                            <?=$one->details?>
                        </textarea>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <h2 class="detail-title">متطلبات الوظيفة</h2>
                        <textarea name="Pjob[requirements]" class="form-control textarea" placeholder="المهارت والخبرات المطلوبة" data-validation="required">
                                <?=$one->requirements?>
                        </textarea>
                    </div>

            </div>


        </div>
    </div>
</div>
<section class="full-detail">
    <div class="container">
        <div class="row bottom-mrg extra-mrg">

                <h2 class="detail-title">معلومات الشركة </h2>
            <?php if (isset($company_data)): ?>
                <input type="hidden" name="status_company" value="update">
            <?php else: ?>
                <input type="hidden" name="status_company" value="insert">
            <?php endif ?>


                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                        <input type="text" name="Pdata[name]" class="form-control" value="<?=$profile_data->name?>"  data-validation="required">
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                        <select name="Pdata_company[activity_id_fk]" class="form-control" id="j-category" data-validation="required" aria-required="true" >
                            <?php if (isset($all_company_activities)): ?>
                                <?php foreach ($all_company_activities as $row):
                                    $sel = "";
                                    if(isset($company_data->activity_id_fk)){
                                        if($company_data->activity_id_fk == $row->activity_id){
                                            $sel = "selected";
                                        }
                                    }
                                    ?>
                                    <option value="<?=$row->activity_id?>" <?=$sel?> ><?=$row->ar_title?></option>
                                <?php endforeach ?>
                            <?php else: ?>
                                <option value="">أضف نشاط</option>
                            <?php endif ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" name="Pdata[email]" value="<?=$profile_data->email?>" class="form-control" data-validation="email">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                        <input type="text" name="Pdata[phone]" value="<?=$profile_data->phone?>" class="form-control" data-validation="required">
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                        <input name="Pdata[address]" type="text" value="<?=$profile_data->address?>" class="form-control" data-validation="required">
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
                        <input type="text" name="Pdata[whatsapp_num]" value="<?=$profile_data->whatsapp_num?>"  class="form-control" data-validation="required">
                    </div>
                </div>




        </div>

        <div class="row bottom-mrg extra-mrg">

                <h2 class="detail-title">وسائل تواصل الشركة</h2>

            <?php if (isset($social_data)): ?>
                <input type="hidden" name="status_social" value="update">
            <?php else: ?>
                <input type="hidden" name="status_social" value="insert">
            <?php endif ?>
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                        <input type="text"  name="Psocial[facebook]" class="form-control" value="<?=(isset($social_data->facebook)? $social_data->facebook:"")?>">
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                        <input type="text"  name="Psocial[gmail]" class="form-control" value="<?=(isset($social_data->gmail)? $social_data->gmail:"")?>" >
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                        <input type="text" name="Psocial[twitter]" class="form-control" value="<?=(isset($social_data->twitter)? $social_data->twitter:"")?>">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                        <input type="text"  name="Psocial[instagram]" class="form-control" value="<?=(isset($social_data->instagram)? $social_data->instagram:"")?>">
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                        <input type="text" name="Psocial[linkedin]" class="form-control" value="<?=(isset($social_data->linkedin)? $social_data->linkedin:"")?>">
                    </div>
                </div>

        </div>

        <div class="row bottom-mrg extra-mrg">


            <div class="col-md-12 col-sm-12 text-center">
                <button type="submit" name="add" value="add" class="btn btn-success large-btn publish-btn">انشر الوظيفة الأن</button>
            </div>

        </div>
    </div>
</section>
<?=form_close()?>




