<!-- Title Header Start -->
<style>
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<section class="inner-header-title" style="background-image:url(<?=base_url().WEBASSETS?>img/banner-10.jpg);min-height: 240px;">
    <div class="container-fluid">
        <!-- Company Searrch Filter Start -->
        <div class="row extra-mrg">
            <div class="wrap-search-filter">
				<?php if ($type == "user"): ?>
					<div class="col-md-4 col-sm-4 col-xs-12 no-pxs">
						<label style="display: block;text-align: right;">كلمة البحث</label>
						<input type="text" name="key_name" class="form-control search_key" placeholder="<?= $title ?>">
						<input type="hidden" name="search_about" value="<?= $type ?>" class="search_key">
					</div>
					<div class="col-md-2 col-sm-3 col-xs-12 no-pxs">
						<label style="display: block;text-align: right;">المسمى الوظيفى</label>
						<select class="form-control selectpicker search_key" name="job_title_id_fk[]" data-show-subtext="true"
								data-live-search="true" multiple="">
							<option value="">اختر</option>
							<?php if (isset($all_job_titles)): ?>
								<?php foreach ($all_job_titles as $row): ?>
									<option value="<?= $row->job_id ?>"><?= $row->ar_title ?></option>
								<?php endforeach ?>
							<?php else: ?>
								<option value=""> الدول</option>
							<?php endif ?>
						</select>

					</div>
					<div class="col-md-2 col-sm-3 col-xs-12 no-pxs">
						<label style="display: block;text-align: right;">الجنسية</label>
						<select class="form-control selectpicker search_key" name="nationality[]" data-show-subtext="true"
								data-live-search="true" multiple="">
							<option value="">اختر</option>
							<?php if (isset($all_countries)): ?>
								<?php foreach ($all_countries as $row): ?>
									<option value="<?= $row->id_country ?>"><?= $row->ar_nationality ?></option>
								<?php endforeach ?>
							<?php else: ?>
								<option value=""> الجنسية</option>
							<?php endif ?>
						</select>

					</div>
					<div class="col-md-2 col-sm-3 col-xs-12 no-pxs">
						<label style="display: block;text-align: right;">بلد الإقامة</label>
						<select class="form-control selectpicker search_key" name="country[]" data-show-subtext="true"
								data-live-search="true" multiple="">
							<option value="">اختر</option>
							<?php if (isset($all_countries)): ?>
								<?php foreach ($all_countries as $row): ?>
									<option value="<?= $row->id_country ?>"><?= $row->ar_name ?></option>
								<?php endforeach ?>
							<?php else: ?>
								<option value=""> الدول</option>
							<?php endif ?>
						</select>

					</div>
				<?php else: ?>
					<div class="col-md-4 col-sm-4 col-xs-12 no-pxs">
						<label style="display: block;text-align: right;">كلمة البحث</label>
						<input type="text" name="key_name" class="form-control search_key" placeholder="<?= $title ?>">
						<input type="hidden" name="search_about" value="<?= $type ?>" class="search_key">
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 no-pxs">
						<label style="display: block;text-align: right;">اختر الدولة </label>
						<select class="form-control selectpicker search_key" name="country[]" data-show-subtext="true"
								data-live-search="true" multiple="">
							<option value="">اختر</option>
							<?php if (isset($all_countries)): ?>
								<?php foreach ($all_countries as $row): ?>
									<option value="<?= $row->id_country ?>"><?= $row->ar_name ?></option>
								<?php endforeach ?>
							<?php else: ?>
								<option value=""> الدول</option>
							<?php endif ?>
						</select>

					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 no-pxs">
						<label style="display: block;text-align: right;">اختر المجال الوظيفي </label>
						<select class="form-control selectpicker search_key" name="activity[]" data-show-subtext="true"
								data-live-search="true"  multiple="">
							<option value="">اختر</option>
							<?php if (isset($all_company_activities)): ?>
								<?php foreach ($all_company_activities as $row): ?>
									<option value="<?= $row->activity_id ?>"><?= $row->ar_title ?></option>
								<?php endforeach ?>
							<?php else: ?>
								<option value=""> نشاط</option>
							<?php endif ?>
						</select>

					</div>
				<?php endif ?>
                    <div class="col-md-2 col-sm-2 col-xs-12 no-pxs">
                        <button type="button"  class="btn btn-primary full-width search-bt" style="margin-top: 25px;">بحث الأن</button>
                    </div>

            </div>
        </div>
        <!-- Company Searrch Filter End -->
    </div>
</section>


<section class="hiring-jop ptop-30">
    <div class="container-fluid">
        <div class="row" id="search_result">
            <?php
            if (isset($persons)):
               $this->load->view('frontend/load/search_person');
            endif;
            ?>

            <?php
            if (isset($company)):
                $this->load->view('frontend/load/search_company');
            endif;
            ?>
            <?php
            if (isset($jobs)):
                $this->load->view('frontend/load/search_job');
            endif;
            ?>
        </div>
       <!-- <div class="row">
            <ul class="pagination">
                <li><a href="#">«</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                <li><a href="#">»</a></li>
            </ul>
        </div>-->
    </div>
</section>


