
<?php if (!empty($profile_data->banner) && is_file(IMAGEPATH . $profile_data->banner)){
    $banner  = base_url().IMAGEPATH .$profile_data->banner;
 }
 else{
    if(!empty($all_imgs[0]->person_image)){ $banner  =  base_url() .IMAGEPATH .$all_imgs[0]->person_image; }
    else{$banner  = base_url().WEBASSETS .'img/banner-10.jpg';}
      
 }
   
 

 
?>
<!-- Title Header Start -->
<section class="inner-header-title" style="background-image:url(<?=$banner?>);">
    <div class="container">
        <h1><?=$profile_data->name?></h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Title Header End -->

<!-- Company Detail Start -->
<section class="detail-desc">
    <div class="container">

        <div class="ur-detail-wrap top-lay">

            <div class="ur-detail-box">

                <div class="ur-thumb">
                    <?php if ( !empty($profile_data->logo) && is_file(IMAGEPATH . $profile_data->logo)): ?>
                        <img src="<?= base_url() . IMAGEPATH . $profile_data->logo ?>" class="img-responsive" alt=""/>
                    <?php else: ?>
                        <img src="<?= base_url() . FAVICONPATH ?>client.png" class="img-responsive" alt=""/>
                    <?php endif ?>
                </div>
                <div class="ur-caption">
                    <h4 class="ur-title"><?=$profile_data->name?></h4>
                    <p class="ur-location">
                        <i class="ti-location-pin mrg-r-5"></i><?= $profile_data->address ?></p>
                    <span  class="ur-designation">
                        <?= (isset($client_data->experience_year->ar_title) ? $client_data->experience_year->ar_title : "") ?>
                    </span><br>
                    <span class="ur-designation">رقم الهاتف : <?= $profile_data->phone ?></span>
                </div>

            </div>

            <div class="ur-detail-btn">
                <a  class="btn btn-warning mrg-bot-10 full-width" style="cursor: default;">
                    <?= (isset($client_data->job_title->ar_title) ? $client_data->job_title->ar_title : "") ?>
                </a><br>
                <?php
                $downPath = "#";
                if(isset($client_data->cv_file)){
              //  $downPath = base_url().'read?file='.$client_data->cv_file ;
                 $downPath = base_url().'uploads/images/'.$client_data->cv_file ;
                }
                ?>
                <a href="<?=$downPath?>" download class="btn btn-primary full-width">تحميل السيرة الذاتية</a>
            </div>

        </div>

    </div>
</section>
<!-- Company Detail End -->

<!-- company full detail Start -->
<section class="full-detail-description full-detail">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-md-8 col-xs-12 no-pxs">

                <div class="row-bottom">
                    <h2 class="detail-title">نبذة عن <?=$profile_data->name?></h2>
                    <p><?=$profile_data->about?></p>
                </div>
                <?php if(isset($client_data->experience)){?>
                <div class="row-bottom">
                    <h2 class="detail-title">الخبرات الوظيفية</h2>
                    <p><?=$client_data->experience?></p>
                </div>
                <?php }?>


            </div>

            <div class="col-lg-4 col-md-4 col-xs-12 no-pxs">
                <div class="full-sidebar-wrap">

                    <!-- Company overview -->
                    <div class="sidebar-widgets">

                        <div class="ur-detail-wrap">
                            <div class="ur-detail-wrap-header">
                                <h4>معلومات شخصية</h4>
                            </div>
                            <div class="ur-detail-wrap-body">
                                <ul class="ove-detail-list">

                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        <h5>بلد الإقامة الأن</h5>
                                        <span>
                                            <?=(isset($profile_data->country->ar_name))? $profile_data->country->ar_name:"";?>
                                        </span>
                                    </li>

                                    <li>
                                        <i class="fa fa-building"></i>
                                        <h5>العنوان </h5>
                                        <span><?=(isset($profile_data->city->ar_city_title ))? $profile_data->city->ar_city_title :"";?> - <?=$profile_data->address?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-user"></i>
                                        <h5>المؤهل الدراسي</h5>
                                        <span> <?= (isset($client_data->qualification->ar_title) ? $client_data->qualification->ar_title : "") ?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-envelope-o"></i>
                                        <h5>البريد الإلكتروني</h5>
                                        <span><?=$profile_data->email?></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-mobile"></i>
                                        <h5>رقم الهاتف</h5>
                                        <span><?=$profile_data->phone?></span>
                                    </li>
                                    <li>
                                        <i class="fa fa-whatsapp"></i>
                                        <h5>رقم الواتساب</h5>
                                        <span><?=$profile_data->whatsapp_num?></span>
                                    </li>

                                </ul>
                            </div>
                        </div>

                    </div>
                    <!-- /Company overview -->



                    <!-- Follow Links -->
                    <div class="sidebar-widgets">

                        <div class="ur-detail-wrap">
                            <div class="ur-detail-wrap-header">
                                <h4>تابعني على</h4>
                            </div>
                            <div class="ur-detail-wrap-body">
                                <ul class="follow-links">
                                    <?php if (isset($social_data)): ?>
                                        <li><a href="<?= $social_data->facebook ?>">
                                                <i class="ti-facebook"></i>Facebook</a>
                                        </li>
                                        <li><a href="<?= $social_data->twitter ?>">
                                                <i class="ti-twitter-alt"></i>Twitter</a>
                                        </li>
                                        <li><a href="<?= $social_data->linkedin ?>">
                                                <i class="ti-linkedin"></i>Linked In</a>
                                        </li>
                                        <li><a href="<?= $social_data->instagram ?>">
                                                <i class="ti-instagram"></i>Instagram</a>
                                        </li>
                                    <?php else: ?>
                                        <li><a href="#"><i class="ti-facebook"></i>Facebook</a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i>Twitter</a></li>
                                        <li><a href="#"><i class="ti-linkedin"></i>Linked In</a></li>
                                        <li><a href="#"><i class="ti-instagram"></i>Instagram</a></li>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <!-- /Working Days -->



                </div>
            </div>

        </div>
    </div>
</section>
<!-- company full detail End -->



