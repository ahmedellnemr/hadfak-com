
<section class="companies ptop-30 pbottom-30">
    <div class="container-fluid">
        <?php if(!empty($company)){
            foreach ($company as $row): ?>
        <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
            <article class="company-box">
                <div class="company-img">
                    <a href="<?=base_url().'show-company/'.$row->user_id_fk?>">
                        <?php if (isset($row->user_data->logo) && !empty($row->user_data->logo) && is_file(IMAGEPATH . $row->user_data->logo)): ?>
                            <img src="<?= base_url() . IMAGEPATH . $row->user_data->logo ?>" class="img-responsive"
                                 alt=""/>
                        <?php else: ?>
                            <img src="<?= base_url() . FAVICONPATH . COMPANEIMAGE?>" class="img-responsive" alt=""/>
                        <?php endif ?>
                    </a>
                </div>
                <div class="companm-det text-center">
                    <a href="<?=base_url().'show-company/'.$row->user_id_fk?>">
                        <h5><?= (isset($row->user_data->name)) ? $row->user_data->name : ""; ?></h5>
                    </a>
                    <div class="company-location">
                        <p>
                            <i class="fa fa-map-marker"></i>
                            <?= (isset($row->country->ar_name)) ? $row->country->ar_name : ""; ?> ,
                            <?= (isset($row->city->ar_city_title)) ? $row->city->ar_city_title : ""; ?> ,
                            <?= (isset($row->user_data->address)) ? $row->user_data->address : ""; ?>
                        </p>
                    </div>
                    <p class="text-center">(<?=count($row->jobs)?> وظائف مطلوبة)</p>
                </div>
            </article>
        </div>
        <?php  endforeach;
           }else{?>
            <div class="alert alert-danger" role="alert"> لا توجد نتائج   </div>
        <?php }?>

      <!--  <div class="col-xs-12">
            <ul class="pagination">
                <li><a href="#">«</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                <li><a href="#">»</a></li>
            </ul>
        </div>
-->
    </div>
</section>

