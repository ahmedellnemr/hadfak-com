<?php
if (!empty($persons)) {
    foreach ($persons as $row): ?>
        <div class="col-lg-<?=(isset($div))? $div:"3"?> col-md-<?=(isset($div))? $div:"3"?> col-sm-4 col-xs-12">
            <div class="manage-cndt">
                <div class="cndt-status pending">
                    <?= (isset($row->experience_year->ar_title)) ? $row->experience_year->ar_title : "سنوات"; ?>
                </div>
                <div class="cndt-caption">
                    <div class="cndt-pic">
                        <?php if (isset($row->user_data->logo) && !empty($row->user_data->logo) && is_file(IMAGEPATH . $row->user_data->logo)): ?>
                            <img src="<?= base_url() . IMAGEPATH . $row->user_data->logo ?>" class="img-responsive" alt=""/>
                        <?php else: ?>
                            <img src="<?= base_url() . FAVICONPATH .USERIMAGE?>" class="img-responsive" alt=""/>
                        <?php endif ?>
                    </div>
                    <h4><?= (isset($row->user_data->name)) ? $row->user_data->name : ""; ?></h4>
                    <span><?= (isset($row->job_title->ar_title) ? $row->job_title->ar_title : "") ?></span>
                    <p><?= (isset($row->user_data->about)) ? word_limiter($row->user_data->about, 12) : ""; ?></p>
                </div>
                <a href="<?= base_url() . "show-user/" . $row->user_id_fk ?>" title="" class="cndt-profile-btn">مشاهدة
                    الصفحة الشخصية</a>
            </div>
        </div>
    <?php endforeach;
}
else{
    echo '<div class="alert alert-danger" role="alert"> لا توجد نتائج   </div>';


}
?>