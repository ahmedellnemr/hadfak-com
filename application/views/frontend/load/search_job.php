<?php
if (!empty($jobs)) {
foreach ($jobs as $row): ?>
<div class="col-lg-<?=(isset($div))? $div:"3"?> col-md-<?=(isset($div))? $div:"3"?> col-sm-4 col-xs-12">
    <div class="grid-view brows-job-list">
        <div class="brows-job-company-img">
            <?php if (isset($row->user_data->logo) && !empty($row->user_data->logo) && is_file(IMAGEPATH . $row->user_data->logo)): ?>
                <img src="<?= base_url() . IMAGEPATH . $row->user_data->logo ?>" class="img-responsive" alt=""/>
            <?php else: ?>
                <img src="<?= base_url() . FAVICONPATH .JOBIMAGE?>" class="img-responsive" alt=""/>
            <?php endif ?>
        </div>
        <div class="brows-job-position">
            <h3>
                <a href="<?=base_url()."show-job/".$row->id."/".$row->company_id_fk ?>">
                    <?= (isset($row->job_title->ar_title) ? $row->job_title->ar_title : "") ?>
                </a>
            </h3>
            <p><span><?= (isset($row->user_data->name) ? $row->user_data->name : "") ?></span></p>
        </div>
        <div class="job-position">
            <span class="job-num"><?=$row->positions?> وظيفة متاحة </span>
        </div>
        <div class="brows-job-type">
            <span class="full-time"><?= (isset($row->type_work->ar_title) ? $row->type_work->ar_title : "") ?></span>
        </div>
        <ul class="grid-view-caption">
            <li>
                <div class="brows-job-location">
                    <p>
                        <i class="fa fa-map-marker"></i>
                        <?= (isset($row->city->ar_city_title)) ? $row->city->ar_city_title : ""; ?> ,
                        <?= (isset($row->user_data->address)) ? $row->user_data->address : ""; ?>
                    </p>
                </div>
            </li>
            <li>
                <p>
                    <?php if (!empty($row->salary) && $row->salary !=null): ?>
                        <span class="brows-job-sallery"><i class="fa fa-money"></i>
                         <?= $row->salary ?>
                    </span>
                    <?php endif ?>
                </p>
            </li>
        </ul>
     <!--   <span class="tg-themetag tg-featuretag">Premium</span>-->
    </div>
</div>
<?php endforeach;
}
else{
    echo '<div class="alert alert-danger" role="alert"> لا توجد نتائج   </div>';


}
?>