
<?php if (!empty($profile_data->banner) && is_file(IMAGEPATH . $profile_data->banner)){
    $banner  = base_url().IMAGEPATH .$profile_data->banner;
 }
 else{
    if(!empty($all_imgs[0]->person_image)){ $banner  =  base_url() .IMAGEPATH .$all_imgs[0]->person_image; }
    else{$banner  = base_url().WEBASSETS .'img/banner-10.jpg';}
      
 }
   

 
?>
<section class="inner-header-title" style="background-image:url(<?=$banner?>);">
    <div class="container">
        <h1><?=$profile_data->name?></h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Title Header End -->

<!-- Candidate Profile Start -->
<section class="detail-desc advance-detail-pr gray-bg">
    <div class="container">
        <div class="ur-detail-wrap create-kit padd-bot-0">

            <div class="row">
                <div class="detail-pic">
                    <?php if ( !empty($profile_data->logo) && is_file(IMAGEPATH . $profile_data->logo)): ?>
                        <img src="<?= base_url() . IMAGEPATH . $profile_data->logo ?>" class="img" alt=""/>
                    <?php else: ?>
                        <img src="<?= base_url() . FAVICONPATH ?>client.png" class="img" alt=""/>
                    <?php endif ?>
                    <a href="#simple-design-tab" class="detail-edit" title="edit"><i class="fa fa-pencil"></i></a>
                </div>
                <div class="detail-status">
                    <span> <?=($profile_data->is_login == 1)? "Active Now":"Not Active"?> </span>
                </div>
            </div>

            <div class="row bottom-mrg">
                <div class="col-md-12 col-sm-12">
                    <div class="advance-detail detail-desc-caption">
                        <h4><?=$profile_data->name?></h4>
                        <span class="designation"><?=(isset($client_data->job_title->ar_title)? $client_data->job_title->ar_title:"")?></span>
                        <ul>
                            <li>
                                <?php if ($jobs_for_me > 0 ): ?>
                                  <a href="<?=base_url()."get-appliers?type=jobs&job_title_id=".$client_data->job_title_id_fk?>">
                                    <strong class="j-view"><?= $jobs_for_me ?></strong>
                                  </a>
                                <?php else: ?>
                                    <strong class="j-view"><?= $jobs_for_me ?></strong>
                                <?php endif ?>

                                وظيفة تناسبك</li>
                            <li><strong class="j-applied"><?=$com_for_me?></strong>شركات نفس مجالك</li>
                            <li><strong class="j-shared"><?=$total_jobs?></strong>Job Shared</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row no-padd">
                <div class="detail pannel-footer">
                    <div class="col-md-5 col-sm-5">
                        <ul class="detail-footer-social">
                            <?php if (isset($social_data)): ?>
                                <li><a href="<?= $social_data->facebook ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?= $social_data->gmail ?>"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="<?= $social_data->twitter ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?= $social_data->linkedin ?>"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="<?= $social_data->instagram ?>"><i class="fa fa-instagram"></i></a></li>
                            <?php else: ?>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <?php endif ?>
                        </ul>
                    </div>
                    <div class="col-md-7 col-sm-7">
                        <div class="detail-pannel-footer-btn pull-right">
                            <a href="#simple-design-tab"  class="footer-btn grn-btn" id="changetabbutton" title="">تعديل الان</a>
                            <a href="#" class="footer-btn blu-btn" title="">بحث عن وظيفة</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="full-detail-description full-detail gray-bg">
    <div class="container">
        <div class="col-md-12 col-sm-12 no-pxs">
            <div class="full-card">
                <div class="deatil-tab-employ tool-tab">
                    <ul class="nav simple nav-tabs" id="simple-design-tab" role="tablist">
                        <li role="presentation" class="active"><a href="#about" role="tab"  data-toggle="tab">نبذة عن</a></li>
                        <li role="presentation"><a href="#address" role="tab"  data-toggle="tab">معلومات التواصل</a></li>
                        <li role="presentation"><a href="#cv-temp" role="tab"  data-toggle="tab">السيرة الذاتية</a></li>
                        <li role="presentation"><a href="#post-job" role="tab"  data-toggle="tab">وظائف مقترحة</a></li>
                        <li role="presentation"><a href="#profile-edit" role="tab"  data-toggle="tab">تعديل الصفحة الشخصية</a></li>
                        <li role="presentation"><a href="#social-edit" role="tab"  data-toggle="tab">تعديل بيانات التواصل الإجتماعي</a></li>
                        <li role="presentation"><a href="#settings" role="tab"  data-toggle="tab">الإعدادات</a></li>
                    </ul>
                    <!-- Start All Sec -->
                    <div class="tab-content">
                        <!-- Start About Sec -->
                        <div id="about" class="tab-pane fade in active">
                            <?php $this->load->view('frontend/tabs/tab_about');?>
                        </div>
                        <!-- End About Sec -->
                        <!-- Start Address Sec -->
                        <div id="address" class="tab-pane fade">
                            <?php $this->load->view('frontend/tabs/tab_address');?>
                        </div>
                        <!-- End Address Sec -->
                        <div id="cv-temp" class="tab-pane fade">
                            <div class="download-cv">
                                <?php
                                if(isset($client_data)){
                                    $filePath = base_url().'read?file='.$client_data->cv_file ;
                                    $downPath = base_url().'uploads/images/'.$client_data->cv_file ;
                                    $target = 'target="_blank"';
                                ?>
                                <a href="<?=$downPath?>" download="">
                                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                    <h5>تحميل السيرة الذاتية</h5>
                                </a>
                                <br>
                                <a <?=$target?> href="<?=$filePath?>" class="btn btn-primary"> عرض السيرة الذاتية</a>
                               <?php }else{?>
                                    <div class="alert alert-warning" role="alert">
                                        لم يتم إضافة السيرة الذاتية
                                    </div>
                                <?php }?>

                            </div>
                        </div>
                        <!-- End Job List -->
                        <!-- Start Job List -->
                        <div id="post-job" class="tab-pane fade">
                            <h3>لديك <?=count($com_jobs)?> وظيفة مرتبطة بمجالك</h3>
                            <div class="row">
                                <?php if (isset($com_jobs) && !empty($com_jobs)) { ?>
                                    <?php foreach ($com_jobs as $row): ?>
                                        <article>
                                            <div class="mng-company">
                                                <div class="col-md-2 col-sm-2 col-xs-3 no-pxs">
                                                    <div class="mng-company-pic">
                                                        <?php if ( !empty($row->logo) && is_file(IMAGEPATH . $row->logo)): ?>
                                                            <img src="<?= base_url() . IMAGEPATH . $row->logo ?>" class="img-responsive" alt=""/>
                                                        <?php else: ?>
                                                            <img src="<?=base_url().FAVICONPATH?>job.png" class="img-responsive" alt="">
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-5 col-xs-9 pad-4">
                                                    <div class="mng-company-name">
                                                        <a href="<?=base_url()."show-job/".$row->id."/".$row->company_id_fk?>">
                                                            <h5> <span class="cmp-tagline"><?=$row->ar_title?></span></h5>
                                                        </a>
                                                        <span class="cmp-time"><?=calculate_from_time($row->created_at)?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-12 pad-4">
                                                    <div class="mng-company-location">
                                                        <p>
                                                            <i class="fa fa-home"></i>
                                                            <?=$row->name?>
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--  <span class="tg-themetag tg-featuretag">Premium</span>-->
                                        </article>
                                    <?php endforeach ?>
                                <?php } else { ?>
                                    <h4>يرجى إستكمال بياناتك حتى تستطيع مشاهدة وظائف مقترحة لك</h4>
                                <?php } ?>
                            </div>
                           <!-- <div class="row">
                                <ul class="pagination">
                                    <li><a href="#">«</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                                    <li><a href="#">»</a></li>
                                </ul>
                            </div>-->
                        </div>
                        <!-- End Job List -->
                        <div id="profile-edit" class="tab-pane fade">
                            <div class="row no-mrg">
                                <h3>تعديل الصفحة الشخصية</h3>
                                <div class="edit-pro">
                                    <?= form_open_multipart('update-profile/'.$profile_data->user_id);  ?>
                                    <div class="col-md-4 col-sm-6">
                                        <label>الإسم بالكامل</label>
                                        <input type="text" name="Pdata[name]" class="form-control" value="<?=$profile_data->name?>"  data-validation="required">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>تاريخ الميلاد</label>
                                        <?php if (isset($client_data->date_of_birth)): ?>
                                            <input type="hidden" name="status" value="update">
                                            <input type="text" name="Pdata_client[date_of_birth]" class="form-control datepicker" value="<?=date("Y-m-d",$client_data->date_of_birth)?>"
                                                   data-validation="required">
                                        <?php else: ?>
                                            <input type="hidden" name="status" value="insert">
                                            <input type="text" name="Pdata_client[date_of_birth]" class="form-control datepicker" value=""
                                                   data-validation="required">
                                        <?php endif ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>الجنسية</label>
                                        <select name="Pdata_client[nationality]" class="form-control selectpicker " id="j-category" data-validation="required" aria-required="true"  data-show-subtext="true" data-live-search="true" >
                                            <option value="">اختر الجنسية</option>
                                            <?php if (isset($all_countries)): ?>
                                                <?php foreach ($all_countries as $row):
                                                    $sel = "";
                                                    if(isset($client_data->nationality)){
                                                        if($client_data->nationality == $row->id_country){
                                                            $sel = "selected";
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?=$row->id_country?>" <?=$sel?> ><?=$row->ar_nationality?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف الجنسيات</option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>بلد الإقامة الأن</label>
                                        <select name="Pdata[country_id]" class="form-control selectpicker " id="country-ids"  data-show-subtext="true" data-live-search="true" >
                                            <option value="">اختر الدولة</option>
                                            <?php if (isset($all_countries)): ?>
                                                <?php foreach ($all_countries as $row):
                                                    $sel = "";
                                                    if($profile_data->country_id == $row->id_country){
                                                        $sel = "selected";
                                                    }?>
                                                    <option value="<?=$row->id_country?>" <?=$sel?>><?=$row->ar_name?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف الدول</option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>المدينة</label>
                                        <select name="Pdata[city_id]" class="form-control selectpicker " id="city-ids"  data-show-subtext="true" data-live-search="true" >
                                            <?php if(!empty($profile_data->city_id)):?>
                                                <option value="">اختر المدينة</option>
                                                <?php if (isset($all_cities)): ?>
                                                    <?php foreach ($all_cities as $row):
                                                        $sel = "";
                                                        if($profile_data->city_id == $row->id_city){
                                                            $sel = "selected";
                                                        }?>
                                                        <option value="<?=$row->id_city?>" <?=$sel?> ><?=$row->ar_city_title?></option>
                                                    <?php endforeach ?>
                                                <?php else: ?>
                                                    <option value="">أضف المدن</option>
                                                <?php endif ?>
                                            <?php else:?>
                                                <option value="">اختر الدولة أولا  </option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>العنوان</label>
                                        <input name="Pdata[address]" type="text" value="<?=$profile_data->address?>" class="form-control" data-validation="required">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>البريد الإلكتروني</label>
                                        <input type="text" name="Pdata[email]" value="<?=$profile_data->email?>"
                                               class="form-control unique-field" field-name="email" data-db="registrations"
                                               data-validation="email">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>رقم الهاتف</label>
                                        <input type="text" maxlength="12" name="Pdata[phone]" value="<?=$profile_data->phone?>"
                                               class="form-control unique-field" field-name="phone" data-db="registrations"
                                               data-validation="required">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>رقم الواتس اب</label>
                                        <input type="text" maxlength="12" name="Pdata[whatsapp_num]" value="<?=$profile_data->whatsapp_num?>"
                                               class="form-control unique-field" field-name="phone" data-db="registrations"
                                               data-validation="required">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>المؤهل الدراسي</label>
                                        <select name="Pdata_client[qualification_id_fk]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                                            <option value="">اختر المؤهل</option>
                                            <?php if (isset($all_qualifications)): ?>
                                                <?php foreach ($all_qualifications as $row):
                                                $sel = "";
                                                if (isset($client_data->qualification_id_fk)) {
                                                    if ($client_data->qualification_id_fk == $row->id_definition) {
                                                        $sel = "selected";
                                                    }
                                                } ?>
                                                    <option value="<?=$row->id_definition?>"  <?=$sel?> ><?=$row->ar_title?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف المؤهل</option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>تاريخ الحصول عليه</label>
                                        <?php if (isset($client_data->qualification_date)): ?>
                                            <input type="text" name="Pdata_client[qualification_date]"  value="<?=date("Y-m-d",$client_data->qualification_date)?>"
                                                   class="form-control datepicker" data-validation="required">
                                        <?php else: ?>
                                            <input type="text" name="Pdata_client[qualification_date]"
                                                   class="form-control datepicker" data-validation="required">
                                        <?php endif ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>المسمى الوظيفي</label>
                                        <select name="Pdata_client[job_title_id_fk]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                                            <option value="">اختر المسمى الوظيفي</option>
                                            <?php if (isset($all_job_titles) && !empty($all_job_titles)): ?>
                                                <?php foreach ($all_job_titles as $row):
                                                    $sel = "";
                                                    if (isset($client_data->job_title_id_fk)) {
                                                        if ($client_data->job_title_id_fk == $row->job_id) {
                                                            $sel = "selected";
                                                        }
                                                    }  ?>
                                                    <option value="<?=$row->job_id."##".$row->activity_id_fk?>" <?=$sel?>><?=$row->ar_title?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف المسمى الوظيفي</option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>اجمالى سنوات الخبرة</label>
                                        <select name="Pdata_client[experience_years]" class="form-control" id="j-category"  data-validation="required" aria-required="true" >
                                            <option value="">اختر العدد</option>
                                            <?php if (isset($all_experience_yearss)):?>
                                                <?php foreach ($all_experience_yearss as $row):
                                                $sel = "";
                                                if (isset($client_data->job_title_id_fk)) {
                                                    if ($client_data->experience_years == $row->id_definition) {
                                                        $sel = "selected";
                                                    }
                                                }?>
                                                    <option value="<?=$row->id_definition?>" <?=$sel?>><?=$row->ar_title?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <option value="">أضف العدد</option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>نبذة عنك</label>
                                        <textarea name="Pdata[about]" class="form-control"
                                                  placeholder="يظهر للشركات الباحثة عنك"
                                                  data-validation="required"><?=$profile_data->about?></textarea>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>الخبرات الوظيفية</label>
                                        <textarea name="Pdata_client[experience]" class="form-control"
                                                  placeholder="يظهر للشركات الباحثة عنك"
                                                  data-validation="required">
                                            <?=(isset($client_data->experience))? $client_data->experience:"";?>
                                        </textarea>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>تحميل صورة شخصية</label>
                                        <?php if ( !empty($profile_data->logo) && $profile_data->logo != null) { ?>
                                            <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"
                                                   data-default-file="<?php echo base_url() .IMAGEPATH .$profile_data->logo  ?>"/>
                                        <?php } else { ?>
                                            <input type="file" id="input-file-now-custom-1" name="logo" class="dropify"/>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>تحميل صورة الغلاف</label>
                                        <?php if ( !empty($profile_data->banner) && $profile_data->banner != null) { ?>
                                            <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"
                                                   data-default-file="<?php echo base_url() .IMAGEPATH .$profile_data->banner  ?>"/>
                                        <?php } else { ?>
                                            <input type="file" id="input-file-now-custom-1" name="banner" class="dropify"/>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>تحميل السيرة الذاتية</label>
                                        <?php if ( isset($client_data->cv_file)) {
                                            $filePath = base_url().'read?file='.$client_data->cv_file ;
                                            $downPath = base_url().'read?file='.$client_data->cv_file ;
                                            $target = 'target="_blank"';
                                            ?>
                                            <input type="file" id="input-file-now-custom-1" name="cv_file" class="dropify"
                                                   data-default-file="<?php echo base_url() .FILESPATHS .$client_data->cv_file  ?>"/>

                                        <?php } else { ?>
                                            <input type="file" id="input-file-now-custom-1" name="cv_file" accept="application/pdf" class="dropify"/>
                                        <?php } ?>

                                    </div>

                                    <div class="col-sm-12 text-center mtop-20">
                                        <button type="submit" name="add" value="add"  class="btn btn-primary blu-btn update-btn">تعديل الأن</button>
                                    </div>
                                    <?=form_close()?>
                                </div>
                            </div>
                        </div>
                        <!-- End Job List -->
                        <!-- End Job List -->
                        <div id="social-edit" class="tab-pane fade">
                            <?php $this->load->view('frontend/tabs/tab_social_edit');?>
                        </div>
                        <!-- End Job List -->
                        <!-- Start Settings -->
                        <div id="settings" class="tab-pane fade">
                            <?php $this->load->view('frontend/tabs/tab_settings');?>
                        </div>
                        <!-- End Settings -->
                    </div>
                    <!-- Start All Sec -->
                </div>
            </div>
        </div>
    </div>
</section>



