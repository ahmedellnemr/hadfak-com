<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/5/2019
 * Time: 11:27 PM
 */
class Registrations_company_model extends MY_Model {
    public $_table = ' registrations_company';
    public $primary_key = 'user_id_fk';
    public $belongs_to = [
        'user_data' => array( 'model' => 'Registrations_model',"primary_key"=>'user_id_fk' ),
        'activity' => array( 'model' => 'Company_activities_model',"primary_key"=>'activity_id_fk' ),
        'number_employees_data' => array( 'model' => 'Definitions_model',"primary_key"=>'number_employees' ),
        'country' => array( 'model' => 'Countries_model',"primary_key"=>'country_id' ),
        'city' => array( 'model' => 'Cities_model',"primary_key"=>'city_id' )
    ];

    public $has_many = ['jobs' => array( 'model' => 'Company_jobs_model','primary_key' => 'company_id_fk' )];

    public function searchBy($where = [] ,$like = false ,$whereIn = []){
        $this->db->select('registrations_company.* , registrations.city_id ,registrations.country_id ');
        $this->db->from($this->_table);
        $this->db->join('registrations', 'registrations.user_id = registrations_company.user_id_fk',"left");
		if (isset($whereIn["activity_id_fk"])  ) {
			$this->db->where_in("registrations_company.activity_id_fk",$whereIn["activity_id_fk"]);
		}
		if (isset($whereIn["country_id"])  ) {
			$this->db->where_in("registrations.country_id",$whereIn["country_id"]);
		}
        if (!empty($where)) {
            $this->db->where($where);
        }
        if ($like != false) {
            $this->db->like('name',$like, 'both');
        }
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
            $data = $this->get_with_in($query->result(),["user_data",'activity','country','city',"jobs"]);
         // $data = $query->result();
        }
        return $data ;
    }

    public function getCompanies(){
        $this->db->select('registrations.* ,  registrations_company.activity_id_fk');
        $this->db->from('registrations');
        $this->db->join($this->_table, 'registrations.user_id = registrations_company.user_id_fk',"left");
        $this->db->where('registrations.user_type',2);
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
            $data = $this->get_with_in($query->result(),['activity']);
        }
        return $data ;
    }


} // END CLASS
