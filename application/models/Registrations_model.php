<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/5/2019
 * Time: 11:27 PM
 */
class Registrations_model extends MY_Model {
    public $_table = 'registrations';
    public $primary_key = 'user_id';
    public $belongs_to = [
        'country' => array( 'model' => 'Countries_model',"primary_key"=>'country_id' ),
        'city' => array( 'model' => 'Cities_model',"primary_key"=>'city_id' )
    ];

    public $before_create = array( 'timestamps_in' );
    public $before_update = array( 'timestamps_up' );

    protected function timestamps_in($row)
    {
        $row['created_at'] = $row['updated_at'] =  time();
        return $row;
    }
    protected function timestamps_up($row)
    {
        $row['updated_at'] =  time();
        return $row;
    }




} // END CLASS