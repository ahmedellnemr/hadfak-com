<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/5/2019
 * Time: 11:27 PM
 */

// ALTER TABLE `apply_jobs` ADD `created_at` INT NOT NULL AFTER `company_id`, ADD `updated_at` INT NOT NULL AFTER `created_at`;
class Apply_jobs_model extends MY_Model {
    public $_table = 'apply_jobs';
    public $primary_key = 'id';
    public $belongs_to = [
        'user_data' => array( 'model' => 'Registrations_model',"primary_key"=>'user_id' ),
        'com_data' => array( 'model' => 'Registrations_model',"primary_key"=>'company_id' ),
        'client' => array( 'model' => 'Registrations_client_model',"primary_key"=>'user_id' ),
        'job' => array( 'model' => 'Job_titles_model',"primary_key"=>'job_id' )
    ];

    public $before_create = array( 'timestamps_in' );
    public $before_update = array( 'timestamps_up' );

    protected function timestamps_in($row)
    {
        $row['created_at'] = $row['updated_at'] =  time();
        return $row;
    }
    protected function timestamps_up($row)
    {
        $row['updated_at'] =  time();
        return $row;
    }

    public function isApply($Conditions_arr){
        $this->db->from($this->_table);
        $this->db->where($Conditions_arr);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        }
        return true;
    }

    public function getApplies(){
        $this->db->select('apply_jobs.* ,  company_jobs.job_title_id');
        $this->db->from($this->_table);
        $this->db->join('company_jobs', 'apply_jobs.job_id = company_jobs.id',"left");
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
            $data = $this->get_with_in($query->result(),['job','user_data','com_data']);
        }
        return $data ;
    }



} // END CLASS