<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/5/2019
 * Time: 11:27 PM
 */
class Registrations_client_model extends MY_Model {
    public $_table = 'registrations_client';
    public $primary_key = 'user_id_fk';
    public $belongs_to = [
        'user_data' => array( 'model' => 'Registrations_model',"primary_key"=>'user_id_fk' ),
        'qualification' => array( 'model' => 'Definitions_model',"primary_key"=>'qualification_id_fk' ),
        'job_title' => array( 'model' => 'Job_titles_model',"primary_key"=>'job_title_id_fk' ),
        'activity' => array( 'model' => 'Company_activities_model',"primary_key"=>'activity_id_fk' ),
        'experience_year' => array( 'model' => 'Definitions_model',"primary_key"=>'experience_years' ),
        'nationality_data' => array( 'model' => 'Countries_model',"primary_key"=>'nationality' )
    ];

    public function searchBy($where = [] ,$like = false ,$whereIn = []){
        $this->db->select('registrations_client.*');
        $this->db->from($this->_table);
        $this->db->join('registrations', 'registrations.user_id = registrations_client.user_id_fk',"left");
        if (isset($whereIn["job_title_id_fk"]) ) {
            $this->db->where_in("registrations_client.job_title_id_fk",$whereIn["job_title_id_fk"]);
        }
        if (isset($whereIn["nationality"])  ) {
            $this->db->where_in("registrations_client.nationality",$whereIn["nationality"]);
        }
        if (isset($whereIn["country_id"])  ) {
            $this->db->where_in("registrations.country_id",$whereIn["country_id"]);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        if ($like != false) {
            $this->db->like('name',$like, 'both');
        }
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
            $data = $this->get_with_in($query->result(),["user_data",'job_title','experience_year']);
        }
        return $data ;
    }


    public function getClients(){
        $this->db->select('registrations.* ,  registrations_client.job_title_id_fk,registrations_client.cv_file');
        $this->db->from('registrations');
        $this->db->join($this->_table, 'registrations_client.user_id_fk = registrations.user_id',"left");
        $this->db->where('registrations.user_type',1);
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
            $data = $this->get_with_in($query->result(),['job_title']);
        }
        return $data ;
    }




} // END CLASS
