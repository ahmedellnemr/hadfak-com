<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/5/2019
 * Time: 11:27 PM
 */
class Definitions_model extends MY_Model {
    public $_table = 'definitions';
    public $primary_key = 'id_definition';

    /*
     protected $soft_delete = TRUE;
     protected $soft_delete_key = 'available';

     public $has_many = ['subs' => array( 'model' => 'Branchs_images_model','primary_key' => 'main_id' )];

     public $before_create = array( 'timestamps_in' );
     public $before_update = array( 'timestamps_up' );

     protected function timestamps_in($row)
     {
         $row['created_at'] = $row['updated_at'] =  date('Y-m-d H:i:s');
         return $row;
     }
     protected function timestamps_up($row)
     {
         $row['updated_at'] =  date('Y-m-d H:i:s');
         return $row;
     }

     */

    public $belongs_to = [
        'main' => array( 'model' => 'Definitions_model',"primary_key"=>'type' )
    ];


} // END CLASS