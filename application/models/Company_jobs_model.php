<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/5/2019
 * Time: 11:27 PM
 */
class Company_jobs_model extends MY_Model {
    public $_table = 'company_jobs';
    public $primary_key = 'id';
    public $belongs_to = [
        'user_data' => array( 'model' => 'Registrations_model',"primary_key"=>'company_id_fk' ),
        'activity' => array( 'model' => 'Company_activities_model',"primary_key"=>'activity_id_fk' ),
        'job_title' => array( 'model' => 'Job_titles_model',"primary_key"=>'job_title_id' ),
        'type_work' => array( 'model' => 'Definitions_model',"primary_key"=>'type_work_id' ),

        'qualification' => array( 'model' => 'Definitions_model',"primary_key"=>'qualification_id_fk' ),
        'experience_year' => array( 'model' => 'Definitions_model',"primary_key"=>'experience_years' ),

        'country' => array( 'model' => 'Countries_model',"primary_key"=>'country_id' ),
        'city' => array( 'model' => 'Cities_model',"primary_key"=>'city_id' )
    ];

    public function searchBy($where = [] ,$like = false ,$whereIn = []){
        $this->db->select('company_jobs.* , registrations.city_id , registrations.country_id ');
        $this->db->from($this->_table);
        $this->db->join('registrations', 'registrations.user_id = company_jobs.company_id_fk',"left");
        $this->db->join('job_titles', 'job_titles.job_id = company_jobs.job_title_id',"left");
        if (isset($whereIn["activity_id_fk"])  ) {
			$this->db->where_in("company_jobs.activity_id_fk",$whereIn["activity_id_fk"]);
		}
		if (isset($whereIn["country_id"])  ) {
			$this->db->where_in("registrations.country_id",$whereIn["country_id"]);
		}
        if (!empty($where)) {
            $this->db->where($where);
        }
        if ($like != false) {
            $this->db->like('job_titles.ar_title',$like, 'both');
        }
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
            $data = $this->get_with_in($query->result(),["user_data",'activity','country','city','job_title','type_work']);
            // $data = $query->result();
        }
        return $data ;
    }

    public function getCompanyJobs($id ){
        $this->db->select('company_jobs.id, , company_jobs.logo, company_jobs.created_at , job_titles.ar_title');
        $this->db->from($this->_table);
        $this->db->join('job_titles', 'job_titles.job_id = company_jobs.job_title_id',"left");
        $this->db->where('company_jobs.company_id_fk',$id);
        $this->db->where('company_jobs.available',1);
        $this->db->order_by('company_jobs.id',"DESC");
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
           // $data = $this->get_with_in($query->result(),['job_title']);
           $data = $query->result();
        }
        return $data ;
    }

    public function getUserJobs($id ){
        $this->db->select('company_jobs.*, job_titles.ar_title , registrations.name, registrations.city_id  ');
        $this->db->from($this->_table);
        $this->db->join('job_titles', 'job_titles.job_id = company_jobs.job_title_id',"left");
        $this->db->join('registrations', 'registrations.user_id = company_jobs.company_id_fk',"left");
        $this->db->where('company_jobs.job_title_id',$id);
        $this->db->where('company_jobs.available',1);
        $query = $this->db->get();
        $data  = [];
        if ($query->num_rows() > 0) {
            $data = $this->get_with_in($query->result(),['type_work','city','user_data']);
           // $data = $query->result();
        }
        return $data ;
    }


} // END CLASS
