<?php
/**
 * Created by PhpStorm.
 * User: win 7
 * Date: 11/5/2019
 * Time: 11:27 PM
 */
class Job_titles_model extends MY_Model {
    public $_table = 'job_titles';
    public $primary_key = 'job_id';
    /*
    protected $soft_delete = TRUE;
    protected $soft_delete_key = 'available';
    */
    public $belongs_to = [
        'activity' => array( 'model' => 'Company_activities_model',"primary_key"=>'activity_id_fk' )
    ];

} // END CLASS