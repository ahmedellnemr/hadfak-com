<?php
class Person extends MY_Controller{
    private $folder = "persons";
    private $con    = "admin-person";
    public function __construct(){
        parent::__construct();
        $this->load->model('Registrations_model','Cmodel');
        //  redirect("Page404", 'refresh');
        /*
         $main_iamge = $this->upload_image("");
            $Idata[""] = $main_iamge;
            $filesCount = $_FILES["images"]['size'][0];
            if ($filesCount != "0" || $filesCount != 0) {
                $imgs = $this->upload_muli_image("images");
                $this->Model_salon->add_images($id, $imgs);
            }

         $main_iamge = $this->upload_image("");
            if (!empty($main_iamge)) {
                $Idata[""] = $main_iamge;
            }

         */

    }

    public  function index(){
        $this->load->model('Registrations_client_model');
        $data["data_table"] = $this->Registrations_client_model->getClients();
       // $this->test($data);
       // die;
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'باحث عن عمل';
        $data["my_footer"] = ['table'];
        $data['subview'] = $this->folder.'/all';
        $this->load->view('layout/admin', $data);
    }

    public  function add(){

        //------------------------------------------------------------
        $this->load->model('Registrations_client_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
        $this->load->model('Definitions_model');
        $this->load->model('Job_titles_model');
        $data["all_countries"] = $this->Countries_model->get_all();
        //$data["all_cities"] = $this->Cities_model->get_all();
        $data["all_qualifications"] = $this->Definitions_model->get_many_by(["type"=>QUALIFICATIONKEY,"available"=>1]);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        $data["all_job_titles"] = $this->Job_titles_model->get_many_by(['available'=>1]);
        //-------------------------------------------------------------
        $data["op"] = 'INSERT';
        $data["form"] = $this->con.'/create';
        $data["profile_data"] = (object) $this->Cmodel->get_filds();
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'باحث عن عمل';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function edit($id){

        $this->load->model('Registrations_model');
        $this->load->model('Registrations_client_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
        $this->load->model('Definitions_model');
        $this->load->model('Job_titles_model');
        //--------------------------------------------------------
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);

        $data["client_data"] = $this->Registrations_client_model->with('qualification')
            ->with('job_title')->with('activity')->with('experience_year')->get($id);
        $data["social_data"] = $this->Registrations_social_model->get($id);
        $data["all_countries"] = $this->Countries_model->get_all();
        $data["all_cities"] = $this->Cities_model->get_all();
        $data["all_qualifications"] = $this->Definitions_model->get_many_by(["type"=>QUALIFICATIONKEY,"available"=>1]);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        $data["all_job_titles"] = $this->Job_titles_model->get_many_by(['available'=>1]);
        //------------------------------------------------------
        $data["op"] = 'UPDTATE';
        $data["form"] = $this->con.'/update/'.$id;
        $data["out"] = $this->Cmodel->as_array()->get($id);
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'باحث عن عمل';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function create(){

        if ($this->input->post('INSERT') == "INSERT") {
            //-------------------------------------------
            $this->load->model('Registrations_model');
            $Udata = $this->input->post('Pdata');
            $Udata["password"] = setPass($Udata["password"]);
            $Udata["created_at"] = time();
            $Udata["user_type"] = 1;
            $logo  = $this->upload_image("logo");
            if (!empty($logo)) {
                $Udata["logo"] = $logo;
            }
            $banner =  $this->upload_image("banner");
            if (!empty($banner)) {
                $Udata["banner"] = $banner;
            }
            $id = $this->Registrations_model->insert($Udata);
            //-------------------------------------------
            if($this->input->post('Pdata_client') ){
                $this->load->model('Registrations_client_model');
                $Cdata = $this->input->post('Pdata_client');
                $Cdata["cv_file"] = $this->upload_file("cv_file");
                $epx = explode("##",$Cdata["job_title_id_fk"]);
                $Cdata["job_title_id_fk"] =  (isset($epx[0]))? $epx[0]:"0" ;
                $Cdata["activity_id_fk"] =  (isset($epx[1]))? $epx[1]:"0" ;
                $Cdata["date_of_birth"] = strtotime($Cdata["date_of_birth"]);
                $Cdata["qualification_date"] = strtotime($Cdata["qualification_date"]);
                $Cdata['user_id_fk'] =$id;
                $this->Registrations_client_model->insert($Cdata);
            }
            //----------------------------------------------
            if($this->input->post('Psocial') ) {
                $this->load->model('Registrations_social_model');
                $Cdata = $this->input->post('Psocial');
                $Cdata['user_id_fk'] = $id;
                $this->Registrations_social_model->insert($Cdata);
            }
            //----------------------------------------------
            $this->message('s');
            redirect($this->con."/add", 'refresh');
        }
    }

    public  function update($id){

        if ($this->input->post('UPDTATE') == "UPDTATE") {
            $this->load->model('Registrations_model');
            //-------------------------------------------
            $Udata = $this->input->post('Pdata');
            if (isset($Udata["password"]) && !empty($Udata["password"])) {
                $Udata["password"] = setPass($Udata["password"]);
            }
            $Udata["updated_at"] = time();
            $logo  =$this->upload_image("logo");
            if (!empty($logo)) {
                $Udata["logo"] = $logo;
            }
            $banner =  $this->upload_image("banner");
            if (!empty($banner)) {
                $Udata["banner"] = $banner;
            }
            $this->Registrations_model->update($id,$Udata);
            //-------------------------------------------
            if($this->input->post('Pdata_client') ){
                $this->load->model('Registrations_client_model');

                $Cdata = $this->input->post('Pdata_client');
                $Cdata["cv_file"] = $this->upload_file("cv_file");
                $epx = explode("##",$Cdata["job_title_id_fk"]);
                $Cdata["job_title_id_fk"] =  (isset($epx[0]))? $epx[0]:"0" ;
                $Cdata["activity_id_fk"] =  (isset($epx[1]))? $epx[1]:"0" ;
                $Cdata["date_of_birth"] = strtotime($Cdata["date_of_birth"]);
                $Cdata["qualification_date"] = strtotime($Cdata["qualification_date"]);
              $this->Registrations_client_model->update($id, $Cdata);
            }
            //----------------------------------------------
            if($this->input->post('Psocial')){
                $this->load->model('Registrations_social_model');
                $Cdata = $this->input->post('Psocial');
                $this->Registrations_social_model->update($id, $Cdata);
            }
            //----------------------------------------------
            $this->message('i');
            redirect( $this->con, 'refresh');
        }

    }

    public function active($value, $id){
        $this->load->model('Registrations_model');
        $Udata["is_active"] = $value;
        $this->Registrations_model->update($id, $Udata);
        $this->message('i');
        redirect( $this->con, 'refresh');
    }

    public  function delete($id){
        $this->load->model('Registrations_social_model');
        $this->load->model('Registrations_client_model');
        $this->load->model('Registrations_model');
        $this->load->model('Apply_jobs_model');

        $this->Registrations_social_model->delete($id);
        $this->Registrations_client_model->delete($id);
        $this->Registrations_model->delete($id);
        $this->Apply_jobs_model->delete_by(["user_id"=>$id]);

        $this->message('e');
        redirect($this->con, 'refresh');
    }



} //END CLASS
?>