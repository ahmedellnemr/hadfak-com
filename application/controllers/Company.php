<?php
class Company extends MY_Controller{
    private $folder = "company";
    private $con    = "admin-company";
    public function __construct(){
        parent::__construct();
        $this->load->model('Registrations_company_model','Cmodel');
        //  redirect("Page404", 'refresh');
        /*
         $main_iamge = $this->upload_image("");
            $Idata[""] = $main_iamge;
            $filesCount = $_FILES["images"]['size'][0];
            if ($filesCount != "0" || $filesCount != 0) {
                $imgs = $this->upload_muli_image("images");
                $this->Model_salon->add_images($id, $imgs);
            }

         $main_iamge = $this->upload_image("");
            if (!empty($main_iamge)) {
                $Idata[""] = $main_iamge;
            }

         */
    }

    public  function index(){

        $data["data_table"] = $this->Cmodel->getCompanies();
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'أصحاب عمل (شركات)';
        $data["my_footer"] = ['table'];
        $data['subview'] = $this->folder.'/all';
        $this->load->view('layout/admin', $data);
    }

    public  function add(){

        $data["op"] = 'INSERT';
        $data["form"] = $this->con.'/create';
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
        $this->load->model('Definitions_model');
        $this->load->model('Company_activities_model');
        //----------------------------------------------
        $data["profile_data"] = (object) $this->Registrations_model->get_filds();
        //--------------------------------------------------------
        $data["all_countries"] = $this->Countries_model->get_all();
        //$data["all_cities"] = $this->Cities_model->get_all();
        $data["all_number_employees"] = $this->Definitions_model->get_many_by(["type"=>NUMBEREMPLOYEES,"available"=>1]);
        $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
        //----------------------------------------------
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'أصحاب عمل (شركات)';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function edit($id){
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
        $this->load->model('Definitions_model');
        $this->load->model('Company_activities_model');
        //----------------------------------------------
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
        $data["social_data"] = $this->Registrations_social_model->get($id);
        $data["company_data"] = $this->Registrations_company_model
            ->with('activity')->with('number_employees_data')->get($id);
        //--------------------------------------------------------
        $data["all_countries"] = $this->Countries_model->get_all();
        $data["all_cities"] = $this->Cities_model->get_all();
        $data["all_number_employees"] = $this->Definitions_model->get_many_by(["type"=>NUMBEREMPLOYEES,"available"=>1]);
        $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
        //----------------------------------------------
        $data["op"] = 'UPDTATE';
        $data["form"] = $this->con.'/update/'.$id;
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'أصحاب عمل (شركات)';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function create(){

        if ($this->input->post('INSERT') == "INSERT") {
            $this->load->model('Registrations_model');
            //-------------------------------------------
            $Udata = $this->input->post('Pdata');
            $Udata["password"] = setPass($Udata["password"]);
            $Udata["created_at"] = time();
            $Udata["user_type"] = 2;
            $logo  =$this->upload_image("logo");
            if (!empty($logo)) {
                $Udata["logo"] = $logo;
            }
            $banner =  $this->upload_image("banner");
            if (!empty($banner)) {
                $Udata["banner"] = $banner;
            }
            $id = $this->Registrations_model->insert($Udata);
            //----------------------------------------------
            if($this->input->post('Pdata_company') ){
                $this->load->model('Registrations_company_model');
                $Cdata = $this->input->post('Pdata_company');
                $Cdata["date_created"] = strtotime($Cdata["date_created"]);
                $Cdata['user_id_fk'] = $id;
                $this->Registrations_company_model->insert($Cdata);
            }
            //----------------------------------------------
            $this->load->model('Registrations_social_model');
            $Cdata = $this->input->post('Psocial');
            if($this->input->post('Psocial') ) {
                $Cdata['user_id_fk'] = $id;
                $this->Registrations_social_model->insert($Cdata);
            }
            //----------------------------------------------
            $this->message('s');
            redirect($this->con."/add", 'refresh');
        }

    }

    public  function update($id){

        if ($this->input->post('UPDTATE') == "UPDTATE") {
            $this->load->model('Registrations_model');
            //-------------------------------------------
            $Udata = $this->input->post('Pdata');
            if (isset($Udata["password"]) && !empty($Udata["password"])) {
                $Udata["password"] = setPass($Udata["password"]);
            }
            $Udata["updated_at"] = time();
            $logo  =$this->upload_image("logo");
            if (!empty($logo)) {
                $Udata["logo"] = $logo;
            }
            $banner =  $this->upload_image("banner");
            if (!empty($banner)) {
                $Udata["banner"] = $banner;
            }
            $this->Registrations_model->update($id,$Udata);
            //----------------------------------------------
            if($this->input->post('Pdata_company') ){
                $this->load->model('Registrations_company_model');
                $Cdata = $this->input->post('Pdata_company');
                $this->Registrations_company_model->update($id , $Cdata);
            }
            //----------------------------------------------
            $this->load->model('Registrations_social_model');
            $Cdata = $this->input->post('Psocial');
            if($this->input->post('Psocial') ) {
                $this->Registrations_social_model->update($id , $Cdata);
            }
            //----------------------------------------------
            $this->message('i');
            redirect( $this->con, 'refresh');
        }

    }

    public function active($value, $id){
        $this->load->model('Registrations_model');
        $Udata["is_active"] = $value;
        $this->Registrations_model->update($id, $Udata);
        $this->message('i');
        redirect( $this->con, 'refresh');
    }


    public  function delete($id){
        $this->load->model('Registrations_social_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Registrations_model');
        $this->load->model('Apply_jobs_model');
        $this->load->model('Company_jobs_model');

        $this->Registrations_social_model->delete($id);
        $this->Registrations_company_model->delete($id);
        $this->Registrations_model->delete($id);

        $this->Apply_jobs_model->delete_by(["user_id"=>$id]);
        $this->Company_jobs_model->delete_by(["company_id_fk"=>$id]);

        $this->message('e');
        redirect($this->con, 'refresh');
    }



} //END CLASS
?>