<?php
class JsControl extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Model_js_control');
    }
    public function index(){
        if ($this->input->post('unique_field') ){
            $field = $this->input->post('field_name');
            $table = $this->input->post('table');
            $value = $this->input->post('value');
            echo $this->Model_js_control->check_unique($table,$field,$value);
        }
    }
    //==================================================================================================================



}// end class 
?>