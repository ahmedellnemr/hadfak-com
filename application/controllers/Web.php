<?php
class Web extends CI_Controller
{
    public  function __construct(){
        parent::__construct();
        $this->load->helper('utility_helper');
        $this->load->helper('cookie');
        //--------------------------------------------------------
        $lang = $this->uri->segment(1);
        if(in_array($lang,array("ar","en"))){
            $this->webLang = $lang ;
            if ($lang == "ar") {
                $this->lang->load('static', 'ar');
                delete_cookie("last_lang");
                 set_cookie('last_lang',"ar",time()+86400*30);
            }else{
                $this->lang->load('static', 'en');
                delete_cookie("last_lang");
                 set_cookie('last_lang',"en",time()+86400*30);
            }
        }
        else{
            $cookeLang  = $this->input->cookie('last_lang');
            if(isset($cookeLang)){
                $this->webLang = $cookeLang;
                $this->lang->load('static', $cookeLang);
            }
            else{
            $this->webLang = 'ar' ;
            $this->lang->load('static', 'ar');
                set_cookie('last_lang',"ar",time()+86400*30);
            }
        }
        //-------------------------------------------------------
        $this->load->model('system_management/Setting_model');
        $this->load->model('Company_jobs_model');
        $this->load->model('Registrations_model');
        $this->setting  = $this->Setting_model->getSettings();
        $this->all_jobs = $this->Company_jobs_model->with("job_title")->order_by("id","DESC")->limit(6)->get_many_by(["available"=>1]);
        $this->all_com = $this->Registrations_model->order_by("user_id","DESC")->limit(6)->get_many_by(["user_type"=>2]);
		//-------------------------------------------------------
        $this->load->model('Job_titles_model');
        $this->load->model('Registrations_client_model');
        $this->load->model('Countries_model');
        $this->load->model('Definitions_model');
        $this->load->model('Bg_model');
       /* echo "<pre>";
        print_r($data["all_job_titles"]);
        die;*/
        //-------------------------------------------------------
        $cookeLastVisit  = $this->input->cookie('last_visit');
        if(isset($cookeLastVisit)){
            delete_cookie("last_visit");
            set_cookie('last_visit',strtotime(date("Y-m-d")),time()+86400*30);
        }
        else{
            set_cookie('last_visit',strtotime(date("Y-m-d")),time()+86400*30);
            $this->load->model('Model_visit');
            $this->Model_visit->insertVisitor(date("Y-m-d"),"web_count");
        }
		//-------------------------------------------------------
		$this->load->library('google');
		$this->googleLoginURL = $this->google->loginURL();
		//-------------------------------------------------------
		$this->load->library('facebook');
		// Facebook authentication url
		$this->facebookAuthURL =  $this->facebook->login_url();
		//-------------------------------------------------------
    }
    private function test($data = array()){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;
    }
    private function test_j($data = array()){
        header('Content-Type: application/json');
        echo  json_encode ($data);
        die;
    }
    private function test_r($data = array()){
        echo "<pre>";
        print_r($data);
        echo "</pre>";

    }
    private function sendEmail($data,$from,$to){
        //Load email library
        $emailConfig = array(
            'protocol' => 'mail',
            'smtp_port' => 25,
            'mailtype' => 'html',
            'validate' => true,
            'charset' => 'utf-8'
        );
        //------------------------------------
        $from = array(
            'email' => $from,
            'name' => 'موقع هدفك'
        );
        $to = array($to);
        $subject = ' موقع هدفك';
        $message = $this->load->view('frontend/requires/message_email', $data, true);
        //------------------------------------
        $this->load->library('email', $emailConfig);
        $this->email->set_newline("\r\n");
        $this->email->from($from['email']);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        // Ready to send email and check whether the email was successfully sent
        if (!$this->email->send()) {
            // Raise error message
            return show_error($this->email->print_debugger());
            // return 0;
        } else {
            // Show success notification or other things here
            return 'Success to send email';
        }
    }
    private function thumb($data){
        $config['image_library'] = 'gd2';
        $config['source_image'] =$data['full_path'];
        $config['new_image'] = THUMBPATH.$data['file_name'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['thumb_marker']='';
        $config['width'] = 275;
        $config['height'] = 250;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }
    private function upload_image($file_name){
        $config['upload_path'] = IMAGEPATH;
        $config['allowed_types'] = '*';
        $config['overwrite'] = true;
        $config['max_size']    = '1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
        $config['encrypt_name']=true;
        $this->load->library('upload',$config);
        if(! $this->upload->do_upload($file_name)){
            return  false;
            // return  $this->upload->display_errors();
        }else{
            $datafile = $this->upload->data();
            $this->thumb($datafile);
            return  $datafile['file_name'];
        }
    }
    private function upload_file($file_name){
        $config['upload_path'] = FILESPATHS;
        $config['allowed_types'] = '*';
        $config['overwrite'] = true;
        $config['max_size']    = '1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
        $config['overwrite'] = true;
        $this->load->library('upload',$config);
        if(! $this->upload->do_upload($file_name)){
            return  false;
        }else {
            $datafile = $this->upload->data();
            return $datafile['file_name'];
        }
    }

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */
    public function index(){

		$id = (isset($_SESSION['user_id']))? $_SESSION['user_id']:0;
        $data["all_imgs"] = $this->Bg_model->get_all();
        $data["all_job_titles"] = $this->Job_titles_model->order_by('ar_title', 'ASC')->get_many_by(['available'=>1]);
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        $data["all_countries"] = $this->Countries_model->get_all();
         $data['metadiscription']=$data['metakeyword']=$data['title']= "الرئيسية - هدفك دوت كوم";
         /*
           $data['subview'] = 'home';
           $this->load->view('layout/web', $data);
          */
          $this->load->view('frontend/home', $data);
    }

    public function contact(){
        if($this->input->post('name')  && $this->input->post('email') &&
           $this->input->post('message')
        ){
            $this->load->model('system_management/Contacts_model');
            $Idata['name'] = $this->input->post('name') ;
            $Idata['email'] = $this->input->post('email') ;
            $Idata['message'] = $this->input->post('message') ;
            $Idata['date'] = time() ;
            $this->Contacts_model->insert($Idata);
            //-----------------------------------
           // $this->sendEmail($Idata,$Idata['email'],$this->website->message_email);
            //-----------------------------------
        }
    }

    public function getCity($id){
        $this->load->model('Cities_model');
        $city = $this->Cities_model->get_many_by(["country_id"=>$id]);
        $out = ' <option value="">اختر </option>';
        if(!empty($city)){
            foreach ($city as $row):
                $out .= ' <option value="'.$row->id_city.'">'.$row->ar_city_title.' </option>';
            endforeach;
        }
        else{
            $out .= ' <option value="">لا يوجد مدن   </option>';
        }
        echo $out ;
    }

    public function readFile() {
        $file_name = $this->input->get('file');
        // $file_name = 'ed97631c96f672815a43b72e7e594090.pdf';
        $this->load->helper('file');
        $path_iamge = './uploads/images/';
        $path_files = './uploads/files/';
        if (file_exists($path_iamge . $file_name)) {
            $main_path = $path_iamge;
        } elseif (file_exists($path_files . $file_name)) {
            $main_path = $path_files;
        }
        $file_path = $main_path.$file_name;
        header('Content-Type: application/pdf');
        header('Content-Discription:inline; filename="'.$file_name.'"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges:bytes');
        header('Content-Length: ' . filesize($file_path));
        readfile($file_path);
    }

    public function download(){
        $file = $this->input->get('file');
        $this->load->helper('download');
        $name = $file;
        $path_iamge = './uploads/images/';
        $path_files = './uploads/files/';
        if(file_exists($path_iamge.$file)){
            $main_path=$path_iamge ;
        }
        elseif( file_exists($path_files.$file)){
            $main_path=$path_files  ;
        }
        $data = file_get_contents($main_path.$file);
        force_download($name, $data);
        if(isset($_SERVER['HTTP_REFERER'])){
            $previos_path = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
            redirect($previos_path,'refresh');
        }else{
            redirect("Web",'refresh');
        }
    }

    /**
     *
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */

    public function register(){
        $this->form_validation->set_rules('Pdata[name]', '', 'required');
        $this->form_validation->set_rules('Pdata[username]', '', 'required|is_unique[registrations.username]');
        $this->form_validation->set_rules('Pdata[password]', '', 'required');
        $this->form_validation->set_rules('Pdata[email]', '', 'required|is_unique[registrations.email]');
        $this->form_validation->set_rules('Pdata[user_type]', '', 'required|numeric|in_list[1,2]');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('frontend/home', []);
        }
        else{
            $this->load->model('Registrations_model');
            $Idata = $this->input->post("Pdata");
            $Idata["password"] = setPass($Idata["password"]);
            $Idata["created_at"] = time();

            $logo  =$this->upload_image("logo");
            if (!empty($logo)) {
                $Idata["logo"] = $logo;
            }
            $user_id  = $this->Registrations_model->insert($Idata);
            $id = $this->Registrations_model->insert($Idata);
            if($this->input->post('Pdata_client') ){
                $this->load->model('Registrations_client_model');
                $Cdata = $this->input->post('Pdata_client');
                $Cdata['user_id_fk'] = $id;
                 $epx = explode("##",$Cdata["job_title_id_fk"]);
               $Cdata["job_title_id_fk"] =  (isset($epx[0]))? $epx[0]:"0" ;
               $Cdata["date_of_birth"] = strtotime($Cdata["date_of_birth"]);
                $Cdata["cv_file"] = $this->upload_file("cv_file");
                $this->Registrations_client_model->insert($Cdata);
            }
            if ($user_id != false) {
                $userdata = $this->Registrations_model->as_array()->get($user_id);
                $userdata['is_web_login'] = true;
                $this->session->set_userdata(["user_data"=>$userdata]);
                $this->Registrations_model->update($userdata["user_id"],["is_login"=>1]);
                if ($userdata["user_type"] == 1) {
                    redirect('user-profile/'.$userdata['user_id']);
                }
                elseif($userdata["user_type"] == 2){
                    redirect('company-profile/'.$userdata['user_id']);
                }
            }
            else{
                $this->load->view('frontend/home', []);
            }
        }
    }

    public function weblogin(){
        $this->form_validation->set_rules('user_name', '', 'required');
        $this->form_validation->set_rules('password', '', 'required');
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(["code"=>422]);
        }
        else{
            $this->load->model('Registrations_model');
            $name = $this->input->post("user_name");
            $pass = setPass($this->input->post("password"));
            $userdata = $this->Registrations_model->as_array()->get_by(["username"=>$name]);

            if ($userdata) {
                if($userdata["password"] == $pass){
                    if ( $userdata["is_active"] == 1) {
                        $userdata['is_web_login'] = true;
                        $this->session->set_userdata(["user_data" => $userdata]);
                        $this->Registrations_model->update($userdata["user_id"], ["is_login" => 1]);
                        if ($userdata["user_type"] == 1) {
                            //redirect('user-profile/' . $userdata['user_id']);
                             echo json_encode(["code"=>200,"url"=>'user-profile/' . $userdata['user_id']]);
                        }
                        elseif ($userdata["user_type"] == 2) {
                            //redirect('company-profile/' . $userdata['user_id']);
                             echo json_encode(["code"=>200,"url"=>'company-profile/' . $userdata['user_id']]);
                        }
                    }
                    else {
                        echo json_encode(["code"=>423]);
                    }
                }
                else{
                    echo json_encode(["code"=>401]);
                }
            }
            else {
                echo json_encode(["code"=>404]);
            }
        }
    }

    public function googlelogin(){
		if(isset($_GET['code'])){
			// Authenticate user with google
			if($this->google->getAuthenticate()){
				// Get user info from google
				$gpInfo = $this->google->getUserInfo();
				// Preparing data for database insertion
				/*
				$userData['oauth_provider'] = 'google';
                $userData['oauth_uid']         = $gpInfo['id'];
                $userData['first_name']     = $gpInfo['given_name'];
                $userData['last_name']         = $gpInfo['family_name'];
                $userData['email']             = $gpInfo['email'];
                $userData['gender']         = !empty($gpInfo['gender'])?$gpInfo['gender']:'';
                $userData['locale']         = !empty($gpInfo['locale'])?$gpInfo['locale']:'';
                $userData['picture']         = !empty($gpInfo['picture'])?$gpInfo['picture']:'';
				*/
				// Insert or update user data to the database
				$this->load->model('Registrations_model');
				$email = $gpInfo['email'];
				$userdata = $this->Registrations_model->as_array()->get_by(["email"=>$email]);
				// Store the status and user profile info into session
				if ($userdata) {
					$userdata['is_web_login'] = true;
					$this->session->set_userdata(["user_data" => $userdata]);
					$this->Registrations_model->update($userdata["user_id"], ["is_login" => 1]);
					if ($userdata["user_type"] == 1) {
						redirect('user-profile/' . $userdata['user_id'],"refresh");
					}
					elseif ($userdata["user_type"] == 2) {
						redirect('company-profile/' . $userdata['user_id'],"refresh");
					}
				}
				else{
					$id = (isset($_SESSION['user_id']))? $_SESSION['user_id']:0;
					$data["all_imgs"] = $this->Bg_model->get_all();
					$data["all_job_titles"] = $this->Job_titles_model->order_by('ar_title', 'ASC')->get_many_by(['available'=>1]);
					$data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
					$data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
					$data["all_countries"] = $this->Countries_model->get_all();
					$data['metadiscription']=$data['metakeyword']=$data['title']= "الرئيسية - هدفك دوت كوم";
					$data["erorr_mail"] = true;
					$this->load->view('frontend/home', $data);
				}
			}
		}
	}

	public function facebooklogin(){ // http://hadfak.com/Web/facebooklogin
		// Authenticate user with facebook
		if($this->facebook->is_authenticated()) {
			// Get user info from facebook
			$fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');
			// Preparing data for database insertion
			/*
			$userData['oauth_provider'] = 'facebook';
			$userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
			$userData['first_name']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
			$userData['last_name']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
			$userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
			$userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
			$userData['picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
			$userData['link']        = !empty($fbUser['link'])?$fbUser['link']:'https://www.facebook.com/';
            */
			// Insert or update user data to the database
			$this->load->model('Registrations_model');
			$email = !empty($fbUser['email'])?$fbUser['email']:'';
			$userdata = $this->Registrations_model->as_array()->get_by(["email"=>$email]);
			// Store the status and user profile info into session
			if ($userdata) {
				$userdata['is_web_login'] = true;
				$this->session->set_userdata(["user_data" => $userdata]);
				$this->Registrations_model->update($userdata["user_id"], ["is_login" => 1]);
				if ($userdata["user_type"] == 1) {
					redirect('user-profile/' . $userdata['user_id'],"refresh");
				}
				elseif ($userdata["user_type"] == 2) {
					redirect('company-profile/' . $userdata['user_id'],"refresh");
				}
			}
			else{
				$id = (isset($_SESSION['user_id']))? $_SESSION['user_id']:0;
				$data["all_imgs"] = $this->Bg_model->get_all();
				$data["all_job_titles"] = $this->Job_titles_model->order_by('ar_title', 'ASC')->get_many_by(['available'=>1]);
				$data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
				$data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
				$data["all_countries"] = $this->Countries_model->get_all();
				$data['metadiscription']=$data['metakeyword']=$data['title']= "الرئيسية - هدفك دوت كوم";
				$data["erorr_mail"] = true;
				$this->load->view('frontend/home', $data);
			}
		}
      // $this->test_j("err");
	}

    public function logout(){
        $this->load->model('Registrations_model');
        $this->Registrations_model->update($_SESSION["user_data"]["user_id"],["is_login"=>0]);
        unset($_SESSION["user_data"]);
		$this->google->revokeToken();
		$this->facebook->destroy_session();
		//$this->session->sess_destroy();
        redirect('home', 'refresh');
    }

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */
    public function clientProfile($id){

        if(!isset($_SESSION["user_data"]['is_web_login']) &&  $_SESSION["user_data"]['is_web_login'] != true){
            redirect('home', 'refresh');
        }

        $this->load->model('Registrations_model');
        $this->load->model('Registrations_client_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
        $this->load->model('Definitions_model');
        $this->load->model('Job_titles_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Company_jobs_model');
        $this->load->model('Apply_jobs_model');
        //--------------------------------------------------------
        $data["all_imgs"] = $this->Bg_model->get_all();

        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);

        $data["client_data"] = $this->Registrations_client_model->with('qualification')
                                    ->with('job_title')->with('activity')->with('experience_year')->get($id);
        $data["social_data"] = $this->Registrations_social_model->get($id);
        $data["all_countries"] = $this->Countries_model->get_all();
        $data["all_cities"] = $this->Cities_model->get_all();
        $data["all_qualifications"] = $this->Definitions_model->get_many_by(["type"=>QUALIFICATIONKEY,"available"=>1]);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        $data["all_job_titles"] = $this->Job_titles_model->get_many_by(['available'=>1]);
        //--------------------------------------
        if (isset($data["client_data"]->job_title_id_fk) && isset($data["client_data"]->activity_id_fk)) {
            $where = ["job_title_id" => $data["client_data"]->job_title_id_fk];
            $data["jobs_for_me"] = $this->Company_jobs_model->count_by($where);
            $where = ["activity_id_fk" => $data["client_data"]->activity_id_fk];
            $data["com_for_me"] = $this->Registrations_company_model->count_by($where);
            $data["com_jobs"] = $this->Company_jobs_model->getUserJobs($data["client_data"]->job_title_id_fk);
        }
        else{
            $data["jobs_for_me"] = 0 ;
            $data["com_for_me"]  = 0 ;
            $data["com_jobs"] = [];
        }


        $data["total_jobs"] = $this->Apply_jobs_model->count_by(['user_id'=>$id]);


        //--------------------------------------
        $data['metadiscription']=$data['metakeyword']=$data['title']= "سيرتى الذاتية- هدفك دوت كوم";
        $data['subview'] = 'client_profile';
        $this->load->view('layout/web', $data);
    }

    public function companyProfile($id){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
               redirect('home', 'refresh');
        }
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
        $this->load->model('Definitions_model');
        $this->load->model('Company_activities_model');
        $this->load->model('Company_jobs_model');
        $this->load->model('Registrations_client_model');
        $this->load->model('Apply_jobs_model');
        //----------------------------------------------
        $data["all_imgs"] = $this->Bg_model->get_all();
        $data["total_jobs"] = $this->Apply_jobs_model->count_by(['company_id'=>$id]);
        $data["total_com_jobs"] = $this->Company_jobs_model->count_by(['available'=>1,"company_id_fk"=>$id]);
        //----------------------------------------------
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
        $data["social_data"] = $this->Registrations_social_model->get($id);
        $data["company_data"] = $this->Registrations_company_model
                                    ->with('activity')->with('number_employees_data')->get($id);
        //--------------------------------------------------------
        $data["all_countries"] = $this->Countries_model->get_all();
        $data["all_cities"] = $this->Cities_model->get_all();
        $data["all_number_employees"] = $this->Definitions_model->get_many_by(["type"=>NUMBEREMPLOYEES,"available"=>1]);
        $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
        //----------------------------------------------
		$where = ["activity_id_fk" => 0];
        $data["com_jobs"] = $this->Company_jobs_model->getCompanyJobs($id);
		if (isset($data["company_data"]->activity_id_fk)) {
			$where = ["activity_id_fk" => $data["company_data"]->activity_id_fk];
		}
		$data["persons"] = $this->Registrations_client_model->searchBy($where ,false);
        //---------------------------------------------
        $data["applaiers"] = $this->Apply_jobs_model
                                  ->with_meny(['user_data','com_data','job','client'])
                                  ->get_many_by(['company_id'=>$id]);
        //----------------------------------------------
        $data['metadiscription']=$data['metakeyword']=$data['title']= "صفحة شركتي- هدفك دوت كوم";
        $data['subview'] = 'company_profile';
        $this->load->view('layout/web', $data);
    }

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */
    public function updateProfile($id){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
            redirect('home', 'refresh');
        }
        if($this->input->post('add') == "add"){
            $this->load->model('Registrations_model');
            //-------------------------------------------
            //$this->test($_POST);
            $Udata = $this->input->post('Pdata');

            $logo  =$this->upload_image("logo");
            if (!empty($logo)) {
                $Udata["logo"] = $logo;
            }
            $banner =  $this->upload_image("banner");
            if (!empty($banner)) {
                $Udata["banner"] = $banner;
            }
            $this->Registrations_model->update($id,$Udata);
            //-------------------------------------------
            if($this->input->post('Pdata_client') ){
                $this->load->model('Registrations_client_model');

                $Cdata = $this->input->post('Pdata_client');
                $Cdata["cv_file"] = $this->upload_file("cv_file");
                  $epx = explode("##",$Cdata["job_title_id_fk"]);
                $Cdata["job_title_id_fk"] =  (isset($epx[0]))? $epx[0]:"0" ;
                $Cdata["activity_id_fk"] =  (isset($epx[1]))? $epx[1]:"0" ;
                $Cdata["date_of_birth"] = strtotime($Cdata["date_of_birth"]);
                $Cdata["qualification_date"] = strtotime($Cdata["qualification_date"]);
                if ($this->input->post('status') == "update") {
                    $this->Registrations_client_model->update($id, $Cdata);
                }
                else{
                    $Cdata['user_id_fk'] =$id;
                    $this->Registrations_client_model->insert($Cdata);
                }
            }
            //-------------------------------------------
            if($this->input->post('Pdata_company') ){
                $this->load->model('Registrations_company_model');

                $Cdata = $this->input->post('Pdata_company');
                //$Cdata["date_created"] = strtotime($Cdata["date_created"]);
                if ($this->input->post('status') == "update") {
                    $this->Registrations_company_model->update($id, $Cdata);
                }
                else{
                    $Cdata['user_id_fk'] =$id;
                    $this->Registrations_company_model->insert($Cdata);
                }
            }
            if(isset($_SERVER['HTTP_REFERER'])){
                $previos_path = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
                redirect($previos_path,'refresh');
            }
            else{
                redirect("Web",'refresh');
            }
        }
    }

    public function updateAuth($id){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
            redirect('home', 'refresh');
        }
        if($this->input->post('add') == "add"){
            $this->load->model('Registrations_model');
            //-------------------------------------------
            $Udata = $this->input->post('Pdata');
            $Udata["password"] = setPass($Udata["password"]);
            $Udata["updated_at"] = time();
            $this->Registrations_model->update($id,$Udata);
            //-------------------------------------------
            if(isset($_SERVER['HTTP_REFERER'])){
                $previos_path = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
                redirect($previos_path,'refresh');
            }
            else{
                redirect("Web",'refresh');
            }
        }
    }

    public function updateSocial($id){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
            redirect('home', 'refresh');
        }
        if($this->input->post('add') == "add"){
            $this->load->model('Registrations_social_model');
            $Cdata = $this->input->post('Psocial');
            if ($this->input->post('status') == "update") {
                $this->Registrations_social_model->update($id, $Cdata);
            }
            else{
                $Cdata['user_id_fk'] =$id;
                $this->Registrations_social_model->insert($Cdata);
            }
            if(isset($_SERVER['HTTP_REFERER'])){
                $previos_path = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
                redirect($previos_path,'refresh');
            }
            else{
                redirect("Web",'refresh');
            }
        }
    }

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */

    public function showClientProfile($id){
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_client_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
        $this->load->model('Definitions_model');
        $this->load->model('Job_titles_model');
        //--------------------------------------
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
        $data["client_data"] = $this->Registrations_client_model->with('qualification')
            ->with('job_title')->with('activity')->with('experience_year')->get($id);
        $data["social_data"] = $this->Registrations_social_model->get($id);
        //--------------------------------------
        $data['metadiscription']=$data['metakeyword']=$data['title']= $data["profile_data"]->name;
        $data['subview'] = 'show_client_profile';
        $this->load->view('layout/web', $data);
    }

    public function showCompanyProfile($id){
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Company_jobs_model');
        //----------------------------------------------
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
        $data["social_data"] = $this->Registrations_social_model->get($id);
        $data["company_data"] = $this->Registrations_company_model
                                     ->with('activity')->with('number_employees_data')->get($id);
        $data["jobs"] = $this->Company_jobs_model->searchBy(["company_id_fk"=>$id] ,false);
        //----------------------------------------------
        $data['metadiscription']=$data['metakeyword']=$data['title']= $data["profile_data"]->name;
        $data['subview'] = 'show_company_profile';
        $this->load->view('layout/web', $data);
    }

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */

    public function search(){
        $type = $this->input->get("type");
        if(isset($type) && !empty($type)){
            if(in_array($type,array("user","company","job"))){
                $this->load->model('Countries_model');
                $this->load->model('Company_activities_model');
                $this->load->model('Job_titles_model');
                //--------------------------
                switch ($type) {
                    case "company":
                        $this->load->model('Registrations_company_model');
                        $title = "ابحث عن شركة- هدفك دوت كوم";
                        $data["company"] = $this->Registrations_company_model->searchBy();
                        break;
                    case "user":
                        $this->load->model('Registrations_client_model');
                        $title = "ابحث عن شخص- هدفك دوت كوم";
                        $data["persons"] = $this->Registrations_client_model->searchBy();
                        break;
                    case "job":
                        $title = "ابحث عن وظائف- هدفك دوت كوم";
                        $this->load->model('Company_jobs_model');
                        $data["jobs"] = $this->Company_jobs_model->searchBy();
                        break;
                }
                //---------------------------
                $data["type"] = $type;
                $data["all_job_titles"] = $this->Job_titles_model->get_many_by(['available'=>1]);
                $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
                $data["all_countries"] = $this->Countries_model->get_all();
                $data['metadiscription'] = $data['metakeyword'] = $data['title'] = $title ;
                $data['subview'] = 'search';
                $this->load->view('layout/web', $data);
            }
            else{
                redirect("Page404", 'refresh');
            }
        }
        else{
            redirect("Page404", 'refresh');
        }
        //----------------------------------------------
    }

    public function searchBy(){
        $type = $this->input->post("search_about");
        $key_name = $this->input->post("key_name");
        $activity = $this->input->post("activity");
        $country = $this->input->post("country");
        $job_title_id_fk = $this->input->post("job_title_id_fk");
        $nationality = $this->input->post("nationality");
        if (isset($type) && !empty($type) && in_array($type,["user","company","job"])) {
            $where = []; $whereIn = [];
            $whereLike = false;
            //--------------------------
            if(isset($key_name) && !empty($key_name)){
                $whereLike = $key_name ;
            }
            if(isset($activity) && !empty($activity)){
                $whereIn['activity_id_fk'] = $activity;
            }
            if(isset($country) && !empty($country)){
                $whereIn['country_id'] = $country ;
            }
            //--------------------------
			if(isset($job_title_id_fk) && !empty($job_title_id_fk)){
				$whereIn['job_title_id_fk'] = $job_title_id_fk ;
			}
			if(isset($nationality) && !empty($nationality)){
				$whereIn['nationality'] = $nationality ;
			}

            //--------------------------
            switch ($type) {
                case "company":
                    $this->load->model('Registrations_company_model');
                    $where['user_type'] = 2 ;
                    $data["company"] = $this->Registrations_company_model->searchBy($where ,$whereLike, $whereIn);
                    $this->load->view('frontend/load/search_company',$data);
                    break;
                case "user":  //
                    $this->load->model('Registrations_client_model');
                    $where['user_type'] = 1 ;
                    $data["persons"] = $this->Registrations_client_model->searchBy($where ,$whereLike , $whereIn);
                    $this->load->view('frontend/load/search_person',$data);
                    break;
                case "job":
                    $this->load->model('Company_jobs_model');
                    $data["jobs"] = $this->Company_jobs_model->searchBy($where ,$whereLike, $whereIn);
                    $this->load->view('frontend/load/search_job',$data);
                    break;
            }
            //---------------------------
        }
    }


    public  function getAppliers(){

        $type = $this->input->get("type");
        if(isset($type) && !empty($type)){
            if(in_array($type,array("appliers","jobs"))){

                //--------------------------
                switch ($type) {
                    case "appliers":
                        $company_id = $this->input->get("company_id");
                        $this->load->model('Apply_jobs_model');
                        $title = 'المتقدمين للوظائف';
                        $data["persons"] = $this->Apply_jobs_model
                                                ->with_meny(['user_data','com_data','job','client'])
                                                ->get_many_by(['company_id'=>$company_id]);
                        break;
                    case "jobs":
                        $this->load->model('Company_jobs_model');
                        $job_title_id_fk = $this->input->get('job_title_id');
                        $data['jobs'] = $this->Company_jobs_model->getUserJobs($job_title_id_fk);
                        $title = 'وظيفة تناسبك';
                        break;
                }
                //-------------------------------
            }
            else{
                redirect("Page404", 'refresh');
            }
        }
        else{
            redirect("Page404", 'refresh');
        }
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = $title ;
        $data['subview'] = 'get_appliers';
        $this->load->view('layout/web', $data);
    }

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */

    public function publishJob($id){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
            redirect('home', 'refresh');
        }
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Job_titles_model');
        $this->load->model('Definitions_model');
        $this->load->model('Company_activities_model');
        $this->load->model('Company_jobs_model');
        //----------------------------------------------
        $data["one"]= (object) $this->Company_jobs_model->get_filds();
        $data["op"] = "insert";
        //----------------------------------------------
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($id);
        $data["social_data"]  = $this->Registrations_social_model->get($id);
        $data["company_data"] = $this->Registrations_company_model
                                     ->with('activity')->with('number_employees_data')->get($id);
        if (isset($data["company_data"]->activity_id_fk)) {
            $where = ['available' => 1/*, "activity_id_fk" =>$data["company_data"]->activity_id_fk*/];
        }
        else {
            $where = ['available' => 1];
        }
        $data["all_job_titles"] = $this->Job_titles_model->get_many_by($where);
        $data["type_work"] = $this->Definitions_model->get_many_by(["type"=>TYPEWORK,"available"=>1]);
        $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
        //----------------------------------------------
        $data["all_qualifications"] = $this->Definitions_model->get_many_by(["type"=>QUALIFICATIONKEY,"available"=>1]);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        //----------------------------------------------
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'اعلان عن وظيفة- هدفك دوت كوم';
        $data['subview'] = 'publish_job';
        $this->load->view('layout/web', $data);
    }

    public function addJob($id){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
            redirect('home', 'refresh');
        }
        if($this->input->post('add') == "add"){
            $this->load->model('Registrations_model');
            //-------------------------------------------
           // $this->test_j($_POST);
            $Udata = $this->input->post('Pdata');
            $this->Registrations_model->update($id,$Udata);
            //-------------------------------------------
            if($this->input->post('Pdata_company') ){
                $this->load->model('Registrations_company_model');
                $Cdata = $this->input->post('Pdata_company');
                if ($this->input->post('status_company') == "update") {
                    $this->Registrations_company_model->update($id, $Cdata);
                }
                else{
                    $Cdata['user_id_fk'] = $id;
                    $this->Registrations_company_model->insert($Cdata);
                }
            }
            //-------------------------------------------
            if($this->input->post('Psocial') ) {
                $this->load->model('Registrations_social_model');
                $Cdata = $this->input->post('Psocial');
                if ($this->input->post('status_social') == "update") {
                    $this->Registrations_social_model->update($id, $Cdata);
                } else {
                    $Cdata['user_id_fk'] = $id;
                    $this->Registrations_social_model->insert($Cdata);
                }
            }
            //-------------------------------------------
            if($this->input->post('Pjob') ) {
                $this->load->model('Company_jobs_model');
                $Cdata = $this->input->post('Pjob');
                //-------------------------
                if($this->input->post('new_job') &&  $Cdata["job_title_id"] == 'other'){
                    $this->load->model('Job_titles_model');
                    $Tdata['ar_title'] = $this->input->post('new_job');
                    $Tdata['activity_id_fk'] = $Cdata["activity_id_fk"];
                    $job_id = $this->Job_titles_model->insert($Tdata);
                    $Cdata["job_title_id"] =  $job_id ;
                    $Cdata["activity_id_fk"] =  $Cdata["activity_id_fk"] ;
                }
                else{
                    $epx = explode("##",$Cdata["job_title_id"]);
                    $Cdata["job_title_id"] =  (isset($epx[0]))? $epx[0]:"0" ;
                    $Cdata["activity_id_fk"] =  (isset($epx[1]))? $epx[1]:"0" ;
                }
                //-------------------------

                $Cdata["created_at"] = time();
                $Cdata['company_id_fk'] = $id;
                $this->Company_jobs_model->insert($Cdata);

            }
            //-------------------------------------------


            //-------------------------------------------
            if(isset($_SERVER['HTTP_REFERER'])){
                $previos_path = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
                redirect($previos_path,'refresh');
            }
            else{
                redirect("Web",'refresh');
            }
        }
    }

    public function editJob($com,$job){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
            redirect('home', 'refresh');
        }
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Registrations_social_model');
        $this->load->model('Job_titles_model');
        $this->load->model('Definitions_model');
        $this->load->model('Company_activities_model');
        $this->load->model('Company_jobs_model');
        //----------------------------------------------
        $data["one"]= $this->Company_jobs_model->get($job);
        $data["op"] = "update";
        //----------------------------------------------
        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($com);
        $data["social_data"]  = $this->Registrations_social_model->get($com);
        $data["company_data"] = $this->Registrations_company_model
            ->with('activity')->with('number_employees_data')->get($com);
        $data["all_job_titles"] = $this->Job_titles_model->get_many_by(['available'=>1]);
        $data["type_work"] = $this->Definitions_model->get_many_by(["type"=>TYPEWORK,"available"=>1]);
        $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
        //----------------------------------------------
        $data["all_qualifications"] = $this->Definitions_model->get_many_by(["type"=>QUALIFICATIONKEY,"available"=>1]);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        //----------------------------------------------
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'تعديل وظيفة- هدفك دوت كوم';
        $data['subview'] = 'publish_job';
        $this->load->view('layout/web', $data);
    }

    public  function updateJob($id,$job){
        if(!isset($_SESSION["user_data"]['is_web_login']) ){
            redirect('home', 'refresh');
        }
        if($this->input->post('add') == "add"){
            $this->load->model('Registrations_model');
            //-------------------------------------------
            // $this->test_j($_POST);
            $Udata = $this->input->post('Pdata');
            $this->Registrations_model->update($id,$Udata);
            //-------------------------------------------
            if($this->input->post('Pdata_company') ){
                $this->load->model('Registrations_company_model');
                $Cdata = $this->input->post('Pdata_company');
                if ($this->input->post('status_company') == "update") {
                    $this->Registrations_company_model->update($id, $Cdata);
                }
                else{
                    $Cdata['user_id_fk'] = $id;
                    $this->Registrations_company_model->insert($Cdata);
                }
            }
            //-------------------------------------------
            if($this->input->post('Psocial') ) {
                $this->load->model('Registrations_social_model');
                $Cdata = $this->input->post('Psocial');
                if ($this->input->post('status_social') == "update") {
                    $this->Registrations_social_model->update($id, $Cdata);
                } else {
                    $Cdata['user_id_fk'] = $id;
                    $this->Registrations_social_model->insert($Cdata);
                }
            }
            //-------------------------------------------
            if($this->input->post('Pjob') ) {
                $this->load->model('Company_jobs_model');
                $Cdata = $this->input->post('Pjob');
                $epx = explode("##",$Cdata["job_title_id"]);
                $Cdata["job_title_id"] =  (isset($epx[0]))? $epx[0]:"0" ;
                $Cdata["activity_id_fk"] =  (isset($epx[1]))? $epx[1]:"0" ;
                $Cdata["created_at"] = time();
                $Cdata['company_id_fk'] = $id;
                $this->Company_jobs_model->update($job,$Cdata);
            }
            //-------------------------------------------
            if(isset($_SERVER['HTTP_REFERER'])){
                $previos_path = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
                redirect($previos_path,'refresh');
            }
            else{
                redirect("Web",'refresh');
            }
        }
    }

    public  function deleteJob($id,$job){
        $this->load->model('Company_jobs_model');
        $this->load->model('Apply_jobs_model');
        $this->Company_jobs_model->delete($job);
        $this->Apply_jobs_model->delete_by(["job_id"=>$job]);
        /*if(isset($_SERVER['HTTP_REFERER'])){
            $previos_path = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
            redirect('company-profile/'.$id,'refresh');
        }
        else{
            redirect("Web",'refresh');
        }*/
    }

    public function showJob($job_id,$company_id){
        $this->load->model('Company_jobs_model');
        $this->load->model('Registrations_model');
        $this->load->model('Registrations_company_model');
        $this->load->model('Apply_jobs_model');
        //----------------------------------------------


        $data["profile_data"] = $this->Registrations_model->with("country")->with("city")->get($company_id);

        $data["company_data"] = $this->Registrations_company_model
                                      ->with('activity')
                                      ->with('number_employees_data')
                                      ->get($company_id);

        $data["one"]= $this->Company_jobs_model
                           ->with_meny(["job_title" ,"type_work",'activity','qualification','experience_year'])
                           ->get($job_id);
        if(isset($_SESSION["user_data"]['is_web_login']) && $_SESSION["user_data"]['user_type'] == 1  ){
            $data["apply_case"] = $this->Apply_jobs_model->isApply(["user_id"=>$_SESSION["user_data"]["user_id"],"job_id"=>$job_id]);
        }
        //----------------------------------------------

         //========= SHARE ===================

         $data["share_title"]=$this->Company_jobs_model->with_meny(["job_title"])->get($job_id)->job_title->ar_title;

         $data["share_content"]=$this->Company_jobs_model->get($job_id)->details;

         $logo= $data["profile_data"]->logo;
          $data["share_image"]="uploads/images/".$logo;

        /* echo "<pre>";
         print_r($data["share_image"]);
         die;*/

        //========= SHARE ===================

        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'تفاصيل وظيفة- هدفك دوت كوم';
        $data['subview'] = 'show_job';
        $this->load->view('layout/web', $data);
    }

    public function applyJob(){
        $this->form_validation->set_rules('job_id', '', 'required|numeric');
        $this->form_validation->set_rules('company_id', '', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
           echo (validation_errors());
        }
        else{
            if(isset($_SESSION["user_data"]['is_web_login']) ){
                $this->load->model('Apply_jobs_model');
                $data["job_id"] = $this->input->post('job_id') ;
                $data["company_id"] = $this->input->post('company_id') ;
                $data["user_id"] = $_SESSION["user_data"]['user_id'] ;
                $this->Apply_jobs_model->insert($data);
            }
        }
    }

    /**
     *  ============================================================
     *
     *  ------------------------------------------------------------
     *
     *  ============================================================
     */

}// END CLASS
