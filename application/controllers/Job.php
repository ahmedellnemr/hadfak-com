<?php
class Job extends MY_Controller{
    private $folder = "jobs";
    private $con    = "admin-job";
    public function __construct(){
        parent::__construct();
        $this->load->model('Company_jobs_model','Cmodel');
        //  redirect("Page404", 'refresh');
        /*
         $main_iamge = $this->upload_image("");
            $Idata[""] = $main_iamge;
            $filesCount = $_FILES["images"]['size'][0];
            if ($filesCount != "0" || $filesCount != 0) {
                $imgs = $this->upload_muli_image("images");
                $this->Model_salon->add_images($id, $imgs);
            }

         $main_iamge = $this->upload_image("");
            if (!empty($main_iamge)) {
                $Idata[""] = $main_iamge;
            }

         */
    }

    public  function index(){
        $data["data_table"] = $this->Cmodel
                                    ->with_meny(['user_data','activity','job_title','type_work','qualification','experience_year'])
                                    ->get_all();
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'الوظائف ';
        $data["my_footer"] = ['table'];
        $data['subview'] = $this->folder.'/all';
        $this->load->view('layout/admin', $data);
    }

    public  function add(){

        $this->load->model('Registrations_model');
        $this->load->model('Job_titles_model');
        $this->load->model('Definitions_model');
        $this->load->model('Company_activities_model');
        $this->load->model('Company_jobs_model');
        //----------------------------------------------
        $data["one"]= (object) $this->Company_jobs_model->get_filds();
        $data["companies"]=  $this->Registrations_model->get_many_by(["user_type"=>2]);
        //----------------------------------------------
        $data["all_job_titles"] = $this->Job_titles_model->get_many_by(['available'=>1]);
        $data["type_work"] = $this->Definitions_model->get_many_by(["type"=>TYPEWORK,"available"=>1]);
        $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
        //----------------------------------------------
        $data["all_qualifications"] = $this->Definitions_model->get_many_by(["type"=>QUALIFICATIONKEY,"available"=>1]);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        //----------------------------------------------
        $data["op"] = 'INSERT';
        $data["form"] = $this->con.'/create';
        $data["out"] = $this->Cmodel->get_filds();
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'الوظائف ';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function edit($id){

        $this->load->model('Registrations_model');
        $this->load->model('Job_titles_model');
        $this->load->model('Definitions_model');
        $this->load->model('Company_activities_model');
        $this->load->model('Company_jobs_model');

        $data["one"]= $this->Company_jobs_model->get($id);
        $data["companies"]=  $this->Registrations_model->get_many_by(["user_type"=>2]);
        //----------------------------------------------
        $data["all_job_titles"] = $this->Job_titles_model->get_many_by(['available'=>1]);
        $data["type_work"] = $this->Definitions_model->get_many_by(["type"=>TYPEWORK,"available"=>1]);
        $data["all_company_activities"] = $this->Company_activities_model->get_many_by(['available'=>1]);
        //----------------------------------------------
        $data["all_qualifications"] = $this->Definitions_model->get_many_by(["type"=>QUALIFICATIONKEY,"available"=>1]);
        $data["all_experience_yearss"] = $this->Definitions_model->get_many_by(["type"=>EXPERYEAR,"available"=>1]);
        $data["op"] = 'UPDTATE';
        $data["form"] = $this->con.'/update/'.$id;
        $data["out"] = $this->Cmodel->as_array()->get($id);
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'الوظائف ';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function create(){

        if ($this->input->post('INSERT') == "INSERT") {
            if($this->input->post('Pjob') ) {
                $Cdata = $this->input->post('Pjob');
                $epx = explode("##",$Cdata["job_title_id"]);
                $Cdata["job_title_id"] =  (isset($epx[0]))? $epx[0]:"0" ;
                $Cdata["activity_id_fk"] =  (isset($epx[1]))? $epx[1]:"0" ;
                $Cdata["created_at"] = time();
                $this->Cmodel->insert($Cdata);
            }
            //----------------------------------------------
            $this->message('s');
            redirect($this->con."/add", 'refresh');
        }

    }

    public  function update($id){

        if ($this->input->post('UPDTATE') == "UPDTATE") {
            if($this->input->post('Pjob') ) {
                $this->load->model('Company_jobs_model');
                $Cdata = $this->input->post('Pjob');
                $epx = explode("##",$Cdata["job_title_id"]);
                $Cdata["job_title_id"] =  (isset($epx[0]))? $epx[0]:"0" ;
                $Cdata["activity_id_fk"] =  (isset($epx[1]))? $epx[1]:"0" ;
                $Cdata["created_at"] = time();
                $this->Company_jobs_model->update($id,$Cdata);
            }
            //----------------------------------------------
            $this->message('i');
            redirect( $this->con, 'refresh');
        }

    }


    public  function delete($id){

        $this->load->model('Apply_jobs_model');
        $this->Cmodel->delete($id);
        $this->Apply_jobs_model->delete_by(["job_id"=>$id]);
        $this->message('e');
        redirect($this->con, 'refresh');
    }



} //END CLASS
?>