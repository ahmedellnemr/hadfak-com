<?php
class BgAdd extends MY_Controller{
    private $folder = "bg_data";
    private $con    = "admin-backgrounds";
    public function __construct(){
        parent::__construct();
        $this->load->helper('html');
        $this->load->model('Bg_model','Cmodel');
       

    }

       public  function index(){

       $data["data_table"] = $this->Cmodel->get_all();
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'تغيير الخلفيات';
        $data["my_footer"] = ['table'];
        $data['subview'] = $this->folder.'/all';
        $this->load->view('layout/admin', $data);
    }

    public  function add(){
       // $data["data_table"] = $this->Cmodel->get_all();
        $data["op"] = 'INSERT';
        $data["form"] = $this->con.'/create';
        $data["out"] = $this->Cmodel->get_all();
       
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'تغيير الخلفيات';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function edit($id){
        $data["all_imgs"] = $this->Cmodel->get_all();
        
        
        $data["op"] = 'UPDTATE';
        $data["form"] = $this->con.'/update/'.$id;
        $data["out"] = $this->Cmodel->as_array()->get($id);
        $data['metadiscription'] = $data['metakeyword'] = $data['title'] = 'تغيير الخلفيات';
        $data["my_footer"] = ["upload","valid","date"];
        $data['subview'] = $this->folder.'/one';
        $this->load->view('layout/admin', $data);
    }

    public  function create(){

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('Pdata');
            
            $image = $this->upload_image("index_image");
            $image_co = $this->upload_image("company_image");
            $image_pe = $this->upload_image("person_image");
            
            
            $Idata["index_image"] = $image;
            $Idata["company_image"] = $image_co;
            $Idata["person_image"] = $image_pe;
            
            
            //$Idata["date"] = strtotime($Idata["date"]) ;
            $Idata["created_at"] = time();
               
                    if (!empty($image)) {
                        $Idata["index_image"] = $image;
                    }
                    if (!empty($image_co)) {
                        $Idata["company_image"] = $image_co;
                    }
                    if (!empty($image_pe)) {
                        $Idata["person_image"] = $image_pe;
                    }
            $id = $this->Cmodel->insert($Idata);
            //----------------------------------------------
            $this->message('s');
            redirect($this->con."/add", 'refresh');
        }

    }

    public  function update($id){

        if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Idata = $this->input->post('Pdata');
            
              
            if (!empty($image)) {
                $Idata["index_image"] = $image;
            }
            if (!empty($image_co)) {
                $Idata["company_image"] = $image_co;
            }
            if (!empty($image_pe)) {
                $Idata["person_image"] = $image_pe;
            }
            
            //$Idata["date"] = strtotime($Idata["date"]) ;
            $Idata["created_at"] = time();

            $image  =$this->upload_image("index_image");
            $image_co  = $this->upload_image("company_image");
              $image_pe  = $this->upload_image("person_image");
            if (!empty($image)) {
                $Idata["index_image"] = $image;
            }
            if (!empty($image_co)) {
                $Idata["company_image"] = $image_co;
            }
            if (!empty($image_pe)) {
                $Idata["person_image"] = $image_pe;
            }

            $id = $this->Cmodel->update($id,$Idata);
            //----------------------------------------------
            $this->message('i');
            redirect( $this->con."/edit/1", 'refresh');
        }

    }


    public  function delete($id){

                $this->Cmodel->delete_by(["id"=>$id]);

        $this->message('e');
        redirect($this->con, 'refresh');
    }


	public function details(){/// to perview details of student
		$value=$this->input->post('valu');
		$data['details']=$this->Cmodel->get($value);
		$this->load->view('backend/remotework/details', $data);
        

					

	}




} //END CLASS
?>